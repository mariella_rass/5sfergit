<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=5sfer_yii2',
            'username' => '5sfer',
            'password' => 'GVjeNCTs0R',
            'charset' => 'utf8',
            'enableQueryCache' => true,
            'enableSchemaCache' => true,
            'queryCacheDuration' => 60
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
    'params' => [
        'mediaBaseUrl' => 'http://i.5sfer.com',
        'frontendBaseUrl' => 'http://5sfer.com',
    ]
];
