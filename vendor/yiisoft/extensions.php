<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-widget-spinner' => 
  array (
    'name' => 'kartik-v/yii2-widget-spinner',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/spinner' => $vendorDir . '/kartik-v/yii2-widget-spinner',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'kartik-v/yii2-widget-growl' => 
  array (
    'name' => 'kartik-v/yii2-widget-growl',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/growl' => $vendorDir . '/kartik-v/yii2-widget-growl',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert',
    ),
  ),
  'kartik-v/yii2-widget-affix' => 
  array (
    'name' => 'kartik-v/yii2-widget-affix',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/affix' => $vendorDir . '/kartik-v/yii2-widget-affix',
    ),
  ),
  'kartik-v/yii2-widgets' => 
  array (
    'name' => 'kartik-v/yii2-widgets',
    'version' => '3.4.0.0',
    'alias' => 
    array (
      '@kartik/widgets' => $vendorDir . '/kartik-v/yii2-widgets',
    ),
  ),
  'dragonjet/yii2-opengraph' => 
  array (
    'name' => 'dragonjet/yii2-opengraph',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dragonjet/opengraph' => $vendorDir . '/dragonjet/yii2-opengraph',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'himiklab/yii2-sitemap-module' => 
  array (
    'name' => 'himiklab/yii2-sitemap-module',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@himiklab/sitemap' => $vendorDir . '/himiklab/yii2-sitemap-module',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop',
    ),
  ),
  'kartik-v/yii2-widget-rangeinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-rangeinput',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/range' => $vendorDir . '/kartik-v/yii2-widget-rangeinput',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-widget-switchinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-switchinput',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/switchinput' => $vendorDir . '/kartik-v/yii2-widget-switchinput',
    ),
  ),
  'kartik-v/yii2-widget-touchspin' => 
  array (
    'name' => 'kartik-v/yii2-widget-touchspin',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@kartik/touchspin' => $vendorDir . '/kartik-v/yii2-widget-touchspin',
    ),
  ),
  'zelenin/yii2-slug-behavior' => 
  array (
    'name' => 'zelenin/yii2-slug-behavior',
    'version' => '1.5.1.0',
    'alias' => 
    array (
      '@Zelenin/yii/behaviors' => $vendorDir . '/zelenin/yii2-slug-behavior',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'sjaakp/yii2-illustrated-behavior' => 
  array (
    'name' => 'sjaakp/yii2-illustrated-behavior',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@sjaakp/illustrated' => $vendorDir . '/sjaakp/yii2-illustrated-behavior',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.8.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'sheershoff/yii2-sentry-component' => 
  array (
    'name' => 'sheershoff/yii2-sentry-component',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@sheershoff/sentry' => $vendorDir . '/sheershoff/yii2-sentry-component/src',
    ),
  ),
  'zelenin/yii2-rss' => 
  array (
    'name' => 'zelenin/yii2-rss',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@Zelenin/yii/extensions/Rss' => $vendorDir . '/zelenin/yii2-rss',
    ),
  ),
  'kop/yii2-scroll-pager' => 
  array (
    'name' => 'kop/yii2-scroll-pager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kop/y2sp' => $vendorDir . '/kop/yii2-scroll-pager',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.2.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.3.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'zhelyabuzhsky/yii2-sitemap' => 
  array (
    'name' => 'zhelyabuzhsky/yii2-sitemap',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@zhelyabuzhsky/sitemap' => $vendorDir . '/zhelyabuzhsky/yii2-sitemap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.6.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.3.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-builder' => 
  array (
    'name' => 'kartik-v/yii2-builder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/builder' => $vendorDir . '/kartik-v/yii2-builder',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '0.9.12.0',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
);
