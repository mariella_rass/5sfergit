<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.12.14
 * Time: 15:06
 */

namespace backend\assets;

use yii\web\AssetBundle;

class HighchartsAsset extends AssetBundle
{

    public $sourcePath = '@vendor/bower/highcharts-release';

    public $js = [
        'highcharts.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

} 