<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views/mail' => '@app/views/dektrium-user/mail'
                ],
            ],
        ],
        'request' => [
            'enableCsrfValidation' => true,
            'enableCsrfCookie' => false,
            'cookieValidationKey' => 'Cap3kxZgtV3MftzTnKZtUjtj3UzVyNfR',
        ]
    ],
//    'modules' => [
//        'user' => [
//            'class' => 'dektrium\user\Module',
//            // following line will restrict access to admin page
//            'as backend' => 'dektrium\user\filters\BackendFilter',
//        ],
//    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            //'as backend' => 'dektrium\user\filters\BackendFilter',
            'admins' => ['admin']
        ],
    ],
//    'on beforeRequest' => function () {
//        if (time()>1464998400) {
//            Yii::$app->catchAll = [
//              'hop/index'
//            ];
//        }
//    },
    'params' => $params,
];
