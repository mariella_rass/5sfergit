<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */
/* @var $form yii\widgets\ActiveForm */

$author = empty($model->userId) ?
    \common\models\Profile::findOne(1)->name :
    \common\models\Profile::findOne($model->userId)->name;

$editors = \common\models\Profile::find()
    ->select(['name as text', 'user_id as id'])
    ->where(['user_id' => Yii::$app->authManager->getUserIdsByRole('admin')])
    ->indexBy('id')
    ->asArray()
    ->column();

?>

<div class="post-form well">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
            ]
        ]
    ); ?>


    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <div class="row">

        <div class="col-xs-3">

            <?= $form->field($model, 'postStatus')->dropDownList(
                [
                    $model::DRAFT => 'Требуется доработка',
                    $model::APPROVAL => 'Ожидает проверки',
                    $model::FUTURE => 'Запланированная',
                    $model::PUBLISH => 'Опубликованная',
                    $model::ARCHIVE => 'Архивная',
                ],
                [
                    'placeholder' => 'статус',
                ]
            ) ?>

        </div>

        <div class="col-xs-3">

            <?= $form->field($model, 'postPublishTime')->widget(
                \kartik\datetime\DateTimePicker::className(),
                [
                    'language' => 'ru',
                    'options' => [
                        'value' => is_int($model->postPublishTime) ? strftime(
                            '%d-%m-%Y %H:%M',
                            $model->postPublishTime
                        ) : $model->postPublishTime
                    ],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy hh:ii',
                        //'startDate' => strftime('%d-%m-%Y %H:%M', time()),
                        'todayHighlight' => true,
                    ]
                ]
            ) ?>
        </div>

        <div class="col-xs-3">
            <?= $form->field($model, 'categories')->widget(
                \kartik\widgets\Select2::className(),
                [
                    'options' => ['placeholder' => 'Категория'],
                    'data' => \yii\helpers\ArrayHelper::map(
                        \common\models\Category::find()->all(),
                        'categoryId',
                        'categoryName'
                    ),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false,
                        'minimumInputLength' => 0,
                    ],
                ]
            ) ?>
        </div>

        <div id="userIdControl" class="col-xs-3">
            <?= $form->field($model, 'userId')->widget(
                \kartik\widgets\Select2::className(),
                [

                    'initValueText' => $author, // set the initial display text
                    'options' => ['placeholder' => 'Автор'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['author/authorlist']),
                            'dataType' => 'json',
                            //'data' => new JsExpression('function(params) { return {q:params.category}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(item) { return item.text; }'),
                        'templateSelection' => new JsExpression('function (item) { return item.text; }'),
                    ],
//                    'pluginEvents' => [
//                        'change' => 'function(e){
//                                console.log(e);
//                            var userId = $("#post-userid").select2().val();
//                            if (userId == 77){
//                                $("#post-editorid").select2().val(148).trigger("change");
//                            } else {
//                                $("#post-editorid").select2().val(1).trigger("change");
//                            }
//                        }'
//                    ]
                ]
            ) ?>
        </div>

        <div id="redactorComment" class="col-xs-12"
             style="<?= $model->postStatus != \backend\models\Post::DRAFT ? 'display: none' : 'display: block' ?>">
            <?= $form->field($model, 'postRedactorComment')->widget(
                yii\imperavi\Widget::className(),
                [
                    'options' => [
                        'placeholder' => 'Сообщение автору',
                        'buttonsHide' => ['hr', 'outdent', 'indent'],
                        'source' => false,
                    ],
                    'plugins' => ['fullscreen']
                ]
            ); ?>
        </div>
        <?php
        $this->registerJs(
            "
                        $('#post-poststatus').change(function(e){
                            var status = $('option:selected', this).val();
                            if (status == 'draft'){
                                $('#redactorComment').css('display','block');
                            } else {
                                $('#redactorComment').css('display','none');
                            }
                        })
                    "
        );
        ?>

        <div class="col-xs-3">
            <?= $form->field($model, 'postType')->dropDownList(
                [
                    $model::TYPE_UNDEF => 'Не определен',
                    $model::TYPE_NEW => 'Авторская',
                    $model::TYPE_RERIGHT => 'Рерайт',
                    $model::TYPE_TRANSLATE => 'Перевод',
                    $model::TYPE_NEWS => 'Новость',
                    $model::TYPE_TEST => 'Тест',
                ],
                [
                    'placeholder' => 'статус',
                ]
            ) ?>
        </div>


        <div class="col-xs-3">

            <?= $form->field($model, 'postComment')->dropDownList(
                [
                    'on' => 'Включены',
                    'off' => 'Выключены',
                ]
            ) ?>

        </div>

        <!--        <div  id="editorIdControl" class="col-xs-6">-->
        <!--            --><?php //= $form->field($model, 'editorId')->widget(
        //                \kartik\widgets\Select2::className(),
        //                [
        //                    'data' => $editors, // set the initial display text
        //                    'options' => [],
        //                    'pluginOptions' => [
        //                        'allowClear' => true,
        //                        'minimumInputLength' => 0,
        //                        'templateResult' => new JsExpression('function(item) { return item.text; }'),
        //                        'templateSelection' => new JsExpression('function (item) { return item.text; }'),
        //                        'disabled' => true
        //                    ],
        //                ]
        //            ) ?>
        <!--        </div>-->

        <!--        <div class="col-xs-12">-->
        <!--            <fieldset class="scheduler-border">-->


        <div class="col-xs-6">
            <?= $form->field($model, 'tags')->widget(
                \kartik\widgets\Select2::className(),
                [
                    'options' => ['placeholder' => 'Тэги'],
                    'data' => \yii\helpers\ArrayHelper::map(
                        $model->tags,
                        'tagId',
                        'tagName'
                    ),
                    'pluginOptions' => [
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['tag/taglist']),
                            'dataType' => 'json',
                            //                            'data' => new JsExpression('function(params) { return {q:params.category}; }')
                        ],
                        'allowClear' => true,
                        'multiple' => true,
                        'tags' => true,
                        'minimumInputLength' => 1,
                    ],
                ]
            ) ?>
        </div>
        <!--                <legend class="scheduler-border">Start Time</legend>-->
        <!--            </fieldset>-->
        <!--        </div>-->
        <?php
        $this->registerCss(
            "
            fieldset.scheduler-border {
                border: 1px solid #ddd !important;
                padding: 0 1.4em 1.4em 1.4em !important;
                margin: 0 0 1.5em 0 !important;
                border-radius: 4px;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }

                legend.scheduler-border {
                    font-size: 1em !important;
                    text-align: left !important;
                    font-style: oblique;
                    width:auto;
                    padding:0 10px;
                    border-radius: 4px;
                    background-color: #ddd;
                }

                legend.scheduler-border:focus {
                    font-size: 2em !important;
                }

                fieldset.has-focus legend{
                    background-color: #f6f6f6;
                }
            "
        );

        $this->registerJs(
            '
            $("input").focus(function (e) {
                console.log("select2:focus", e);
                $(e.currentTarget).parents("fieldset").addClass("has-focus");
            });
            $("input").blur(function (e) {
                $("fieldset.has-focus").removeClass("has-focus");
            });
        '
        );
        ?>

    </div>

    <?= $form->field($model, 'postTitle')->textInput(['maxlength' => true, 'placeholder' => 'Название статьи']) ?>
    <?= $form->field($model, 'postImage')->label('Изображение от 800x500 и выше')->widget(
        \sjaakp\illustrated\Uploader::className(),
        [
            'id' => 'wCropper',
            'cropperOptions' => [
                'diagonal' => 849,
                'sliderPosition' => "right"
            ]
        ]
    ) ?>

    <?= $form->field($model, 'postLead')->textarea(['rows' => 6, 'placeholder' => 'Подзаголовок']) ?>

    <?= $form->field($model, 'postText')->widget(
        yii\imperavi\Widget::className(),
        [
            'options' => [
                'placeholder' => 'Текст статьи',
                'imageUpload' => \yii\helpers\Url::to(['post/image-upload']),
                'imageEditable' => true,
                'imageResizable' => false,
                'buttonsHide' => ['outdent', 'indent'],
                'formatting' => ['p', 'blockquote', 'h2', 'h3', 'pre'],
                'uploadImageFields' => ['_csrf' => new JsExpression('$(\'meta[name="csrf-token"]\').attr("content")')]
            ],
            'plugins' => ['fullscreen']
        ]
    ); ?>

    <?= $form->field($model, 'postVideo')->textarea(['rows' => 6, 'placeholder' => 'Код вставки видео']) ?>

    <?= $form->field($model, 'postSourceUrl')->textInput(['maxlength' => true, 'placeholder' => 'Урл источника']) ?>


    <?php
    //    echo $form->field($model, 'postRel')->radioList(
    //        ['follow' => 'follow', 'no-follow' => 'no-follow'],
    //        ['separator' => '<br>']
    //    );
    ?>

    <?php

    if ($model->postImage) {
        $this->registerJs(
            '$("#wCropper .cropper").cropper("loadImage", "' . \Yii::$app->params['mediaBaseUrl'] . '/post/' . $model->postImage . '")'
        );
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php

    $this->registerCss(
        '
        .redactor-editor > hr {
            padding: 0;
            border: none;
            border-top: 4px double #6495ED;
            color: #333;
            text-align: center;
        }
        .redactor-editor>hr:after {
            content: "🔒";
            display: inline-block;
            position: relative;
            top: -0.5em;
            font-size: 2em;
            padding: 0 0.25em;
            background: white;
        }
        .field-post-posttext .redactor-editor{
            height: 500px!important;
        }

        .redactor-editor pre {
            background: #FFE0E0;
            border: 1px solid #ddd;
            border: 1px solid #F44336;

            overflow: auto;
            overflow-x: auto; /* Use horizontal scroller if needed; for Firefox 2, not
            white-space: pre-wrap; /* css-3 */
            white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
            word-wrap: break-word; /* Internet Explorer 5.5+ */
            white-space : normal; /* crucial for IE 6, maybe 7? */
        }

        .redactor-editor pre::before {
            content: "script";
            display: inline;
            padding: .2em .6em .3em .7em;
            font-size: 1em;
            font-weight: bold;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
            background-color: #d9534f;
            position: absolute;
            top:1em;
        }
    '
    );

    ?>

</div>
