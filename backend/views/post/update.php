<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */

$this->title = $model->isNewRecord ? 'Новая статья' : 'Редактирование статьи';
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="post-update" xmlns="http://www.w3.org/1999/html">

    <h1><?= Html::encode($this->title) ?><?= in_array(
            $model->postStatus,
            [
                \common\models\Post::FUTURE,
                \common\models\Post::PUBLISH
            ]
        ) ? ' <a class="btn btn-info" target="_blank" href="' . Yii::$app->params['frontendBaseUrl'] . '/' . $model->primaryKey . '">просмотреть</a>' : '' ?></h1>

    <?= $this->render(
        '_form',
        [
            'model' => $model,
        ]
    ) ?>

</div>
