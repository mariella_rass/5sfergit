<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;

$listLayout = "
            <table class='table'>
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Автор</th>
                    <th>Статус</th>
                    <th style='min-width: 120px'>Время</th>
                </tr>
                </thead>
                <tbody>
                    {items}\n
                </tbody>
            </table>
            {pager}
";
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-8">
            <?= ListView::widget(
                [
                    'layout' => $listLayout,
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_item',
                ]
            ) ?>
        </div>
        <div class="col-md-4">
            <p>
                <?= Html::a('Создать статью', ['create'], ['class' => 'btn btn-lg btn-block btn-success']) ?>
            </p>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <br>
            <p class="text-center">
                для быстрого перехода в админку с фронтенда, перетащите эту кнопку на панель закладок
            </p>
            <p class="text-center">
                <a href='javascript:location.href = location.href.replace("http://5sfer.com/","http://backend.5sfer.com/post/update?id=")'
                   type="button" class="btn btn-info btn-xs" onclick="return false;">
                    <span class="glyphicon glyphicon-pencil"></span> Редактировать статью</a>
            </p>
        </div>
    </div>

</div>
