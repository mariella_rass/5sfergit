<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="category-form well">

    <legend>Фильтры</legend>

    <?php

    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_VERTICAL,
//            'enableClientValidation' => true,
//            'enableClientScript' => true,
            'action' => ['index'],
            'method' => 'get',
        ]
    );

    $cityDesc = empty($model->userId) ? '' : \common\models\Profile::findOne($model->userId)->name;

    $mainRow = [
        'attributes' => [
            'postId' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => '']],
            'postTitle' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => '']],
            'postStatus' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                //'draft','approval','future','publish','archive'
                'items' => [
                    '' => 'Все',
                    $model::DRAFT => 'Черновики',
                    $model::APPROVAL => 'Ожидают проверки',
                    $model::FUTURE => 'Запланированные',
                    $model::PUBLISH => 'Опубликованные',
                    $model::ARCHIVE => 'Архивные',
                ],
                'options' => ['placeholder' => 'статус']
            ],
            'categoryId' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'options' => ['placeholder' => 'Категория'],
                    'data' => \yii\helpers\ArrayHelper::map(
                        \common\models\Category::find()->all(),
                        'categoryId',
                        'categoryName'
                    ),
                    'pluginOptions' => [
                        'multiple' => false,
                        'minimumInputLength' => 0,
                    ],
                ]
            ],
            'userId' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'initValueText' => $cityDesc, // set the initial display text
                    'options' => ['placeholder' => 'Выберите ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['author/authorlist']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],

                ],
            ]
        ]
    ];

    $actionRow = [
        'attributes' => [
            'actions' => [    // embed raw HTML content
                'type' => Form::INPUT_RAW,
                'value' => Html::submitButton(
                        'Применить',
                        ['class' => 'btn btn-primary']
                    ) .
                    ' ' .
                    Html::a(
                        'Сбросить',
                        \yii\helpers\Url::to(['']),
                        ['class' => 'btn btn-default']
                    )
            ],
        ]
    ];

    echo FormGrid::widget(
        [
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [$mainRow, $actionRow]
        ]
    );

    ActiveForm::end();

    ?>

</div>
<div class="btn-group btn-group-justified sort-ordinal">
    <?= $model->sorter->link('popular', ['class' => 'btn btn-default']) . $model->sorter->link(
        'date',
        ['class' => 'btn btn-default']
    ) ?>
</div>