<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model backend\models\Post
 */
?>
<tr>
    <td>
        <div>
            <?php foreach ($model->categories as $cat): ?>
                <span class="small" style="margin-right: 5px">
                        <b><?= $cat->title ?></b>
                    </span>
            <?php endforeach ?>
        </div>
        <div><?= \yii\helpers\Html::a(
                $model->postTitle,
                \yii\helpers\Url::to(['update', 'id' => $model->primaryKey])
            ) ?></div>
    </td>
    <td>
        <?= \yii\helpers\Html::a(
            $model->user->profile->name,
            \yii\helpers\Url::to(['author/update', 'id' => $model->user->profile->user_id])
        ) ?>
    </td>
    <td>
        <span class="label label-<?= 'true' == 'true' ? 'primary' : 'info' ?>">
            <?= $model->postStatus ?>
        </span>
    </td>
    <td>
        <?= $model->postPublishTime ? strftime('%d-%m-%y&nbsp;%H:%M', $model->postPublishTime) : '' ?>
    </td>
</tr>