<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $newModel backend\models\Category */

$this->title = 'Разделы';
$this->params['breadcrumbs'][] = $this->title;

$listLayout = "
            <table class='table'>
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                    {items}\n
                </tbody>
            </table>
            {pager}
";
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-8">
            <?= ListView::widget(
                [
                    'layout' => $listLayout,
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_item',
                    'itemOptions' => ['tag' => false]
                ]
            ) ?>
        </div>
        <div class="col-md-4">
            <?= $this->render('_form', ['model' => $newModel]) ?>
        </div>
    </div>
</div>
