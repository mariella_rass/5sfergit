<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form well">

    <?= $model->isNewRecord ? '<legend>Новый раздел</legend>' : '' ?>

    <?php

    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableClientValidation' => true,
            'enableClientScript' => true,
            'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
        ]
    );

    $mainRow = [
        'attributes' => [
            'categoryName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Название категори']],
        ]
    ];

    if (!$model->isNewRecord) {
        $mainRow['attributes']['categoryStatus'] = [
            'type' => Form::INPUT_RADIO_LIST,
            'items' => [
                'on' => 'Активна',
                'off' => 'Выключена',
            ],
            'options' => ['placeholder' => 'Название категори']
        ];
        $mainRow['attributes']['categoryAppendHtml'] = [
            'type' => Form::INPUT_TEXTAREA,
            'options' => ['placeholder' => 'Код формы для статей категории']
        ];
    }

    $seoRow = [
        'contentBefore' => '<legend class="text-info"><small>SEO</small></legend>',
        'attributes' => [
            'categoryTitle' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Seo title']],
            'categoryDescription' => [
                'type' => Form::INPUT_TEXTAREA,
                'options' => ['placeholder' => 'Seo description']
            ],
//            'categoryRel' => [
//                'type' => Form::INPUT_RADIO_LIST,
//                'items' => ['follow' => 'follow', 'no-follow' => 'no-follow',]
//            ]
        ]
    ];

    echo FormGrid::widget(
        [
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [$mainRow, $seoRow]
        ]
    );

    echo Html::submitButton(
        $model->isNewRecord ? 'Создать' : 'Сохранить',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );

    ActiveForm::end();

    ?>

</div>
