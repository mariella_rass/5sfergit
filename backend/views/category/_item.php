<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model backend\models\Category
 */
?>
<tr>
    <td>
        <?= \yii\helpers\Html::a($model->categoryName, \yii\helpers\Url::to(['update', 'id' => $model->primaryKey])) ?>
    </td>
    <td>
        <span class="label label-<?= $model->categoryEditable == 'true' ? 'primary' : 'info' ?>">
            <?= $model->categoryEditable == 'true' ? 'Спецпроект' : 'Раздел' ?>
        </span>
    </td>
    <td>
        <?= \kartik\switchinput\SwitchInput::widget(
            [
                'name' => 'status_1',
                'pluginOptions' => [
                    'size' => 'mini',
                    'onText' => 'Вкл.',
                    'offText' => 'Вык.',
                    'onColor' => 'success',
                    'offColor' => 'danger',
                ],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function(event, state) {
                        $.ajax({
                          type: 'POST',
                          url: '" . \yii\helpers\Url::to(['update', 'id' => $model->primaryKey]) . "',
                          data: {'Category[categoryStatus]': (state ? 'on' : 'off')},
                          success:function(){\$.notify({icon: 'glyphicon glyphicon-ok-sign', title:'Все ок!',message: 'Изменения сохранены!'},{type: 'success'})}
                        });
                    }",
                ],
                'value' => $model->categoryStatus == 'on' ? '1' : '0',
                'containerOptions' => ['class' => ''],
                'readonly' => $model->categoryEditable != 'true',
            ]
        ); ?>
    </td>
</tr>