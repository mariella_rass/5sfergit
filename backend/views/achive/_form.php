<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Achive */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="achive-form well">

    <?= $model->isNewRecord ? '<legend>Новый тренинг</legend>' : '' ?>

    <?php

    $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
            ]
        ]
    );

    echo $form->field($model, 'achiveTitle')->textInput(['maxlength' => true, 'placeholder' => 'Название']);
    echo $form->field($model, 'achiveDescription')->textarea(['placeholder' => 'Описание', 'rows' => 8]);
    echo $form->field($model, 'achiveUrl')->textInput(['maxlength' => true, 'placeholder' => 'Ссылка']);

    ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
