<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */
$this->context->layout = '/empty';
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<!-- Login -->
<div class="lc-block toggled" id="l-login">
    <?php $form = ActiveForm::begin(
        [
            'id' => 'login-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false
        ]
    ) ?>
    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-person"></i></span>

        <div class="fg-line">
            <?= $form->field(
                $model,
                'login',
                [
                    'template' => "{label}\n{input}\n{hint}\n",
                    'inputOptions' => [
                        'autofocus' => 'autofocus',
                        'class' => 'form-control',
                        'placeholder' => 'Логин',
                        'tabindex' => '1'
                    ]
                ]
            )->label(false); ?>
        </div>
    </div>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-accessibility"></i></span>

        <div class="fg-line">
            <!--                    <input type="password" class="form-control" placeholder="Password">-->
            <?= $form->field(
                $model,
                'password',
                [
                    'template' => "{label}\n{input}\n{hint}\n",
                    'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Пароль', 'tabindex' => '2']
                ]
            )->label(false)->passwordInput() ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="checkbox">
        <label>
            <?= $form->field(
                $model,
                'rememberMe',
                ['template' => "{label}\n{input}\n<i class=\"input-helper\"></i>{hint}\n{error}"]
            )->checkbox(
                ['tabindex' => '4', 'label' => 'Запомнить меня'],
                false
            ) ?>
        </label>
    </div>
    <?= $form->errorSummary($model, ['class' => 'text-left c-red']) ?>
    <button type="submit" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></button>
    <?php ActiveForm::end(); ?>

    <ul class="login-navigation">
        <li data-block="#l-register" class="bgm-red">Register</li>
        <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
    </ul>
</div>

<!-- Register -->
<div class="lc-block" id="l-register">
    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-person"></i></span>

        <div class="fg-line">
            <input type="text" class="form-control" placeholder="Username">
        </div>
    </div>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-mail"></i></span>

        <div class="fg-line">
            <input type="text" class="form-control" placeholder="Email Address">
        </div>
    </div>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-accessibility"></i></span>

        <div class="fg-line">
            <input type="password" class="form-control" placeholder="Password">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            <i class="input-helper"></i>
            Accept the license agreement
        </label>
    </div>

    <a href="" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></a>

    <ul class="login-navigation">
        <li data-block="#l-login" class="bgm-green">Login</li>
        <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
    </ul>
</div>

<!-- Forgot Password -->
<div class="lc-block" id="l-forget-password">
    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo
        lorem fringilla enim feugiat commodo sed ac lacus.</p>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="md md-email"></i></span>

        <div class="fg-line">
            <input type="text" class="form-control" placeholder="Email Address">
        </div>
    </div>

    <a href="" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></a>

    <ul class="login-navigation">
        <li data-block="#l-login" class="bgm-green">Login</li>
        <li data-block="#l-register" class="bgm-red">Register</li>
    </ul>
</div>
