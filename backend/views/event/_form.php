<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */

$author = empty($model->userId) ?
    \common\models\Profile::findOne(1)->name :
    \common\models\Profile::findOne($model->userId)->name;
?>

<div class="event-form well">

    <?= $model->isNewRecord ? '<legend>Новое событие</legend>' : '' ?>

    <?php

    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableClientValidation' => true,
            'enableClientScript' => true,
            'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->eventId]
        ]
    );

    $mainRow = [
        'attributes' => [
            'eventName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Название']],
            'userId' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'options' => ['placeholder' => 'Выберите ...'],
                    'initValueText' => $author,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['author/authorlist']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],

                ],
            ],
            'eventDate' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\datetime\DateTimePicker::className(),
                'options' => [
                    'options' => [
                        'value' => is_int($model->eventDate) ? strftime(
                            '%d-%m-%Y %H:%M',
                            $model->eventDate
                        ) : $model->eventDate
                    ],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy hh:ii',
                        'startDate' => strftime('%d-%m-%Y %H:%M', time()),
                        'todayHighlight' => true,
                    ]
                ]
            ],
            'eventUrl' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Ссылка']],
            'eventPaid' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => [
                    'free' => 'Бесплатное',
                    'paid' => 'Платное',
                ]
            ],
            'eventStatus' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'items' => [
                    'off' => 'Не активно',
                    'on' => 'Активно',
                ]
            ],
        ]
    ];

    if (!$model->isNewRecord) {
        $mainRow['attributes']['eventStatus'] = [
            'type' => Form::INPUT_DROPDOWN_LIST,
            'items' => [
                'off' => 'Не активно',
                'on' => 'Активно',
            ],
            'options' => ['placeholder' => 'Название категори']
        ];
    }

    echo FormGrid::widget(
        [
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [$mainRow]
        ]
    );

    echo Html::submitButton(
        $model->isNewRecord ? 'Создать' : 'Сохранить',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );

    ActiveForm::end();

    ?>

</div>
