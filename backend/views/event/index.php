<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $newModel backend\models\Category */

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;

$listLayout = "
            <table class='table'>
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Эксперт</th>
                    <th>Дата</th>
                    <th>Активность</th>
                </tr>
                </thead>
                <tbody>
                    {items}\n
                </tbody>
            </table>
            {pager}
";
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-8">
            <?= ListView::widget(
                [
                    'layout' => $listLayout,
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_item',
                    'itemOptions' => ['tag' => false]
                ]
            ) ?>
        </div>
        <div class="col-md-4">
            <p>
                <?= Html::a('Создать событие', ['create'], ['class' => 'btn btn-lg btn-block btn-success']) ?>
            </p>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
</div>
