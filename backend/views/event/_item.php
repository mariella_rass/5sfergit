<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model backend\models\Event
 */
?>
<tr>
    <td>
        <?= \yii\helpers\Html::a($model->eventName, \yii\helpers\Url::to(['event/update', 'id' => $model->eventId])) ?>
    </td>
    <td>
        <?= \yii\helpers\Html::a($model->user->profile->name, \yii\helpers\Url::to(['author/update', 'id' => $model->user->id])) ?>
    </td>
    <td>
        <?= $model->eventDate ? strftime('%d-%m-%y %H:%M', $model->eventDate) : ''?>
    </td>
    <td>
        <?= \kartik\switchinput\SwitchInput::widget(
            [
                'name' => 'status_1',
                'pluginOptions' => [
                    'size' => 'mini',
                    'onText' => 'Вкл.',
                    'offText' => 'Вык.',
                    'onColor' => 'success',
                    'offColor' => 'danger',
                ],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function(event, state) {
                        $.ajax({
                          type: 'POST',
                          url: '" . \yii\helpers\Url::to(['update', 'id' => $model->eventId]) . "',
                          data: {'Event[eventStatus]': (state ? 'on' : 'off')},
                          success:function(){\$.notify({icon: 'glyphicon glyphicon-ok-sign', title:'Все ок!',message: 'Изменения сохранены!'},{type: 'success'})}
                        });
                    }",
                ],
                'value' => $model->eventStatus == 'on' ? '1' : '0',
                'containerOptions' => ['class' => '']
            ]
        ); ?>
    </td>
</tr>