<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin(
        [
            'brandLabel' => '5 сфер',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]
    );
    $menuItems = [];
    if (Yii::$app->user->isGuest) {
        $menuItemsRight = [['label' => 'Войти', 'url' => ['/user/security/login']]];
    } else {
        if (Yii::$app->authManager->checkAccess(Yii::$app->user->identity->getId(), 'admin')) {
            $menuItems = [
                ['label' => 'Статьи', 'url' => ['/post/index']],
                ['label' => 'Статистика', 'url' => ['/stat/index']],
                ['label' => 'События', 'url' => ['/event/index']],
                ['label' => 'Авторы', 'url' => ['/author/index']],
                ['label' => 'Разделы', 'url' => ['/category/index']],
                ['label' => 'Тренинги', 'url' => ['/achive/index']],
            ];
        } elseif (Yii::$app->authManager->checkAccess(Yii::$app->user->identity->getId(), 'author')) {
            $menuItems = [
                ['label' => 'Статьи', 'url' => ['/author-post/index']],
                //['label' => 'Статистика', 'url' => ['/author-stat/index']],
            ];
        }
        $menuItemsRight = [
            [
                'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                'url' => ['/user/security/logout'],
                'linkOptions' => ['data-method' => 'post']
            ]
        ];
    }
    echo Nav::widget(
        [
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => $menuItems,
        ]
    );
    echo Nav::widget(
        [
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItemsRight,
        ]
    );
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
        <?= $content ?>
    </div>
</div>
<?php
$growl = Yii::$app->session->getFlash('growl', null, true);

if ($growl) {
    echo \kartik\widgets\Growl::widget(
        [
            'type' => $growl->type,
            'title' => $growl->title,
            'icon' => $growl->icon,
            'body' => $growl->body,
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'placement' => [
                    'from' => 'top',
                    'align' => 'right',
                ]
            ]
        ]
    );

}
$this->registerJs(
    "
$.notifyDefaults({
	template: '<div data-notify=\"container\" class=\"col-xs-11 col-sm-3 alert alert-{0}\" role=\"alert\">'+
    '   <button type=\"button\" aria-hidden=\"true\" class=\"close\" data-notify=\"dismiss\">×</button>'+
    '       <span data-notify=\"icon\"></span>'+
    '       <span data-notify=\"title\">{1}</span>'+
    '       <hr class=\"kv-alert-separator\">'+
    '      <span data-notify=\"message\">{2}</span>'+
    '      <div class=\"progress\" data-notify=\"progressbar\">'+
	'		<div class=\"progress-bar progress-bar-{0}\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 0%;\"></div>'+
	'	</div>'+
    '	<a href=\"{3}\" target=\"{4}\" data-notify=\"url\"></a>'+
	'</div>'
});
"
);
?>
<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= date('Y') ?> © Isaac Pintosevich Systems</p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
