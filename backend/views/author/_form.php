<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Author;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $profile common\models\Profile */

?>

<div class="user-form well">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
            ]
        ]
    ); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord) {
        echo $form->field($model->profile, 'name')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'nameGenitivus')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'rate')->label('Рейтинг')->widget(
            \kartik\widgets\StarRating::classname(),
            [
                'pluginOptions' => [
                    'step' => 1,
                    'size' => 'xs',
                    'starCaptions' => [
                        0 => 'Нет рейтинга',
                        1 => '1 звезда',
                        2 => '2 звезды',
                        3 => '3 звезды',
                        4 => '4 звезды',
                        5 => '5 звезд',
                    ],
                    'starCaptionClasses' => [
                        1 => 'text-danger',
                        1 => 'text-info',
                        2 => 'text-info',
                        3 => 'text-info',
                        4 => 'text-info',
                        5 => 'text-info',
                    ],
                ]
            ]
        );
        echo $form->field($model->profile, 'avatar')->widget(
            \sjaakp\illustrated\Uploader::className(),
            [
                'id' => 'wCropper',
                'cropperOptions' => [
                    'diagonal' => 141,
                    'sliderPosition' => "right"
                ]
            ]

        );
        echo $form->field($model->profile, 'bio')->textarea(['maxlength' => true]);

        echo $form->field($model, 'achives')->widget(
            \kartik\widgets\Select2::className(),
            [
                'options' => ['placeholder' => 'Пройденые тренинги'],
                'data' => \yii\helpers\ArrayHelper::map(
                    $model->achives,
                    'achiveId',
                    'achiveTitle'
                ),
                'pluginOptions' => [
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['achive/achivelist']),
                        'dataType' => 'json',
//                            'data' => new JsExpression('function(params) { return {q:params.category}; }')
                    ],
                    'allowClear' => true,
                    'multiple' => true,
                    'minimumInputLength' => 1,
                ],
            ]
        )->label('Пройденые тренинги');

        echo $form->field($model->profile, 'facebook')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'vk')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'twitter')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'youtube')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'gplus')->textInput(['maxlength' => true]);
        echo $form->field($model->profile, 'website')->textInput(['maxlength' => true]);

        echo $form->field($model->profile, 'postForm')->textarea(['maxlength' => true, 'rows' => 15]);

        echo $form->field($model->profile, 'subscribeDoneText')->widget(
            yii\imperavi\Widget::className(),
            [
                'options' => [
                    'placeholder' => 'Текст на странице подтверждения подписки',
                    'imageEditable' => true,
                    'imageResizable' => false,
                    'buttonsHide' => ['hr', 'outdent', 'indent'],
                    'formatting' => ['p', 'blockquote', 'h2', 'h3'],
                ],
                'plugins' => [
                    'fullscreen'
                ]
            ]
        );

        echo $form->field($model->profile, 'triggerComment')->dropDownList(
            [
                'on' => 'Включены',
                'off' => 'Выключены'
            ]
        );

        echo $form->field($model->profile, 'status')->dropDownList(
            [
                Author::WAIT => 'Не активен',
                Author::APPROVED => 'Активен',
                Author::STAFF => 'Служебный'
            ],
            array_merge(
                [
                    'options' => [Author::STAFF => ['disabled' => true]],
                ],
                $model->profile->status == Author::STAFF ? ['disabled' => 'disabled'] : []
            )
        );
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    if (!$model->isNewRecord && $model->profile->avatar) {
        $this->registerJs(
            '$("#wCropper .cropper").cropper("loadImage", "' . \Yii::$app->params['mediaBaseUrl'] . '/user/avatar/' . $model->profile->avatar . '")'
        );
    }
    $this->registerCss('.sjaakp-overlay{border-radius:100%;border-color: rgba(0, 255, 0, .75)!important;}')
    ?>

</div>
