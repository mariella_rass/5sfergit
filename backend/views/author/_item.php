<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model common\models\Author
 */
?>

<tr>
    <td>
        <div class="media">
            <div class="media-left">
                <a href="<?= \yii\helpers\Url::to(['update', 'id' => $model->primaryKey]) ?>">
                    <?= $model->profile->getImgHtml(
                        'avatar',
                        0,
                        false,
                        [
                            'class' => "media-object",
                            'style' => "width: 64px; height: 64px;border-radius: 50% !important;"
                        ]
                    ) ?>
                </a>
            </div>
            <div class="media-left">
                <h4 class="media-heading"><?= $model->profile->name ?></h4>
            </div>
        </div>
    </td>
    <td>
        <span class="label label-<?= isset($model->profile->status) ? [
            \common\models\Author::APPROVED => 'primary',
            \common\models\Author::WAIT => 'info',
            \common\models\Author::STAFF => 'warning'
        ][$model->profile->status] : 'info' ?>">
            <?= isset($model->profile->status) ? $model->profile->status : 'not set' ?>
        </span>
    </td>
    <td>
        <?= \kartik\switchinput\SwitchInput::widget(
            [
                'name' => 'status_2',
                'pluginOptions' => [
                    'size' => 'mini',
                    'onText' => 'Вкл.',
                    'offText' => 'Вык.',
                    'onColor' => 'success',
                    'offColor' => 'danger',
                ],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function(event, state) {
                        $.ajax({
                          type: 'POST',
                          url: '" . \yii\helpers\Url::to(['block', 'id' => $model->primaryKey]) . "',
                          data: {},
                          success:function(){\$.notify({icon: 'glyphicon glyphicon-ok-sign', title:'Все ок!',message: 'Изменения сохранены!'},{type: 'success'})}
                        });
                    }",
                ],
                'value' => $model->blocked_at ? '0' : '1',
                'containerOptions' => ['class' => '']
            ]
        ); ?>
    </td>
</tr>