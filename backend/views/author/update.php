<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Редактирование автора';
$this->params['breadcrumbs'][] = ['label' => 'Авторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?><?= in_array(
                $model->profile->status,
                [
                    \common\models\Author::APPROVED,
                    \common\models\Author::STAFF,
                    \common\models\Author::WAIT,
                ]
            ) ? ' <a class="btn btn-info" target="_blank" href="' . Yii::$app->params['frontendBaseUrl'] . '/author/' . $model->profile->slug . '">просмотреть</a>' : '' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
