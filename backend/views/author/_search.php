<?php

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<br>
<div class="category-form well">

    <legend>Фильтры</legend>

    <?php

    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_VERTICAL,
//            'enableClientValidation' => true,
//            'enableClientScript' => true,
            'action' => ['index'],
            'method' => 'get',
        ]
    );

    $mainRow = [
        'attributes' => [
            'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'somename@somedomain.com']],
            'profile_name' => ['type' => Form::INPUT_TEXT, 'label' => 'Имя', 'options' => ['placeholder' => 'Ицхак']],
        ]
    ];

    $actionRow = [
        'attributes' => [
            'actions' => [    // embed raw HTML content
                'type' => Form::INPUT_RAW,
                'value' => Html::submitButton(
                        'Применить',
                        ['class' => 'btn btn-primary']
                    ) .
                    ' ' .
                    Html::a(
                        'Сбросить',
                        \yii\helpers\Url::to(['']),
                        ['class' => 'btn btn-default']
                    )
            ],
        ]
    ];

    echo FormGrid::widget(
        [
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [$mainRow, $actionRow]
        ]
    );

    ActiveForm::end();

    ?>

</div>
