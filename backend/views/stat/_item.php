<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model common\models\Author
 */
?>

<tr>
    <td>
        <?= \yii\helpers\Html::a(($index+1). '. ' . $model->profile->name, \yii\helpers\Url::to(['author/update', 'id' => $model->primaryKey])) ?>
    </td>
    <td>
        <?= $model->profile->popularity1 ?>
    </td>
    <td>
        <?= $model->profile->popularity7 ?>
    </td>
    <td>
        <?= $model->profile->popularity30 ?>
    </td>
</tr>

