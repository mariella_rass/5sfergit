<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика просмотров';
$this->params['breadcrumbs'][] = $this->title;

$listLayout = "
            <table class='table'>
                <thead>
                <tr>
                    <th>Название</th>
                    <th class='sort-ordinal'>" . $searchModel->sorter->link('popularity1', ['class' => 'btn btn-default']) . "</th>
                    <th class='sort-ordinal'>" . $searchModel->sorter->link('popularity7', ['class' => 'btn btn-default']) . "</th>
                    <th class='sort-ordinal'>" . $searchModel->sorter->link('popularity30', ['class' => 'btn btn-default']) . "</th>
                </tr>
                </thead>
                <tbody>
                    {items}\n
                </tbody>
            </table>
            {pager}
";
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-8">
            <?= ListView::widget(
                [
                    'layout' => $listLayout,
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_item',
                ]
            ) ?>
        </div>
        <div class="col-md-4">
            <p>
                <?= Html::a('Создать автора', ['create'], ['class' => 'btn btn-lg btn-block btn-success']) ?>
            </p>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

</div>
