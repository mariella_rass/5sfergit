<div class="row">
    <div class="col-xs-12" id="chart">
        1
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.07.15
 * Time: 16:26
 *
 * @var $this yii\web\View
 * @var $model common\models\User
 *
 */

\backend\assets\HighchartsAsset::register($this);

$userId = $model->primaryKey;
$userId = 119;

$positions7 = \common\models\Position::find()
    ->where(['user_id' => $userId])
    ->andWhere(['type' => '7'])
    ->andWhere('date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')
    ->orderBy(['date' => SORT_ASC])
    ->asArray()
    ->all();

$positions30 = \common\models\Position::find()
    ->where(['user_id' => $userId])
    ->andWhere(['type' => '30'])
    ->andWhere('date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')
    ->orderBy(['date' => SORT_ASC])
    ->asArray()
    ->all();

$date = json_encode(array_column($positions7, 'date'));
$pos7 = json_encode(
    array_map(
        function ($v) {
            return (int)$v;
        },
        array_column($positions7, 'position')
    )
);
$pos30 = json_encode(
    array_map(
        function ($v) {
            return (int)$v;
        },
        array_column($positions30, 'position')
    )
);

$this->registerJs(
    "
$(function () {
    $('#chart').highcharts({
        title: {
            text: 'Позиция за прошедшие 30 дней',
            x: -20 //center
        },
        xAxis: {
            categories: $date,
        },
        yAxis: {
            title: {
                text: 'Позиция'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
            ,
            reversed: true,
            showFirstLabel: false,
            showLastLabel: true
        },
        tooltip: {
            valueSuffix: ' место',
            shared: true,
            crosshairs: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '7 дней',
            data: $pos7
        }, {
            name: '30 дней',
            data: $pos30
        }]
    });
});
"
);

$profile = \common\models\Profile::findOne($userId);

$popularity7 = \common\models\Profile::find()
    ->select(['popularity7'])
    ->where(['status' => \common\models\Author::APPROVED])
    ->orderBy(['popularity7' => SORT_DESC])
    ->limit(10)
    ->asArray()
    ->column();

$popularity30 = \common\models\Profile::find()
    ->select(['popularity30'])
    ->where(['status' => \common\models\Author::APPROVED])
    ->orderBy(['popularity30' => SORT_DESC])
    ->limit(10)
    ->asArray()
    ->column();

?>
<div class="row">
    <div class="col-xs-6">
        <?php
        $poularity = $profile->popularity7;
        ?>
        <?= $poularity ?><br>
        <?= $popularity7[0]/$poularity ?><br>
        <?= $popularity7[2]/$poularity ?><br>
        <?= $popularity7[9]/$poularity ?><br>
    </div>
    <div class="col-xs-6">
        <?php
        $poularity = $profile->popularity30;
        ?>
        <?= $poularity ?><br>
        <?= $popularity30[0]/$poularity ?><br>
        <?= $popularity30[2]/$poularity ?><br>
        <?= $popularity30[9]/$poularity ?><br>
    </div>
</div>