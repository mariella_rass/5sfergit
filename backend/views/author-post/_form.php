<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */
/* @var $form yii\widgets\ActiveForm */

$author = empty($model->userId) ?
    \common\models\Profile::findOne(1)->name :
    \common\models\Profile::findOne($model->userId)->name;

$category = [
    ['id' => 1, 'text' => 11, 'selected' => 'selected'],
    ['id' => 2, 'text' => 12, 'selected' => 'selected']
];

?>


<?php if ($model->postStatus == \common\models\Post::DRAFT && !empty($model->postRedactorComment)): ?>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Комментарий
                        редактора:</a></h3>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <p><?= $model->postRedactorComment ?></p>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="post-form well">

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
            ]
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <div class="row">

        <div class="col-xs-12">

            <?= $form->field($model, 'postStatus')->dropDownList(
                [
                    $model::DRAFT => 'Черновик',
                    $model::APPROVAL => 'На проверку',
                ],
                [
                    'placeholder' => 'статус',
                ]
            ) ?>

        </div>

    </div>

    <?= $form->field($model, 'postTitle')->textInput(['maxlength' => true, 'placeholder' => 'Название статьи']) ?>

    <?= $form->field($model, 'postLead')->textarea(['rows' => 6, 'placeholder' => 'Подзаголовок']) ?>

    <?= $form->field($model, 'postText')->widget(
        yii\imperavi\Widget::className(),
        [
            'options' => [
                'placeholder' => 'Текст статьи',
                'imageUpload' => \yii\helpers\Url::to(['image-upload']),
                'imageEditable' => true,
                'imageResizable' => false,
                'source' => false,
                'buttonsHide' => ['hr', 'alignment', 'outdent', 'indent'],
                'formatting' => ['p', 'blockquote', 'h2', 'h3'],
//                'formattingAdd' => [
//                    ['tag' => 'p', 'title' => 'red block', 'class' => 'color-red']
//                ],
                'uploadImageFields' => [
                    '_csrf' => new JsExpression('$(\'meta[name="csrf-token"]\').attr("content")')
                ]
            ],
            'plugins' => [
                'fullscreen',
            ]
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Создать' : 'Сохранить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
