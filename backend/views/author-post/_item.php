<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:41
 *
 * @var $model backend\models\Post
 */
?>
<tr>
    <td>
        <?= \yii\helpers\Html::a($model->postTitle, \yii\helpers\Url::to(['update', 'id' => $model->primaryKey])) ?>
    </td>
    <td>
        <?= $model->postStatus == \common\models\Post::DRAFT ? (!empty($model->postRedactorComment) ? '<span class="label label-danger">Требуется доработка</span>' : '<span class="label label-info">Черновик</span>') : '<span class="label label-primary">На проверке</span>' ?>
    </td>
</tr>