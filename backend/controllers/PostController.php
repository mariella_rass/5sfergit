<?php

namespace backend\controllers;

use backend\components\BackendController;
use backend\models\PostSearch;
use common\models\Category;
use common\models\Post;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UnprocessableEntityHttpException;
use yii\web\UnsupportedMediaTypeHttpException;
use darkdrim\simplehtmldom\SimpleHTMLDom;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
// use Imagine\Exception;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends BackendController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['admin']
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
//        if (!\Yii::$app->user->can('admin')) {
//            $searchModel->query = $searchModel->query->andWhere(['userId' => Yii::$app->user->id]);
//        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post(['scenario' => 'create']);
        $model->userId = $model->userId ? $model->userId : Yii::$app->getUser()->id;

        // if 5sfer
        if ($model->userId == 77 && !$model->editorId && Yii::$app->authManager->checkAccess(
                Yii::$app->user->identity->getId(),
                'admin'
            )
        ) {
            $model->editorId = Yii::$app->getUser()->id;
        }
        if ($model->load(Yii::$app->request->post())) {

      
           if ( isset($model->postText) ){
                $pattern = "/(<img.*?src=\".*?)(uploads\/(?!thumb))(.*?\".*?>)/im";
                $replace = "$1uploads/thumb-$3";
                $result = preg_replace($pattern, $replace, $model->postText);
                $result = preg_replace('/alt=".*?"/',"", $result);
                $result = str_replace('<img','<img alt="'.$model->postTitle.'"', $result);
                $model->postText =  $result;
            }

           if($model->save()){
                $model->linkCategories(Yii::$app->request->post('Post')['categories']);
                $model->linkTags(Yii::$app->request->post('Post')['tags']);
                if( isset($model->postImage)){
                      try{
                            $path = "/usr/share/nginx/5sfer/media/post/";
                            // $path = "/home/bigdig/5sfer.bigdig.com.ua/www/media/post/";
                            $filename = $model->postImage;
                            $photo = Image::getImagine()->open($path .$filename);
                            $box = new Box(800,500);
                            $photo->thumbnail($box)->save($path ."thumb-".$filename, ['quality' => 70]);
                     }catch(Exception $e){
                            echo "not valid img! because of: ".$e;
                            continue;
                        }
                }
                $this->alert('Статья создана!');
                return $this->redirect(['update', 'id' => $model->postId]);
           }

        }
        Yii::trace($model->errors);
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );

    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['post' => $model])) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $model->scenario = 'update';
        if ($model->load(Yii::$app->request->post())) {
            if ( isset($model->postText) ){
                $pattern = "/(<img.*?src=\".*?)(uploads\/(?!thumb))(.*?\".*?>)/im";
                $replace = "$1uploads/thumb-$3";
                $result = preg_replace($pattern, $replace, $model->postText);
                $result = preg_replace('/alt=".*?"/',"", $result);
                $result = str_replace('<img','<img alt="'.$model->postTitle.'"', $result);
                $model->postText =  $result;
            }

            if ( $model->save() ){
                $model->linkCategories(Yii::$app->request->post('Post')['categories']);
                $model->linkTags(Yii::$app->request->post('Post')['tags']);
          
                $this->alert('Изменения сохранены!');


                  if( isset($model->postImage)){
                      try{
                            $path = "/usr/share/nginx/5sfer/media/post/";
                            // $path = "/home/bigdig/5sfer.bigdig.com.ua/www/media/post/";
                            $filename = $model->postImage;
                            $photo = Image::getImagine()->open($path .$filename);
                            $box = new Box(800,500);
                            $photo->thumbnail($box)->save($path ."thumb-".$filename, ['quality' => 70]);
                     }catch(Exception $e){
                            echo "not valid img! because of: ".$e;
                            continue;
                        }
                }
            }
        }

    
        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );

    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageUpload()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        // files storage folder
        $path = Yii::getAlias('@media') . '/uploads/';

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        // setting file's mysterious name
        $filename = md5(mt_rand()) . '.jpg';
        $file = $path . $filename;

        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {
            // copying
            $moved = move_uploaded_file($_FILES['file']['tmp_name'], $file);
    
            // crazy test
            if ($moved && file_exists($file) && filesize($file) > 0) {

                $photo = Image::getImagine()->open($path .$filename);
                $box = new Box(720,505);
                $photo->thumbnail($box)->save($path ."thumb-".$filename, ['quality' => 70]);

                echo shell_exec('convert ' . $file . ' -resize \'720>\' ' . $file . ' >/dev/null 2>/dev/null');
                if (exif_imagetype($file) == IMAGETYPE_JPEG) {
                    echo shell_exec('jpegoptim -p -m80 --strip-all ' . $file . ' >/dev/null 2>/dev/null');
                } elseif (exif_imagetype($file) == IMAGETYPE_PNG . ' >/dev/null 2>/dev/null') {
                    echo shell_exec('optipng ' . $file);
                } else {
                    unlink($file);
                    throw new UnsupportedMediaTypeHttpException("Unrecognized image format");
                }
            } elseif (file_exists($file)) {
                unlink($file);
                throw new UnprocessableEntityHttpException("Empty file recived");
            } else {
                throw new UnprocessableEntityHttpException("Unrecognized upload");
            }

            chmod($file, 0644);

            // displaying file
            $out['filelink'] = '//' . Yii::$app->params['imageHost'] . '/uploads/thumb-' . $filename;
            return $out;
        } else {
            throw new UnprocessableEntityHttpException("Unrecognized upload");
        }

    }

}
