<?php

namespace backend\controllers;

use backend\components\BackendController;
use backend\models\PostSearch;
use common\models\Author;
use common\models\Category;
use common\models\Post;
use dektrium\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class AuthorStatController extends BackendController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['author']
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = User::findOne(Yii::$app->user->id);

        return $this->render(
            'index',
            [
                'model' => $model
            ]
        );
    }



}
