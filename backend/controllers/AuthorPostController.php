<?php

namespace backend\controllers;

use backend\components\BackendController;
use backend\models\PostSearch;
use common\models\Category;
use common\models\Post;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class AuthorPostController extends BackendController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['author']
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch(['scenario' => 'author-search']);
       if (!\Yii::$app->user->can('admin')) {
           $searchModel->query = $searchModel->query->andWhere(['userId' => Yii::$app->user->id]);
       }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post(['scenario' => 'author-create']);
        $model->userId = $model->userId ? $model->userId : Yii::$app->getUser()->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->postStatus == Post::DRAFT) {
                $this->alert('Статья создана!');
                return $this->redirect(['update', 'id' => $model->postId]);
            } else {
                $this->alert('Статья отправлена на проверку!');
                return $this->redirect(['index']);
            }
        }
        Yii::trace($model->errors);
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );

    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['post' => $model])) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $model->scenario = 'author-update';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->postStatus == Post::DRAFT) {
                $this->alert('Изменения сохранены!');
                return $this->redirect(['update', 'id' => $model->postId]);
            } else {
                $this->alert('Статья отправлена на проверку!');
                return $this->redirect(['index']);
            }
        }

        if ($model->postStatus != Post::DRAFT){
            throw new ForbiddenHttpException("Вы не можете редактировать статью отправленную на проверку");
        }

        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );

    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Post::find()->where(['postStatus' => [Post::DRAFT, Post::APPROVAL]])->andWhere(['postId' => $id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageUpload()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        // files storage folder
        $path = Yii::getAlias('@media') . '/uploads/';

        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        // setting file's mysterious name
        $filename = md5(date('YmdHis')) . '.jpg';
        $file = $path . $filename;

        // copying
        move_uploaded_file($_FILES['file']['tmp_name'], $file);

        // test
        if (file_exists($file) && filesize($file) > 0) {
            echo shell_exec('convert ' . $file . ' -resize \'720>\' ' . $file . ' >/dev/null 2>/dev/null');
            if (exif_imagetype($file) == IMAGETYPE_JPEG) {
                echo shell_exec('jpegoptim -p -m80 --strip-all ' . $file . ' >/dev/null 2>/dev/null');
            } elseif (exif_imagetype($file) == IMAGETYPE_PNG . ' >/dev/null 2>/dev/null') {
                echo shell_exec('optipng ' . $file);
            } else {
                unlink($file);
                Yii::$app->end(200);
            }
        } else {
            unlink($file);
            Yii::$app->end(200);
        }

        chmod($file, 0644);

        // displaying file
        $out['filelink'] = '//' . Yii::$app->params['imageHost'] . '/uploads/' . $filename;
        return $out;

    }

}
