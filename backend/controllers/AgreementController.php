<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.12.15
 * Time: 19:24
 */

namespace backend\controllers;


use backend\components\BackendController;
use common\models\Author;
use yii\filters\AccessControl;

class AgreementController extends BackendController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['author']
                        ],
                    ],
                ],
            ]
        );
    }

    public function actionIndex()
    {
        if (\Yii::$app->request->post('agree', false)){
            $author = Author::findOne(\Yii::$app->user->id);
            $author->agreement = 1;
            $author->save(false);
            $this->redirect(['/']);
        } else {
            return $this->render('index');
        }
    }
}