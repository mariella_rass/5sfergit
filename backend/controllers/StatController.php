<?php

namespace backend\controllers;

use backend\components\BackendController;
use common\models\Author;
use common\models\AuthorSearch;
use kartik\widgets\Growl;
use Yii;
use common\models\User;
use common\models\UserSearch;

use yii\db\Query;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class StatController extends BackendController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthorSearch();
        $searchModel->query->andWhere(['status' => Author::APPROVED]);
        $searchModel->pagination = false;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new AuthorSearch();
        $searchModel->query->andWhere(['status' => Author::APPROVED]);
        $searchModel->pagination = false;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Author::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = Yii::createObject(
            [
                'class' => \dektrium\user\models\User::className(),
                'scenario' => 'create',
            ]
        );

        if ($model->load(Yii::$app->request->post()) && $model->create()) {

            $auth = Yii::$app->authManager;
            $auth->assign($auth->getRole('author'), $model->primaryKey);

            $this->alert('Пользователь создан');
            return $this->redirect(['update', 'id' => $model->primaryKey]);
        }

        return $this->render(
            'create',
            [
                'model' => $model
            ]
        );

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post())
            && $model->profile->load(Yii::$app->request->post())
            && $model->profile->validate()
            && $model->save()
            && $model->profile->save()
        ) {
            $this->alert('Изменения сохранены');
        }
        return $this->render(
            'update',
            [
                'model' => $model
            ]
        );

    }

    public function actionBlock($id)
    {
        if ($id == Yii::$app->user->getId()) {
            Yii::$app->getSession()->setFlash('danger', Yii::t('user', 'You can not block your own account'));
        } else {
            $user = $this->findModel($id);
            if ($user->getIsBlocked()) {
                $user->unblock();
            } else {
                $user->block();
            }
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }


    public function actionAuthorlist($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('user_id as id, name AS text')
                ->from('profile')
                ->where(['like', 'profile.name', $q]);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Profile::find($id)->name];
        }
        return $out;
    }
}
