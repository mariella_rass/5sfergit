<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.05.15
 * Time: 13:07
 */

namespace backend\controllers;


use common\models\Author;
use common\models\base\Position;
use common\models\base\View;
use common\models\Category;
use common\models\Tag;
use common\models\ViewLog;
use dektrium\user\models\Profile;
use common\models\Post;
use common\models\Term;
use yii\web\Controller;
use dektrium\user\models\User;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;
use Yii;
/**
 * Class ImportController
 * @package console\controllers
 *
 * @property \yii\db\Connection importdb
 */
class ImageController extends Controller
{
   

    public function init()
    {
        return parent::init();
    }

    public function actionStartOptim()
    {
        // Yii::warning(__DIR__);
        // die();
        // Yii::warning(realpath("../../media/post"));
        
        set_time_limit(50000000);
        $this->ImageOptimize();
    }

    public function ImageOptimize()
    {
        // $i = 0;
        $path = "../../media/post/";
        // $path = "../../media/uploads/";
        $files = scandir("../../media/post/");
        // $files = scandir("../../media/uploads/");
        foreach ($files as $f ) {
            // if( $i == 50 ) break;
            if($f != '.' && $f != '..' && (!strpos($f ,"thumb-"))){
                // Yii::warning( "path now is".$f );
               //  if(is_dir($path .'/'.$f)) 
               // {
                
               // }

                $photo = Image::getImagine()->open($path .$f);
                $box = new Box(720,505);
                $photo->thumbnail($box)->save($path ."thumb-".$f, ['quality' => 70]);
                Yii::warning( "original: ".$path .$f." optimized: ".$path .'/'.$f."-thumbnail");
                // $i++;
             }
        }



    }

  
}