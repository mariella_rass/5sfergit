<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.06.15
 * Time: 17:26
 */

namespace backend\controllers;


use backend\components\BackendController;
use common\models\Tag;
use yii\db\Query;
use yii\web\HttpException;

class TagController extends BackendController
{
    public function actionTaglist($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Tag::find()
                ->select(['tagId as id', 'tagName as text'])
                ->andWhere(['like', 'tagName', $q])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Tag::find($id)->name];
        }
        return $out;
    }
}