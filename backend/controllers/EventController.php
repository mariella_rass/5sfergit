<?php

namespace backend\controllers;

use backend\components\BackendController;
use common\models\EventSearch;
use Yii;
use common\models\Event;
use yii\web\NotFoundHttpException;


/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends BackendController
{
    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]
        );
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->alert('Событие создано!');
            return $this->redirect(['index']);
        } else {
            Yii::trace($model->errors);
            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                $model->save(true, ['eventStatus']);
            }
            print_r($model->errors);
            Yii::$app->end(200);
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $this->alert('Изменения сохранены!');
            }
        }

        return $this->render(
            'update',
            [
                'model' => $model
            ]
        );

    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
