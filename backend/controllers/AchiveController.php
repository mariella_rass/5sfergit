<?php

namespace backend\controllers;

use backend\components\BackendController;
use Yii;
use common\models\Achive;
use common\models\AchiveSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AchiveController implements the CRUD actions for Achive model.
 */
class AchiveController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Achive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Creates a new Achive model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Achive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->alert('Тренинг создан!');
            return $this->redirect(['index']);
        } else {
            Yii::trace($model->errors);
            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing Achive model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->alert('Изменения сохранены!');
        }

        return $this->render(
            'update',
            [
                'model' => $model
            ]
        );

    }

    public function actionAchivelist($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Achive::find()
                ->select(['achiveId as id', 'achiveTitle as text'])
                ->andWhere(['like', 'achiveTitle', $q])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Achive::find($id)->name];
        }
        return $out;
    }

    /**
     * Finds the Achive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Achive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Achive::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
