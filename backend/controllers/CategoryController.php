<?php

namespace backend\controllers;

use backend\components\BackendController;
use kartik\growl\Growl;
use Yii;
use backend\models\Category;
use backend\models\CategorySearch;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends BackendController
{

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $newModel = new \backend\models\Category(['scenario' => 'create']);

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'newModel' => $newModel
            ]
        );
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->alert('Раздел создан');
            return $this->redirect(['index', 'id' => $model->categoryId]);
        } else {
            return $this->render(
                'create',
                [
                    'model' => $model,
                ]
            );
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->end(200);
            }
            $this->alert('Изменения сохранены');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->end(200);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $model
                ]
            );
        }
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
