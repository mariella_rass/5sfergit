<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:35
 */

namespace backend\models;


class Category extends \common\models\Category
{
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['categoryName', 'categoryTitle', 'categoryDescription'];//Scenario Values Only Accepted
        $scenarios['update'] = [
            'categoryName',
            'categoryStatus',
            'categoryTitle',
            'categoryDescription',
            'categoryRel',
            'categoryAppendHtml'
        ];
        return $scenarios;
    }

}