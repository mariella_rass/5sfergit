<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Post;

/**
 * PostSearch represents the model behind the search form about `backend\models\Post`.
 */
class PostSearch extends \common\models\PostSearch
{
    public function init()
    {
        parent::init();
        $this->sorter = $this->getSorter();
    }

    private function getSorter()
    {
        $sorter = new \yii\data\Sort(
            [
                'defaultOrder' => ['date' => SORT_DESC],
                'attributes' => [
                    'date' => [
                        'asc' => ['postPublishTime' => SORT_ASC],
                        'desc' => ['postPublishTime' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Дата',
                    ],
                    'popular' => [
                        'asc' => ['postPopularity' => SORT_ASC],
                        'desc' => ['postPopularity' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Популярность',
                    ],
                ],
            ]
        );
        return $sorter;
    }
}
