<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Oooppps... 404</title>
    <script> AC_FL_RunContent = 0; </script>
    <script src="js/AC_RunActiveContent.js"></script>
    <style type="text/css">
        body, html {
            height: 100%;
        }

        #media {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            border-top: #212938 0px dashed;
        }

        #top_a {
            text-align: left;
            background-color: #fff;
            padding: 4px 3px 0 3px;
        }

        #top_soc {
            background-color: #fff;
            width: 500px;
        }

        #img_game {
            background-color: #fff;
            float: right;
            overflow: hidden;
            white-space: nowrap;
        }

        #bot_adv {
            position: absolute;
            top: 0px;
            left: 0;
        }

        img {
            border: 0;
        }

        .clear {
            clear: both;
            display: block;
            overflow: hidden;
            visibility: hidden;
            width: 0;
            height: 0;
        }
    </style>
</head>

<body style="margin:0 0;width:100%; height: 100%; min-height: 100%;">
<!--div id="top_a">
	<div id="img_game"><a target="_blank" href=""><img title="Самая красивая и популярная игра Цветник!" width="150" src="images/flowers.jpg" /></a>&nbsp;<a target="_blank" href=""><img title="Отличная игра - Шарики на столбиках!" width="150" src="images/balls.jpg" /></a>&nbsp;<a target="_blank" href=""><img title="Крокодил - рисуй и угадывай онлайн!" width="150" src="images/crocodile.jpg" /></a>&nbsp;<a target="_blank" href=""><img title="Играй с друзьями в старые добрые Линии!" width="150" src="images/lines.jpg" /></></div>
	<div id="top_soc">


	</div>
</div-->
<div class="clear"></div>
<div id="media">
    <script language="javascript">
        if (AC_FL_RunContent == 0) {
            alert("This page requires AC_RunActiveContent.js.");
        } else {
            AC_FL_RunContent(
                    'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0',
                    'width', '100%',
                    'height', '100%',
                    'src', 'index',
                    'quality', 'high',
                    'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
                    'align', 'middle',
                    'play', 'true',
                    'loop', 'true',
                    'scale', 'showall',
                    'wmode', 'opaque',
                    'devicefont', 'false',
                    'id', 'index',
                    'bgcolor', '#f9fafb',
                    'name', 'index',
                    'menu', 'true',
                    'allowFullScreen', 'false',
                    'allowScriptAccess', 'sameDomain',
                    'movie', 'flash/index',
                    'salign', ''
            ); //end AC code
        }
    </script>
    <noscript>
        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0"
                width="100%" height="100%" id="index" align="middle">
            <param name="allowScriptAccess" value="sameDomain"/>
            <param name="allowFullScreen" value="false"/>
            <param name="movie" value="flash/index.swf"/>
            <param name="quality" value="high"/>
            <param name="wmode" value="opaque"/>
            <param name="bgcolor" value="#f9fafb"/>
            <embed src="index.swf" quality="high" wmode="opaque" bgcolor="#f9fafb" width="100%" height="100%"
                   name="index" align="middle" allowscriptaccess="sameDomain" allowfullscreen="false"
                   type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"/>
        </object>
    </noscript>
</div>
</body>
</html>

