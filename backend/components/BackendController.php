<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.06.15
 * Time: 14:08
 */

namespace backend\components;


use kartik\widgets\Growl;
use yii\filters\AccessControl;
use yii\web\Controller;

class BackendController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($event)
    {
        if (isset(\Yii::$app->user->identity) &&
            \Yii::$app->user->identity->agreement != 1 &&
            !\Yii::$app->authManager->checkAccess(\Yii::$app->user->identity->getId(), 'admin') &&
            \Yii::$app->authManager->checkAccess(\Yii::$app->user->identity->getId(), 'author')
        ) {
            if ($this->uniqueId != 'agreement' && $this->action->uniqueId != 'index') {
                \Yii::$app->response->redirect(['agreement/index']);
            }
        }
        return parent::beforeAction($event);
    }

    protected function alert()
    {
        switch (func_num_args()) {
            case 1:
                $title = 'Все ок!';
                $mes = func_get_arg(0);
                break;
            case 2:
                $title = func_get_arg(0);
                $mes = func_get_arg(1);
                break;
            default:
                throw new \Exception('Invalid arg count');
        }
        $growl = new \stdClass();
        $growl->type = Growl::TYPE_SUCCESS;
        $growl->icon = 'glyphicon glyphicon-ok-sign';
        $growl->title = $title;
        $growl->body = $mes;
        \Yii::$app->session->setFlash('growl', $growl);
    }
}