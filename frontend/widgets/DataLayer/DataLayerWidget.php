<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.04.16
 * Time: 07:09
 */

namespace frontend\widgets\DataLayer;


use yii\base\Widget;

class DataLayerWidget extends Widget
{
    public $data = [];

    public function run()
    {
        return $this->render(
            'datalayer',
            [
                'data' => $this->data
            ]
        );
    }
}