<?php
/*
 * @var $this yii\web\View
 * @var $data
 *
 * */
$buffer = [];
foreach ($data as $key => $val) {
    $buffer[$key] = "'" . str_replace("'", "\\'", $key) . "':'" . str_replace("'", "\\'", $val) . "'";
}
Yii::trace(join(',', $buffer));
$this->registerJs('dataLayer = [{' . join(',', $buffer) . '}];', \yii\web\View::POS_BEGIN, 'dataLayer');
