<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

use frontend\assets\SocialLikes\SocialLikesAsset;
use yii\helpers\Url;

class VerticalShare extends \yii\bootstrap\Widget
{
    public $url;
    public $title;

    public function init()
    {
        parent::init();

        SocialLikesAsset::register($this->view);

        $this->url = !empty($this->url) ? $this->url : Url::base() . Url::to();
        $this->title = !empty($this->title) ? $this->title : $this->view->title;

        ?>
        <div class="margin-bottom-20">
            <img src="/img/share.png">
        </div>
        <div class="social-likes social-likes_vertical" data-url="<?= $this->url ?>" data-title="<?= htmlspecialchars($this->title) ?>">
            <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
            <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
            <!--div class="twitter" data-via="pintosevich_7" title="Поделиться ссылкой в Твиттере">Twitter</--div-->
            <!--div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div-->
            <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
            <!--div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div-->
        </div>
        <?php
    }
}
