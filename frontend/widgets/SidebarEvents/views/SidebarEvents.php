<?php if ($events): ?>
    <div class="sidebar-events margin-bottom-20">
        <h3>Обучайся</h3>
        <?php foreach (array_slice($events, 0, 5) as $event): ?>
            <div class="row margin-top-20">
                <div class="col-xs-3"><a href="<?= $event->user->url ?>"><?= $event->user->profile->getImgHtml(
                            'avatar',
                            0,
                            false,
                            [
                                'class' => "img-responsive rounded-x"
                            ]
                        ) ?></a></div>
                <div class="col-xs-9">
                    <div class="color-dark">
                        <small><b><a href="<?= $event->user->url ?>"
                                     class="color-dark"><?= $event->user->profile->name ?></a></b></small>
                    </div>
                    <div>
                        <a href="<?= $event->eventUrl ?>" target="_blank"> <?= $event->eventName ?></a>
                    </div>
                    <small class="color-grey"><i
                            class="fa fa-calendar color-darkgrey"></i> <?= \common\components\DateFormater::format(
                            $event->eventDate
                        ) ?>
                        <?php if ($event->eventPaid == 'free'): ?>
                            <span class="label label-info pull-right"><small>бесплатно</small></span>
                        <?php endif; ?>
                    </small>
                </div>
            </div>
        <?php endforeach ?>
        <?php if (count($events) > 5): ?>
            <div class="margin-top-20"><?= \yii\helpers\Html::a(
                    'Смотреть все события',
                    \yii\helpers\Url::to(['events/']),
                    [
                        'class' => 'btn-u btn-u-blue btn-block rounded-2x'
                    ]
                ) ?></div>
        <?php endif ?>
    </div>
<?php endif ?>