<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.07.15
 * Time: 13:20
 */

namespace frontend\widgets;


use yii\base\Widget;
use yii\web\View;

class AnalyticsExtraData extends Widget
{
    public $data = [];

    public function run()
    {
        $this->view->registerJs(" var pageExtraData = " . json_encode($this->data) . ';', View::POS_BEGIN);
    }
}