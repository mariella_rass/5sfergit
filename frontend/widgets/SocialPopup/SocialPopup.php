<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.07.15
 * Time: 13:10
 */

namespace frontend\widgets\SocialPopup;


use yii\base\Widget;

class SocialPopup extends Widget
{
    public function run()
    {
        return $this->render('social-popup');
    }
}