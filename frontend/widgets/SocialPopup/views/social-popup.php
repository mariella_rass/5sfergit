<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.07.15
 * Time: 13:52
 */
$this->registerJsFile("//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=132147813635823", ['yii\web\JqueryAsset']);
$asset = \frontend\widgets\SocialPopup\SocialPopupAsset::register($this);
?>
<div id="social-popup-overlay"
     class="_hidden" style="display: none">
    <div
        id="facebook-win"
        class="hidden-xs hidden-sm">
        <div class="title">
            <span class="close offbtn" title="Закрыть" style="display: none"></span>
            <a rel="nofollow" href="https://www.facebook.com/5sfer">
                <img src="<?= $asset->baseUrl ?>/img/facebook-logo.png" alt="facebook"/>
            </a>

            <p>Нажми «<b>Нравится</b>», чтобы читать 5 Сфер на Facebook</p>
        </div>

        <div class="fb-page" data-href="https://www.facebook.com/5sfer" data-small-header="false"
             data-width="445" data-hide-cover="false" data-show-facepile="true">
            <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/5sfer"><a href="https://www.facebook.com/5sfer">5 Сфер</a>
                </blockquote>
            </div>
        </div>
        <div class="for-login offbtn" style="display: none"><a class="dashed" rel="close">Мне уже нравятся 5 сфер</a>
        </div>
    </div>
</div>

