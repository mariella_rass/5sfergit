<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.07.15
 * Time: 13:40
 */

namespace frontend\widgets\SocialPopup;


use frontend\assets\SkrollrAsset;
use yii\web\AssetBundle;

class SocialPopupAsset extends AssetBundle
{
    public $css = [
        'social-popup.css',
    ];
    public $js = [
        'facebook.js',
        'social-popup.js',
    ];
    public $depends = [
        'frontend\assets\JsCookieAssets',
        'frontend\assets\SkrollrAsset'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';
        return parent::init();
    }

}