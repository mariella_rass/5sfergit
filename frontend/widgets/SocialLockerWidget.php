<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.12.15
 * Time: 03:27
 */

namespace frontend\widgets;


use frontend\assets\SocialLocker\SocialLockerAsset;
use yii\base\Widget;

class SocialLockerWidget extends Widget
{
    public $content;
    public $SEPARATOR = '<hr>';

    public function run()
    {

        $parts = explode($this->SEPARATOR, $this->content, 2);
        if (count($parts) == 2) {
            $asset = SocialLockerAsset::register($this->view);
            $this->view->registerCss(
                "
                .onp-sociallocker{
                    margin-top: -200px;
                    padding-top: 220px;
                    margin-left: 0px;
                    margin-right: 0px;
                    max-width: 100%;
                    background: -webkit-linear-gradient(rgba(255,255,255,0) 0%, rgba(255,255,255,1) 45%, rgba(255,255,255,1) 50%, rgba(249,249,249,1) 50%);
                    background: -o-linear-gradient(rgba(255,255,255,0) 0%, rgba(255,255,255,1) 45%, rgba(255,255,255,1) 50%, rgba(249,249,249,1) 50%);
                    background: -moz-linear-gradient(rgba(255,255,255,0) 0%, rgba(255,255,255,1) 45%, rgba(255,255,255,1) 50%, rgba(249,249,249,1) 50%);
                    background: linear-gradient(rgba(255,255,255,0) 0%, rgba(255,255,255,1) 45%, rgba(255,255,255,1) 50%, rgba(249,249,249,1) 50%);
                }
            "
            );
            $this->view->registerJs(
                '
                $("#locked-content").sociallocker({
                    facebook: {
                        appId: 132147813635823,
                        like:{
                            url: "https://www.facebook.com/5sfer"
                        }
                    },
                    vk: {
                        appId: 4278462,
                        subscribe:{
                            group_id: "5sfer"
                        }
                    },
                    twitter: {
                        follow:{
                            url: "https://twitter.com/pintosevich_7"
                        }
                    },
                    google: {
                        youtube: {
                            // ID вашего приложени в google
                            clientId: "659426721908-9vmitb25snpbif0ipjpnl8t0eq37fg9e.apps.googleusercontent.com",
                            // ID вашего канала на ютуб
                            channelId: "UCYIQjKYYUnHy8YCLdLX3j6w"
                        }
                    },
                    buttons: {
                        order: [
                            "facebook-like",
                            "vk-subscribe",
                            "google-youtube"
                        ],
                        counter: false
                    },
                    locker: {
                        useCookies: true,
                        close: false,
                        mobile: false
                    },
                    theme: "starter",
                    text: {
                        header: "Полная версия статьи доступна только подписчикам!",
                        message: "Подпишись на наши обновления, что бы получить полный доступ ко всем статьям!"
                    }
                })
            '
            );
            $res = $parts[0] . '<div id="locked-content">' . str_replace($this->SEPARATOR, '', $parts[1]) . '</div>';
        } else {
            $res = $this->content;
        }
        return $res;
    }

}