<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.07.15
 * Time: 17:24
 */

namespace frontend\widgets\SidebarRecommended;


use common\models\Event;
use yii\base\Widget;

class SidebarRecommended extends Widget
{
    public function run()
    {
        if (!isset($this->view->params['postModel']) || !$this->view->params['postModel']) {
            $model = \frontend\models\Post::find()->status(\frontend\models\Post::PUBLISH)->orderBy(
                ['postPopularity' => SORT_DESC]
            )->limit(1)->one();
        } else {
            $model = $this->view->params['postModel'];
        }

        $posts = \frontend\models\Post::find()->andWhere(
            'postPopularity < ' . $model->postPopularity
        )->offset(3)->limit(5)->status(\frontend\models\Post::PUBLISH)->orderBy(['postPopularity' => SORT_DESC])->all();

        if (count($posts) != 5) {
            $posts = \frontend\models\Post::find()->offset(3)->limit(5)->orderBy(['postPopularity' => SORT_DESC])->all(
            );
        }

        return $this->render('SidebarRecommended', [
            'posts' => $posts
        ]);
    }
}