<div class="margin-bottom-40">
    <h3>Лучшие статьи</h3>
    <ul class="list-unstyled">
        <?php foreach ($posts as $post): ?>
            <li>
                <a class=" color-blue" href="<?= $post->getUrl() ?>">
                    <?= $post->postTitle ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
</div>