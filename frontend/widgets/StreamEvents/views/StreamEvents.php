<?php if ($events): ?>
    <div id="stream-events" class="visible-xs visible-sm hidden-print">
        <div class="container">
            <div class="stream-events">
                <h3>Обучайся!</h3>

                <div class="row">
                    <?php foreach (array_slice($events, 0, 5) as $event): ?>
                        <div class="col-xs-12 margin-bottom-20">
                            <div style="background-color: #e7e7e7;padding: 10px 15px 10px 15px;" class="rounded-2x">
                                <div>
                                    <?php if ($event->eventPaid == 'free'): ?>
                                        <span class="label label-info pull-right"><small>бесплатно</small></span>
                                    <?php endif; ?>
                                    <span
                                        class="color-grey small"><?= \common\components\DateFormater::format(
                                            $event->eventDate
                                        ) ?>
                                    </span>
                                    - <a
                                        href="<?= $event->user->url ?>"
                                        class="color-dark small"
                                        style="font-weight: bold"><?= $event->user->profile->name ?></a>
                                </div>
                                <div><a
                                        href="<?= $event->eventUrl ?>" style="font-size: 18px"
                                        target="_blank"> <?= $event->eventName ?></a></div>
                                <div>

                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <?php if (count($events) > 5): ?>
                    <div class="margin-top-20"><?= \yii\helpers\Html::a(
                            'Смотреть все события',
                            \yii\helpers\Url::to(['events/']),
                            [
                                'class' => 'btn-u btn-u-blue btn-u-lg rounded'
                            ]
                        ) ?></div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>