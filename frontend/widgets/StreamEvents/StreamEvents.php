<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.07.15
 * Time: 17:24
 */

namespace frontend\widgets\StreamEvents;


use common\models\Event;
use yii\base\Widget;

class StreamEvents extends Widget
{
    public function run()
    {
        if (!isset($this->view->params['sidebar_events']) || $this->view->params['sidebar_events'] !== false) {
            $events = Event::find()
                ->where(['eventStatus' => 'on'])
                ->andWhere(['>', 'eventDate', time()])
                ->orderBy(['eventDate' => SORT_ASC])
                ->limit(6)
                ->with('user.profile', 'user.achives')
                ->all();

            return $this->render(
                'StreamEvents',
                [
                    'events' => $events
                ]
            );
        }
    }
}