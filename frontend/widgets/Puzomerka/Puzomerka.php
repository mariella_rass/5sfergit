<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.08.15
 * Time: 13:40
 */

namespace frontend\widgets\Puzomerka;


use yii\base\Widget;
use yii\caching\DummyCache;

class Puzomerka extends Widget
{
    public function run()
    {


        $reads = \Yii::$app->db->createCommand(
                "
        SELECT
            SUM(count)
        FROM
            view
        WHERE
            date > DATE_SUB(CURDATE(), INTERVAL 30 DAY)
        "
            )->queryScalar();

            $authors = \Yii::$app->db->createCommand(
                "
        SELECT
            COUNT(*)
        FROM
            profile
        WHERE
            status = 'approved'
        "
            )->queryScalar();

        $cache = \Yii::$app->getCache();
        $cache = $cache ? $cache : new DummyCache();

        $cache_key = "util:facebook:IPS_fan_count";

        $count = $cache->get($cache_key);

        if ($count === false) {

            $fb = new \Facebook\Facebook(
                [
                    'app_id' => '162314464202044',
                    'app_secret' => '06a1c7e8e78aac5aa2cc2c415b282b67',
                    'default_graph_version' => 'v2.7',
                    'default_access_token' => '162314464202044|YI-o8MzT6wml6ybFG1JhbAk3wwc', // optional
                ]
            );

            try {
                $response = $fb->get('/282646155930?fields=fan_count');
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                \Yii::trace('Graph returned an error: ' . $e->getMessage());
                return;
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                \Yii::trace( 'Facebook SDK returned an error: ' . $e->getMessage());
                return;
            }

            $count = $response->getGraphPage()->asArray()['fan_count'];

            $cache->set($cache_key, $count, 3600);

        }

        return $this->render('puzomerka',[
            'reads' => $reads,
            'authors' => $authors,
            'likes' => $count
        ]);
    }
}