<section class="funfacts hidden-xs hidden-sm margin-bottom-20" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    <?= number_format($reads, 0, ',', ' ') ?>
                </div>
                <div class="funfact">
                    Читателей в месяц
                </div>
                <a class="btn btn-primary" href="<?= \yii\helpers\Url::to(
                    [
                        'post/index',
                        'term' => null,
                        'filter' => null,
                        'sort' => 'popular',
                        'page' => 1
                    ]
                ) ?>"><i class="fa fa-eye"></i> читай</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    <?= number_format($likes, 0, ',', ' ') ?>
                </div>
                <div class="funfact">
                    Фанов Facebook
                </div>
                <a class="btn btn-primary" href="https://www.facebook.com/isaac.pintosevich.systems"
                   target="_blank"><i class="fa fa-facebook-official"></i>
                    присоединяйся</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    141 889
                </div>
                <div class="funfact">
                    Получателей рассылки
                </div>
                <div class="link">
                    <a class="btn btn-primary manual-optin-trigger" data-optin-slug="wa626atqtnwmmpw0" href="#"
                       onclick="return false;"><i
                            class="fa fa-envelope-o"></i> подписывайся</a>
                    <!-- This site is converting visitors into subscribers and customers with OptinMonster - http://optinmonster.com :: Campaign Title: СТАВЬ ЦЕЛИ_Подписка с главной --><div id="om-wa626atqtnwmmpw0-holder"></div><script>var wa626atqtnwmmpw0,wa626atqtnwmmpw0_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){wa626atqtnwmmpw0_poll(function(){if(window['om_loaded']){if(!wa626atqtnwmmpw0){wa626atqtnwmmpw0=new OptinMonsterApp();return wa626atqtnwmmpw0.init({"u":"21308.402263","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;wa626atqtnwmmpw0=new OptinMonsterApp();wa626atqtnwmmpw0.init({"u":"21308.402263","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster -->
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    <?= number_format($authors, 0, ',', ' ') ?>
                </div>
                <div class="funfact">
                    Постоянных авторов
                </div>
                <div class="link">
                    <a class="btn btn-primary" href="<?= \yii\helpers\Url::to(['akademiya-ekspertov/index']) ?>"><i
                            class="fa fa-pencil"></i> публикуйся</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
//$this->registerJs(
//    '
//var wa626atqtnwmmpw0,wa626atqtnwmmpw0_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){wa626atqtnwmmpw0_poll(function(){if(window[\'om_loaded\']){if(!wa626atqtnwmmpw0){wa626atqtnwmmpw0=new OptinMonsterApp();return wa626atqtnwmmpw0.init({"u":"21308.402263","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;wa626atqtnwmmpw0=new OptinMonsterApp();wa626atqtnwmmpw0.init({"u":"21308.402263","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");
//    '
//,
//    \yii\web\View::POS_LOAD
//);

$this->registerCssFile(
    'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&subset=latin,cyrillic,cyrillic-ext'
);
$this->registerCssFile(
    'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic'
);

$this->registerCss(
    "
.funfacts {
    text-align: center;
    background-color: #f27242;
    color: #fff;
    font-family: 'Open Sans Condensed', sans-serif;
    padding-top: 20px;
    height: 208px;
}

.funnumber {
    font-size: 60px;
    font-weight: bolder;
}

.funfact {
    font-size: 20px;
}

.funfacts a{
    color: #fff;
    font-size: 16px;
    text-decoration: none;
    text-transform: uppercase;
    background-color: #8a2be2;
    border-color: #8a2be2;
}

.funfacts a i{
    padding-right:3px;
}

.funfacts a:hover{
    background-color: rgba(138, 43, 226, 0.8);
    border-color: rgba(138, 43, 226, 0.9);
}

.funfacts a:hover{
    text-decoration: none;
}

"
);