<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.07.15
 * Time: 14:29
 */

namespace frontend\widgets;


use yii\base\Widget;
use yii\helpers\Json;
use yii\web\View;

class DFP extends Widget
{
    public $iu;
    public $sz;
    public $cid;
    public $t;
    public $imgClass;

    public function init()
    {
        parent::init();
        // .setTargeting("interests", ["sports", "music", "movies"])
        $t = '';
        $js = '';
        if (isset($this->t) && is_array($this->t) && !empty($this->t)) {
            // проходим по масиву
            foreach ($this->t as $tk => $te) {
                if (is_array($te)) {
                    foreach ($te as $te_key => $te_el) {
                        $te[$te_key] = str_replace(
                            ['\\', '.', ',', ':', ';', '(', ')', '!', '?', '@', '#', '$', '%', '^', '*', '-', '+', '='],
                            '',
                            $te_el
                        );
                    }
                }
                $t[$tk] = //urlencode(
                    $tk . '=' .
                    (
                    is_array($te)
                        ? join(',', $te)
                        : $te
                    //)
                );
                $js .= '.setTargeting("' . $tk . '", ' . (
                    is_array($te)
                        ? '["' . join('", "', $te) . '"]'
                        : '"' . $te . '"'
                    ) . ')';
            }
            $t = urlencode(join('&', $t));
        }


        if (!isset($this->cid)) {
            $rand = rand(1, 9999999);
            echo "<a href='http://pubads.g.doubleclick.net/gampad/jump?iu={$this->iu}&sz=" . implode(
                    'x',
                    $this->sz
                ) . "&c={$rand}&t={$t}'>";
            echo "<img src='http://pubads.g.doubleclick.net/gampad/ad?iu={$this->iu}&sz=" . implode(
                    'x',
                    $this->sz
                ) . "&c={$rand}&t={$t}' class='img-responsive img-center {$this->imgClass}' style='border-radius: 0px!important'>";
            echo "</a>";
        } else {
            $this->view->registerJs(
                "
                var googletag = googletag || {};
                googletag.cmd = googletag.cmd || [];
                (function() {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
                })();
                ",
                View::POS_HEAD
            );


            $this->view->registerJs(
                "
                googletag.cmd.push(function() {
                googletag.defineSlot('{$this->iu}', " . Json::encode($this->sz) . ", '{$this->cid}').addService(googletag.pubads()){$js};
                //googletag.pubads().enableSingleRequest();
                googletag.enableServices();
                });
                ",
                View::POS_HEAD
            );

            echo "<!-- {$this->iu} -->";
            echo "<div id='{$this->cid}'></div>";
            $this->view->registerJs(
                "
                googletag.cmd.push(function() { googletag.display('{$this->cid}'); });
                "
            );
        }

    }
}