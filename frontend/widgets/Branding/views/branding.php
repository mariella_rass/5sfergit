<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.03.16
 * Time: 18:13
 */
\frontend\assets\AdRiver\AdRiverAsset::register($this);
?>

    <div id="adriver_banner_1432940518"></div>
<?php
$this->registerJs(
    '
    if (Math.max(window.screen.availWidth, window.screen.availHeight) > 749){
        (function(){
            new adriver("adriver_banner_1432940518", {sid:212467, bt:52, bn:1, custom: ' . json_encode($custom) . '});

            var checkBrandingTimer = setInterval(
            function (){
                if (!$("body").hasClass("branded") && $("body").css("background-image").search(/^url/) == 0){
                    $("body").addClass("branded");
                    clearInterval(checkBrandingTimer);
                }
            }, 250);

        })();
    }
'
);