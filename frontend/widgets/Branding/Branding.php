<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 24.03.16
 * Time: 18:11
 */

namespace frontend\widgets\Branding;


use yii\base\Widget;

class Branding extends Widget
{
    public $custom = [];
    function run()
    {
        return $this->render(
            'branding',
            [
                'custom' => $this->custom
            ]
        );
    }
}