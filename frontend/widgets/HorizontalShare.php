<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

use frontend\assets\SocialLikes\SocialLikesAsset;
use yii\helpers\Url;

class HorizontalShare extends \yii\bootstrap\Widget
{
    public $url;
    public $title;

    public function init()
    {
        parent::init();

        SocialLikesAsset::register($this->view);

        $this->url = !empty($this->url) ? $this->url : Url::base() . Url::to();
        $this->title = !empty($this->title) ? $this->title : $this->view->title;

        ?>
        <div class="social-likes social-likes_horizontal" data-url="<?= $this->url ?>" data-counters="no"
             data-title="<?= htmlspecialchars($this->title) ?>">
            <div class="facebook margin-bottom-10 " title="Поделиться ссылкой на Фейсбуке">Поделиться в Facebook</div>
            <div class="odnoklassniki margin-bottom-10" title="Поделиться ссылкой в Однокласниках">Опубликовать в
                Однокласники
            </div>
            <div class="vkontakte margin-bottom-10" title="Поделиться ссылкой во Вконтакте">Рассказать Вконтакте</div>
        </div>
        <?php
    }
}
