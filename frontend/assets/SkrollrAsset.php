<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SkrollrAsset extends AssetBundle{

    public $sourcePath = '@bower/skrollr/src';
    public $js = [
        'skrollr.js'
    ];

}
