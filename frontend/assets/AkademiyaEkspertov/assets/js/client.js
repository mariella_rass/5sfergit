$('.carousel-inner').slick({
    'adaptiveHeight':true,
    'autoplay':true,
    'arrows':false,
    'speed':800,
    'dots': false,
    'autoplaySpeed':3300
});

$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

//new WOW().init();