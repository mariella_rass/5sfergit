<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.08.15
 * Time: 12:59
 */

namespace frontend\assets\AkademiyaEkspertov;


use yii\web\AssetBundle;

class AkademiyaEkspertovAsset extends AssetBundle
{
    public $css = [
        'css/main.css'
    ];
    public $js = [
        'js/client.js',
        'js/wow.min.js'
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
        'frontend\assets\SlickAsset'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';
        return parent::init();
    }
}