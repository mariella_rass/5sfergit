<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.08.15
 * Time: 13:46
 */

namespace frontend\assets;


use frontend\assets\AppAsset;
use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

class StandartAssets extends AppAsset
{
    public $extend_css = [
        'css/site.css',
        'css/headers/header-v5.css',
        'css/footers/footer-v3.css',
        'css/pages/shortcode_timeline2.css',
        'plugins/owl-carousel/owl-carousel/owl.carousel.css',
        'plugins/parallax-slider/css/parallax-slider.css',
        'plugins/sky-forms-pro/skyforms/css/sky-forms.css',
        'plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css'
    ];

    public $extend_js = [
    ];

    public $extend_depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset'
    ];


    public function init(){
        $this->extend();
        return parent::init();
    }

    private function extend(){
        $this->css = ArrayHelper::merge($this->css, $this->extend_css);
        $this->js = ArrayHelper::merge($this->js, $this->extend_js);
        $this->depends = ArrayHelper::merge($this->depends, $this->extend_depends);
    }
}