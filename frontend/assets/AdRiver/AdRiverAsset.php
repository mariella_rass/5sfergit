<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.08.15
 * Time: 12:59
 */

namespace frontend\assets\AdRiver;


use yii\web\AssetBundle;

class AdRiverAsset extends AssetBundle
{
    public $js = [
        'js/adriver.core.2.js'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';
        return parent::init();
    }
}