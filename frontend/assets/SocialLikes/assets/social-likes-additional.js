/**
 * Created by admin on 31.03.16.
 */

(function () {
    var totalLikes = 0;
    var services = [];
    var wait = false;
    var completed = [];
    var data = [];

    $(document).ready(
        function () {
            $('.social-likes').children().each(function (i, e) {
                $(e).attr("class").split(/\s+/).filter(function (className) {
                    return className.indexOf('social-likes__widget_') == 0;
                }).forEach(function (e) {
                    var serviceName = e.substring(21).toLocaleLowerCase();
                    if (services.indexOf(serviceName) == -1) {
                        services.push(serviceName);
                    }
                });
            });
        }
    );

    $('.social-likes').on('counter.social-likes', function (event, service, number) {
        if (!wait) {
            wait = true;
            data = [];
            totalLikes = 0;
            completed = [];
        }
        if (services.indexOf(service) != -1 && completed.indexOf(service) == -1) {
            completed.push(service);
            data[service] = number;
            totalLikes += number;
            if (completed.length == services.length) {
                wait = false;
                $('body').trigger('complete.social-likes', totalLikes);
            }
        }
    });

})();