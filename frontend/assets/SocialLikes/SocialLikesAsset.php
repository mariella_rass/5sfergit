<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets\SocialLikes;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class SocialLikesAsset extends AssetBundle
{

    public $js = [
        'social-likes-additional.js'
    ];

    public $css = [
        'social-likes-additional.css'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';

        $this->depends = [
                JqueryAsset::className(), SocialLikesBundleAsset::className()
            ];
        return parent::init();
    }

}
