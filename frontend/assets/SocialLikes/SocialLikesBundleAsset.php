<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets\SocialLikes;


use yii\web\AssetBundle;

class SocialLikesBundleAsset extends AssetBundle
{

    public $sourcePath = '@bower/social-likes/dist';

    public $js = [
        'social-likes.min.js'
    ];

    public $css = [];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
