<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SlickAsset extends AssetBundle{

    public $sourcePath = '@bower/slick-carousel';

    public $css = [
        'slick/slick.css',
        'slick/slick-theme.css'
    ];

    public $js = [
        'slick/slick.min.js'
    ];
    public $depends = [
       'yii\web\JqueryAsset',
    ];

}
