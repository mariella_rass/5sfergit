<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.08.15
 * Time: 12:59
 */

namespace frontend\assets\SocialLocker;


use yii\web\AssetBundle;

class SocialLockerAsset extends AssetBundle
{
    public $css = [
        'css/jquery.onp.sociallocker.1.7.9.min.css'
    ];
    public $js = [
        'js/jquery.onp.sociallocker.1.7.9.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';
        return parent::init();
    }
}