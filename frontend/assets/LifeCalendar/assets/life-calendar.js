function Tree() {
    var t = this;
    t.build_tree = function (e) {
        var n, r, s = t.dyn_tree, o = t.stat_desc.static_tree, i = t.stat_desc.elems, a = -1;
        for (e.heap_len = 0, e.heap_max = HEAP_SIZE, n = 0; i > n; n++)0 !== s[2 * n] ? (e.heap[++e.heap_len] = a = n, e.depth[n] = 0) : s[2 * n + 1] = 0;
        for (; 2 > e.heap_len;)r = e.heap[++e.heap_len] = 2 > a ? ++a : 0, s[2 * r] = 1, e.depth[r] = 0, e.opt_len--, o && (e.static_len -= o[2 * r + 1]);
        for (t.max_code = a, n = Math.floor(e.heap_len / 2); n >= 1; n--)e.pqdownheap(s, n);
        r = i;
        do n = e.heap[1], e.heap[1] = e.heap[e.heap_len--], e.pqdownheap(s, 1), o = e.heap[1], e.heap[--e.heap_max] = n, e.heap[--e.heap_max] = o, s[2 * r] = s[2 * n] + s[2 * o], e.depth[r] = Math.max(e.depth[n], e.depth[o]) + 1, s[2 * n + 1] = s[2 * o + 1] = r, e.heap[1] = r++, e.pqdownheap(s, 1); while (2 <= e.heap_len);
        e.heap[--e.heap_max] = e.heap[1], n = t.dyn_tree;
        for (var u, l, a = t.stat_desc.static_tree, c = t.stat_desc.extra_bits, f = t.stat_desc.extra_base, d = t.stat_desc.max_length, m = 0, i = 0; MAX_BITS >= i; i++)e.bl_count[i] = 0;
        for (n[2 * e.heap[e.heap_max] + 1] = 0, r = e.heap_max + 1; HEAP_SIZE > r; r++)o = e.heap[r], i = n[2 * n[2 * o + 1] + 1] + 1, i > d && (i = d, m++), n[2 * o + 1] = i, o > t.max_code || (e.bl_count[i]++, u = 0, o >= f && (u = c[o - f]), l = n[2 * o], e.opt_len += l * (i + u), a && (e.static_len += l * (a[2 * o + 1] + u)));
        if (0 !== m) {
            do {
                for (i = d - 1; 0 === e.bl_count[i];)i--;
                e.bl_count[i]--, e.bl_count[i + 1] += 2, e.bl_count[d]--, m -= 2
            } while (m > 0);
            for (i = d; 0 !== i; i--)for (o = e.bl_count[i]; 0 !== o;)a = e.heap[--r], a > t.max_code || (n[2 * a + 1] != i && (e.opt_len += (i - n[2 * a + 1]) * n[2 * a], n[2 * a + 1] = i), o--)
        }
        for (n = t.max_code, r = e.bl_count, e = [], o = 0, i = 1; MAX_BITS >= i; i++)e[i] = o = o + r[i - 1] << 1;
        for (r = 0; n >= r; r++)if (c = s[2 * r + 1], 0 !== c) {
            o = s, i = 2 * r, a = e[c]++, f = 0;
            do f |= 1 & a, a >>>= 1, f <<= 1; while (0 < --c);
            o[i] = f >>> 1
        }
    }
}
function StaticTree(t, e, n, r, s) {
    this.static_tree = t, this.extra_bits = e, this.extra_base = n, this.elems = r, this.max_length = s
}
function Config(t, e, n, r, s) {
    this.good_length = t, this.max_lazy = e, this.nice_length = n, this.max_chain = r, this.func = s
}
function smaller(t, e, n, r) {
    var s = t[2 * e];
    return t = t[2 * n], t > s || s == t && r[e] <= r[n]
}
function Deflate() {
    function t() {
        var t;
        for (t = 0; L_CODES > t; t++)Y[2 * t] = 0;
        for (t = 0; D_CODES > t; t++)G[2 * t] = 0;
        for (t = 0; BL_CODES > t; t++)X[2 * t] = 0;
        Y[2 * END_BLOCK] = 1, nt = st = J.opt_len = J.static_len = 0
    }

    function e(t, e) {
        var n, r, s = -1, o = t[1], i = 0, a = 7, u = 4;
        for (0 === o && (a = 138, u = 3), t[2 * (e + 1) + 1] = 65535, n = 0; e >= n; n++)r = o, o = t[2 * (n + 1) + 1], ++i < a && r == o || (u > i ? X[2 * r] += i : 0 !== r ? (r != s && X[2 * r]++, X[2 * REP_3_6]++) : 10 >= i ? X[2 * REPZ_3_10]++ : X[2 * REPZ_11_138]++, i = 0, s = r, 0 === o ? (a = 138, u = 3) : r == o ? (a = 6, u = 3) : (a = 7, u = 4))
    }

    function n(t) {
        J.pending_buf[J.pending++] = t
    }

    function r(t) {
        n(255 & t), n(t >>> 8 & 255)
    }

    function s(t, e) {
        at > Buf_size - e ? (it |= t << at & 65535, r(it), it = t >>> Buf_size - at, at += e - Buf_size) : (it |= t << at & 65535, at += e)
    }

    function o(t, e) {
        var n = 2 * t;
        s(65535 & e[n], 65535 & e[n + 1])
    }

    function i(t, e) {
        var n, r, i = -1, a = t[1], u = 0, l = 7, c = 4;
        for (0 === a && (l = 138, c = 3), n = 0; e >= n; n++)if (r = a, a = t[2 * (n + 1) + 1], !(++u < l && r == a)) {
            if (c > u) {
                do o(r, X); while (0 !== --u)
            } else 0 !== r ? (r != i && (o(r, X), u--), o(REP_3_6, X), s(u - 3, 2)) : 10 >= u ? (o(REPZ_3_10, X), s(u - 3, 3)) : (o(REPZ_11_138, X), s(u - 11, 7));
            u = 0, i = r, 0 === a ? (l = 138, c = 3) : r == a ? (l = 6, c = 3) : (l = 7, c = 4)
        }
    }

    function a() {
        16 == at ? (r(it), at = it = 0) : at >= 8 && (n(255 & it), it >>>= 8, at -= 8)
    }

    function u(t, e) {
        var n, r, s;
        if (J.pending_buf[rt + 2 * nt] = t >>> 8 & 255, J.pending_buf[rt + 2 * nt + 1] = 255 & t, J.pending_buf[tt + nt] = 255 & e, nt++, 0 === t ? Y[2 * e]++ : (st++, t--, Y[2 * (Tree._length_code[e] + LITERALS + 1)]++, G[2 * Tree.d_code(t)]++), 0 === (8191 & nt) && j > 2) {
            for (n = 8 * nt, r = L - O, s = 0; D_CODES > s; s++)n += G[2 * s] * (5 + Tree.extra_dbits[s]);
            if (st < Math.floor(nt / 2) && n >>> 3 < Math.floor(r / 2))return !0
        }
        return nt == et - 1
    }

    function l(t, e) {
        var n, r, i, a, u = 0;
        if (0 !== nt)do n = J.pending_buf[rt + 2 * u] << 8 & 65280 | 255 & J.pending_buf[rt + 2 * u + 1], r = 255 & J.pending_buf[tt + u], u++, 0 === n ? o(r, t) : (i = Tree._length_code[r], o(i + LITERALS + 1, t), a = Tree.extra_lbits[i], 0 !== a && (r -= Tree.base_length[i], s(r, a)), n--, i = Tree.d_code(n), o(i, e), a = Tree.extra_dbits[i], 0 !== a && (n -= Tree.base_dist[i], s(n, a))); while (nt > u);
        o(END_BLOCK, t), ot = t[2 * END_BLOCK + 1]
    }

    function c() {
        at > 8 ? r(it) : at > 0 && n(255 & it), at = it = 0
    }

    function f(t, e, n) {
        s((STORED_BLOCK << 1) + (n ? 1 : 0), 3), c(), ot = 8, r(e), r(~e), J.pending_buf.set(k.subarray(t, t + e), J.pending), J.pending += e
    }

    function d(n) {
        var r, o, a = O >= 0 ? O : -1, u = L - O, d = 0;
        if (j > 0) {
            for (V.build_tree(J), Q.build_tree(J), e(Y, V.max_code), e(G, Q.max_code), $.build_tree(J), d = BL_CODES - 1; d >= 3 && 0 === X[2 * Tree.bl_order[d] + 1]; d--);
            J.opt_len += 3 * (d + 1) + 14, r = J.opt_len + 3 + 7 >>> 3, o = J.static_len + 3 + 7 >>> 3, r >= o && (r = o)
        } else r = o = u + 5;
        if (r >= u + 4 && -1 != a)f(a, u, n); else if (o == r)s((STATIC_TREES << 1) + (n ? 1 : 0), 3), l(StaticTree.static_ltree, StaticTree.static_dtree); else {
            for (s((DYN_TREES << 1) + (n ? 1 : 0), 3), a = V.max_code + 1, u = Q.max_code + 1, d += 1, s(a - 257, 5), s(u - 1, 5), s(d - 4, 4), r = 0; d > r; r++)s(X[2 * Tree.bl_order[r] + 1], 3);
            i(Y, a - 1), i(G, u - 1), l(Y, G)
        }
        t(), n && c(), O = L, y.flush_pending()
    }

    function m() {
        var t, e, n, r;
        do {
            if (r = S - B - L, 0 === r && 0 === L && 0 === B)r = q; else if (-1 == r)r--; else if (L >= q + q - MIN_LOOKAHEAD) {
                k.set(k.subarray(q, q + q), 0), N -= q, L -= q, O -= q, n = t = C;
                do e = 65535 & E[--n], E[n] = e >= q ? e - q : 0; while (0 !== --t);
                n = t = q;
                do e = 65535 & T[--n], T[n] = e >= q ? e - q : 0; while (0 !== --t);
                r += q
            }
            if (0 === y.avail_in)break;
            t = y.read_buf(k, L + B, r), B += t, B >= MIN_MATCH && (A = 255 & k[L], A = (A << F ^ 255 & k[L + 1]) & R)
        } while (MIN_LOOKAHEAD > B && 0 !== y.avail_in)
    }

    function w(t) {
        var e, n = 65535;
        for (n > v - 5 && (n = v - 5); ;) {
            if (1 >= B) {
                if (m(), 0 === B && t == Z_NO_FLUSH)return NeedMore;
                if (0 === B)break
            }
            if (L += B, B = 0, e = O + n, (0 === L || L >= e) && (B = L - e, L = e, d(!1), 0 === y.avail_out))return NeedMore;
            if (L - O >= q - MIN_LOOKAHEAD && (d(!1), 0 === y.avail_out))return NeedMore
        }
        return d(t == Z_FINISH), 0 === y.avail_out ? t == Z_FINISH ? FinishStarted : NeedMore : t == Z_FINISH ? FinishDone : BlockDone
    }

    function p(t) {
        var e, n = P, r = L, s = U, o = L > q - MIN_LOOKAHEAD ? L - (q - MIN_LOOKAHEAD) : 0, i = W, a = b, u = L + MAX_MATCH, l = k[r + s - 1], c = k[r + s];
        U >= K && (n >>= 2), i > B && (i = B);
        do if (e = t, !(k[e + s] != c || k[e + s - 1] != l || k[e] != k[r] || k[++e] != k[r + 1])) {
            r += 2, e++;
            do; while (k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && k[++r] == k[++e] && u > r);
            if (e = MAX_MATCH - (u - r), r = u - MAX_MATCH, e > s) {
                if (N = t, s = e, e >= i)break;
                l = k[r + s - 1], c = k[r + s]
            }
        } while ((t = 65535 & T[t & a]) > o && 0 !== --n);
        return B >= s ? s : B
    }

    function h(t) {
        for (var e, n, r = 0; ;) {
            if (MIN_LOOKAHEAD > B) {
                if (m(), MIN_LOOKAHEAD > B && t == Z_NO_FLUSH)return NeedMore;
                if (0 === B)break
            }
            if (B >= MIN_MATCH && (A = (A << F ^ 255 & k[L + (MIN_MATCH - 1)]) & R, r = 65535 & E[A], T[L & b] = E[A], E[A] = L), U = D, z = N, D = MIN_MATCH - 1, 0 !== r && H > U && q - MIN_LOOKAHEAD >= (L - r & 65535) && (Z != Z_HUFFMAN_ONLY && (D = p(r)), 5 >= D && (Z == Z_FILTERED || D == MIN_MATCH && L - N > 4096)) && (D = MIN_MATCH - 1), U >= MIN_MATCH && U >= D) {
                n = L + B - MIN_MATCH, e = u(L - 1 - z, U - MIN_MATCH), B -= U - 1, U -= 2;
                do++L <= n && (A = (A << F ^ 255 & k[L + (MIN_MATCH - 1)]) & R, r = 65535 & E[A], T[L & b] = E[A], E[A] = L); while (0 !== --U);
                if (M = 0, D = MIN_MATCH - 1, L++, e && (d(!1), 0 === y.avail_out))return NeedMore
            } else if (0 !== M) {
                if ((e = u(0, 255 & k[L - 1])) && d(!1), L++, B--, 0 === y.avail_out)return NeedMore
            } else M = 1, L++, B--
        }
        return 0 !== M && (u(0, 255 & k[L - 1]), M = 0), d(t == Z_FINISH), 0 === y.avail_out ? t == Z_FINISH ? FinishStarted : NeedMore : t == Z_FINISH ? FinishDone : BlockDone
    }

    var y, _, v, g, q, x, b, k, S, T, E, A, C, I, R, F, O, D, z, M, L, N, B, U, P, H, j, Z, K, W, Y, G, X, J = this, V = new Tree, Q = new Tree, $ = new Tree;
    J.depth = [];
    var tt, et, nt, rt, st, ot, it, at;
    J.bl_count = [], J.heap = [], Y = [], G = [], X = [], J.pqdownheap = function (t, e) {
        for (var n = J.heap, r = n[e], s = e << 1; s <= J.heap_len && (s < J.heap_len && smaller(t, n[s + 1], n[s], J.depth) && s++, !smaller(t, r, n[s], J.depth));)n[e] = n[s], e = s, s <<= 1;
        n[e] = r
    }, J.deflateInit = function (e, n, r, s, o, i) {
        if (s || (s = Z_DEFLATED), o || (o = DEF_MEM_LEVEL), i || (i = Z_DEFAULT_STRATEGY), e.msg = null, n == Z_DEFAULT_COMPRESSION && (n = 6), 1 > o || o > MAX_MEM_LEVEL || s != Z_DEFLATED || 9 > r || r > 15 || 0 > n || n > 9 || 0 > i || i > Z_HUFFMAN_ONLY)return Z_STREAM_ERROR;
        for (e.dstate = J, x = r, q = 1 << x, b = q - 1, I = o + 7, C = 1 << I, R = C - 1, F = Math.floor((I + MIN_MATCH - 1) / MIN_MATCH), k = new Uint8Array(2 * q), T = [], E = [], et = 1 << o + 6, J.pending_buf = new Uint8Array(4 * et), v = 4 * et, rt = Math.floor(et / 2), tt = 3 * et, j = n, Z = i, e.total_in = e.total_out = 0, e.msg = null, J.pending = 0, J.pending_out = 0, _ = BUSY_STATE, g = Z_NO_FLUSH, V.dyn_tree = Y, V.stat_desc = StaticTree.static_l_desc, Q.dyn_tree = G, Q.stat_desc = StaticTree.static_d_desc, $.dyn_tree = X, $.stat_desc = StaticTree.static_bl_desc, at = it = 0, ot = 8, t(), S = 2 * q, e = E[C - 1] = 0; C - 1 > e; e++)E[e] = 0;
        return H = config_table[j].max_lazy, K = config_table[j].good_length, W = config_table[j].nice_length, P = config_table[j].max_chain, B = O = L = 0, D = U = MIN_MATCH - 1, A = M = 0, Z_OK
    }, J.deflateEnd = function () {
        return _ != INIT_STATE && _ != BUSY_STATE && _ != FINISH_STATE ? Z_STREAM_ERROR : (k = T = E = J.pending_buf = null, J.dstate = null, _ == BUSY_STATE ? Z_DATA_ERROR : Z_OK)
    }, J.deflateParams = function (t, e, n) {
        var r = Z_OK;
        return e == Z_DEFAULT_COMPRESSION && (e = 6), 0 > e || e > 9 || 0 > n || n > Z_HUFFMAN_ONLY ? Z_STREAM_ERROR : (config_table[j].func != config_table[e].func && 0 !== t.total_in && (r = t.deflate(Z_PARTIAL_FLUSH)), j != e && (j = e, H = config_table[j].max_lazy, K = config_table[j].good_length, W = config_table[j].nice_length, P = config_table[j].max_chain), Z = n, r)
    }, J.deflateSetDictionary = function (t, e, n) {
        t = n;
        var r = 0;
        if (!e || _ != INIT_STATE)return Z_STREAM_ERROR;
        if (MIN_MATCH > t)return Z_OK;
        for (t > q - MIN_LOOKAHEAD && (t = q - MIN_LOOKAHEAD, r = n - t), k.set(e.subarray(r, r + t), 0), O = L = t, A = 255 & k[0], A = (A << F ^ 255 & k[1]) & R, e = 0; t - MIN_MATCH >= e; e++)A = (A << F ^ 255 & k[e + (MIN_MATCH - 1)]) & R, T[e & b] = E[A], E[A] = e;
        return Z_OK
    }, J.deflate = function (t, e) {
        var r, i, l;
        if (e > Z_FINISH || 0 > e)return Z_STREAM_ERROR;
        if (!t.next_out || !t.next_in && 0 !== t.avail_in || _ == FINISH_STATE && e != Z_FINISH)return t.msg = z_errmsg[Z_NEED_DICT - Z_STREAM_ERROR], Z_STREAM_ERROR;
        if (0 === t.avail_out)return t.msg = z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        if (y = t, r = g, g = e, _ == INIT_STATE && (i = Z_DEFLATED + (x - 8 << 4) << 8, l = (j - 1 & 255) >> 1, l > 3 && (l = 3), i |= l << 6, 0 !== L && (i |= PRESET_DICT), _ = BUSY_STATE, i += 31 - i % 31, n(i >> 8 & 255), n(255 & i)), 0 !== J.pending) {
            if (y.flush_pending(), 0 === y.avail_out)return g = -1, Z_OK
        } else if (0 === y.avail_in && r >= e && e != Z_FINISH)return y.msg = z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        if (_ == FINISH_STATE && 0 !== y.avail_in)return t.msg = z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        if (0 !== y.avail_in || 0 !== B || e != Z_NO_FLUSH && _ != FINISH_STATE) {
            switch (r = -1, config_table[j].func) {
                case STORED:
                    r = w(e);
                    break;
                case FAST:
                    t:{
                        for (r = 0; ;) {
                            if (MIN_LOOKAHEAD > B) {
                                if (m(), MIN_LOOKAHEAD > B && e == Z_NO_FLUSH) {
                                    r = NeedMore;
                                    break t
                                }
                                if (0 === B)break
                            }
                            if (B >= MIN_MATCH && (A = (A << F ^ 255 & k[L + (MIN_MATCH - 1)]) & R, r = 65535 & E[A], T[L & b] = E[A], E[A] = L), 0 !== r && q - MIN_LOOKAHEAD >= (L - r & 65535) && Z != Z_HUFFMAN_ONLY && (D = p(r)), D >= MIN_MATCH)if (i = u(L - N, D - MIN_MATCH), B -= D, H >= D && B >= MIN_MATCH) {
                                D--;
                                do L++, A = (A << F ^ 255 & k[L + (MIN_MATCH - 1)]) & R, r = 65535 & E[A], T[L & b] = E[A], E[A] = L; while (0 !== --D);
                                L++
                            } else L += D, D = 0, A = 255 & k[L], A = (A << F ^ 255 & k[L + 1]) & R; else i = u(0, 255 & k[L]), B--, L++;
                            if (i && (d(!1), 0 === y.avail_out)) {
                                r = NeedMore;
                                break t
                            }
                        }
                        d(e == Z_FINISH), r = 0 === y.avail_out ? e == Z_FINISH ? FinishStarted : NeedMore : e == Z_FINISH ? FinishDone : BlockDone
                    }
                    break;
                case SLOW:
                    r = h(e)
            }
            if ((r == FinishStarted || r == FinishDone) && (_ = FINISH_STATE), r == NeedMore || r == FinishStarted)return 0 === y.avail_out && (g = -1), Z_OK;
            if (r == BlockDone) {
                if (e == Z_PARTIAL_FLUSH)s(STATIC_TREES << 1, 3), o(END_BLOCK, StaticTree.static_ltree), a(), 9 > 1 + ot + 10 - at && (s(STATIC_TREES << 1, 3), o(END_BLOCK, StaticTree.static_ltree), a()), ot = 7; else if (f(0, 0, !1), e == Z_FULL_FLUSH)for (r = 0; C > r; r++)E[r] = 0;
                if (y.flush_pending(), 0 === y.avail_out)return g = -1, Z_OK
            }
        }
        return e != Z_FINISH ? Z_OK : Z_STREAM_END
    }
}
function ZStream() {
    this.total_out = this.avail_out = this.total_in = this.avail_in = this.next_out_index = this.next_in_index = 0
}
function Deflater(t) {
    var e = new ZStream, n = Z_NO_FLUSH, r = new Uint8Array(512);
    "undefined" == typeof t && (t = Z_DEFAULT_COMPRESSION), e.deflateInit(t), e.next_out = r, this.append = function (t, s) {
        var o, i, a = [], u = 0, l = 0, c = 0;
        if (t.length) {
            e.next_in_index = 0, e.next_in = t, e.avail_in = t.length;
            do {
                if (e.next_out_index = 0, e.avail_out = 512, o = e.deflate(n), o != Z_OK)throw"deflating: " + e.msg;
                e.next_out_index && (512 == e.next_out_index ? a.push(new Uint8Array(r)) : a.push(new Uint8Array(r.subarray(0, e.next_out_index)))), c += e.next_out_index, s && 0 < e.next_in_index && e.next_in_index != u && (s(e.next_in_index), u = e.next_in_index)
            } while (0 < e.avail_in || 0 === e.avail_out);
            return i = new Uint8Array(c), a.forEach(function (t) {
                i.set(t, l), l += t.length
            }), i
        }
    }, this.flush = function () {
        var t, n, s = [], o = 0, i = 0;
        do {
            if (e.next_out_index = 0, e.avail_out = 512, t = e.deflate(Z_FINISH), t != Z_STREAM_END && t != Z_OK)throw"deflating: " + e.msg;
            0 < 512 - e.avail_out && s.push(new Uint8Array(r.subarray(0, e.next_out_index))), i += e.next_out_index
        } while (0 < e.avail_in || 0 === e.avail_out);
        return e.deflateEnd(), n = new Uint8Array(i), s.forEach(function (t) {
            n.set(t, o), o += t.length
        }), n
    }
}
var jsPDF = function () {
    function t(r, s, o, i) {
        r = "undefined" == typeof r ? "p" : r.toString().toLowerCase(), "undefined" == typeof s && (s = "mm"), "undefined" == typeof o && (o = "a4"), "undefined" == typeof i && "undefined" == typeof zpipe && (i = !1);
        var a = o.toString().toLowerCase(), u = [], l = 0, c = i;
        i = {a3: [841.89, 1190.55], a4: [595.28, 841.89], a5: [420.94, 595.28], letter: [612, 792], legal: [612, 1008]};
        var f, d, m, w, p, h, y, _, v = "0 g", g = 0, q = [], x = 2, b = !1, k = [], S = {}, T = {}, E = 16, A = {
            title: "",
            subject: "",
            author: "",
            keywords: "",
            creator: ""
        }, C = 0, I = 0, R = {}, F = new n(R), O = function (t) {
            return t.toFixed(2)
        }, D = function (t) {
            var e = t.toFixed(0);
            return 10 > t ? "0" + e : e
        }, z = function (t) {
            b ? q[g].push(t) : (u.push(t), l += t.length + 1)
        }, M = function () {
            return x++, k[x] = l, z(x + " 0 obj"), x
        }, L = function (t) {
            z("stream"), z(t), z("endstream")
        }, N = function (t, e) {
            var n;
            n = t;
            var r, s, o, i, a, u, l = e;
            if (void 0 === l && (l = {}), r = l.sourceEncoding ? r : "Unicode", o = l.outputEncoding, (l.autoencode || o) && S[f].metadata && S[f].metadata[r] && S[f].metadata[r].encoding && (r = S[f].metadata[r].encoding, !o && S[f].encoding && (o = S[f].encoding), !o && r.codePages && (o = r.codePages[0]), "string" == typeof o && (o = r[o]), o)) {
                for (a = !1, i = [], r = 0, s = n.length; s > r; r++)(u = o[n.charCodeAt(r)]) ? i.push(String.fromCharCode(u)) : i.push(n[r]), i[r].charCodeAt(0) >> 8 && (a = !0);
                n = i.join("")
            }
            for (r = n.length; void 0 === a && 0 !== r;)n.charCodeAt(r - 1) >> 8 && (a = !0), r--;
            if (a) {
                for (i = l.noBOM ? [] : [254, 255], r = 0, s = n.length; s > r; r++) {
                    if (u = n.charCodeAt(r), l = u >> 8, l >> 8)throw Error("Character at position " + r.toString(10) + " of string '" + n + "' exceeds 16bits. Cannot be encoded into UCS-2 BE");
                    i.push(l), i.push(u - (l << 8))
                }
                n = String.fromCharCode.apply(void 0, i)
            }
            return n.replace(/\\/g, "\\\\").replace(/\(/g, "\\(").replace(/\)/g, "\\)")
        }, B = function () {
            g++, b = !0, q[g] = [], z(O(.200025 * w) + " w"), z("0 G"), 0 !== C && z(C.toString(10) + " J"), 0 !== I && z(I.toString(10) + " j"), F.publish("addPage", {pageNumber: g})
        }, U = function (t, e) {
            var n;
            void 0 === t && (t = S[f].fontName), void 0 === e && (e = S[f].fontStyle);
            try {
                n = T[t][e]
            } catch (r) {
                n = void 0
            }
            if (!n)throw Error("Unable to look up font label for font '" + t + "', '" + e + "'. Refer to getFontList() for available fonts.");
            return n
        }, P = function () {
            b = !1, u = [], k = [], z("%PDF-1.3"), h = m * w, y = d * w;
            var t, e, n, r, s;
            for (t = 1; g >= t; t++) {
                if (M(), z("<</Type /Page"), z("/Parent 1 0 R"), z("/Resources 2 0 R"), z("/Contents " + (x + 1) + " 0 R>>"), z("endobj"), e = q[t].join("\n"), M(), c) {
                    for (n = [], r = 0; r < e.length; ++r)n[r] = e.charCodeAt(r);
                    s = adler32cs.from(e), e = new Deflater(6), e.append(new Uint8Array(n)), e = e.flush(), n = [new Uint8Array([120, 156]), new Uint8Array(e), new Uint8Array([255 & s, s >> 8 & 255, s >> 16 & 255, s >> 24 & 255])], e = "";
                    for (r in n)n.hasOwnProperty(r) && (e += String.fromCharCode.apply(null, n[r]));
                    z("<</Length " + e.length + " /Filter [/FlateDecode]>>")
                } else z("<</Length " + e.length + ">>");
                L(e), z("endobj")
            }
            for (k[1] = l, z("1 0 obj"), z("<</Type /Pages"), _ = "/Kids [", r = 0; g > r; r++)_ += 3 + 2 * r + " 0 R ";
            z(_ + "]"), z("/Count " + g), z("/MediaBox [0 0 " + O(h) + " " + O(y) + "]"), z(">>"), z("endobj");
            for (var o in S)S.hasOwnProperty(o) && (t = S[o], t.objectNumber = M(), z("<</BaseFont/" + t.PostScriptName + "/Type/Font"), "string" == typeof t.encoding && z("/Encoding/" + t.encoding), z("/Subtype/Type1>>"), z("endobj"));
            F.publish("putResources"), k[2] = l, z("2 0 obj"), z("<<"), z("/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]"), z("/Font <<");
            for (var i in S)S.hasOwnProperty(i) && z("/" + i + " " + S[i].objectNumber + " 0 R");
            for (z(">>"), z("/XObject <<"), F.publish("putXobjectDict"), z(">>"), z(">>"), z("endobj"), F.publish("postPutResources"), M(), z("<<"), z("/Producer (jsPDF 20120619)"), A.title && z("/Title (" + N(A.title) + ")"), A.subject && z("/Subject (" + N(A.subject) + ")"), A.author && z("/Author (" + N(A.author) + ")"), A.keywords && z("/Keywords (" + N(A.keywords) + ")"), A.creator && z("/Creator (" + N(A.creator) + ")"), o = new Date, z("/CreationDate (D:" + [o.getFullYear(), D(o.getMonth() + 1), D(o.getDate()), D(o.getHours()), D(o.getMinutes()), D(o.getSeconds())].join("") + ")"), z(">>"), z("endobj"), M(), z("<<"), z("/Type /Catalog"), z("/Pages 1 0 R"), z("/OpenAction [3 0 R /FitH null]"), z("/PageLayout /OneColumn"), F.publish("putCatalog"), z(">>"), z("endobj"), o = l, z("xref"), z("0 " + (x + 1)), z("0000000000 65535 f "), i = 1; x >= i; i++)t = k[i].toFixed(0), t = 10 > t.length ? Array(11 - t.length).join("0") + t : t, z(t + " 00000 n ");
            return z("trailer"), z("<<"), z("/Size " + (x + 1)), z("/Root " + x + " 0 R"), z("/Info " + (x - 1) + " 0 R"), z(">>"), z("startxref"), z(o), z("%%EOF"), b = !0, u.join("\n")
        }, H = function (t) {
            var e = "S";
            return "F" === t ? e = "f" : ("FD" === t || "DF" === t) && (e = "B"), e
        }, j = function (t, e) {
            var n, r, s, o;
            switch (t) {
                case void 0:
                    return P();
                case"save":
                    if (navigator.getUserMedia && (void 0 === window.URL || void 0 === window.URL.createObjectURL))return R.output("dataurlnewwindow");
                    for (n = P(), r = n.length, s = new Uint8Array(new ArrayBuffer(r)), o = 0; r > o; o++)s[o] = n.charCodeAt(o);
                    n = new Blob([s], {type: "application/pdf"}), saveAs(n, e);
                    break;
                case"datauristring":
                case"dataurlstring":
                    return "data:application/pdf;base64," + btoa(P());
                case"datauri":
                case"dataurl":
                    document.location.href = "data:application/pdf;base64," + btoa(P());
                    break;
                case"dataurlnewwindow":
                    window.open("data:application/pdf;base64," + btoa(P()));
                    break;
                default:
                    throw Error('Output type "' + t + '" is not supported.')
            }
        };
        if ("pt" === s)w = 1; else if ("mm" === s)w = 72 / 25.4; else if ("cm" === s)w = 72 / 2.54; else {
            if ("in" !== s)throw"Invalid unit: " + s;
            w = 72
        }
        if (i.hasOwnProperty(a))d = i[a][1] / w, m = i[a][0] / w; else try {
            d = o[1], m = o[0]
        } catch (Z) {
            throw"Invalid format: " + o
        }
        if ("p" === r || "portrait" === r)r = "p", m > d && (r = m, m = d, d = r); else {
            if ("l" !== r && "landscape" !== r)throw"Invalid orientation: " + r;
            r = "l", d > m && (r = m, m = d, d = r)
        }
        R.internal = {
            pdfEscape: N,
            getStyle: H,
            getFont: function () {
                return S[U.apply(R, arguments)]
            },
            getFontSize: function () {
                return E
            },
            btoa: btoa,
            write: function (t, e, n, r) {
                z(1 === arguments.length ? t : Array.prototype.join.call(arguments, " "))
            },
            getCoordinateString: function (t) {
                return O(t * w)
            },
            getVerticalCoordinateString: function (t) {
                return O((d - t) * w)
            },
            collections: {},
            newObject: M,
            putStream: L,
            events: F,
            scaleFactor: w,
            pageSize: {width: m, height: d},
            output: function (t, e) {
                return j(t, e)
            }
        }, R.addPage = function () {
            return B(), this
        }, R.text = function (t, e, n, r) {
            var s, o;
            if ("number" == typeof t && (s = t, o = e, t = n, e = s, n = o), "string" == typeof t && t.match(/[\n\r]/) && (t = t.split(/\r\n|\r|\n/g)), "undefined" == typeof r ? r = {
                    noBOM: !0,
                    autoencode: !0
                } : (void 0 === r.noBOM && (r.noBOM = !0), void 0 === r.autoencode && (r.autoencode = !0)), "string" == typeof t)r = N(t, r); else {
                if (!(t instanceof Array))throw Error('Type of text must be string or Array. "' + t + '" is not recognized.');
                for (t = t.concat(), s = t.length - 1; -1 !== s; s--)t[s] = N(t[s], r);
                r = t.join(") Tj\nT* (")
            }
            return z("BT\n/" + f + " " + E + " Tf\n" + E + " TL\n" + v + "\n" + O(e * w) + " " + O((d - n) * w) + " Td\n(" + r + ") Tj\nET"), this
        }, R.line = function (t, e, n, r) {
            return z(O(t * w) + " " + O((d - e) * w) + " m " + O(n * w) + " " + O((d - r) * w) + " l S"), this
        }, R.lines = function (t, e, n, r, s) {
            var o, i, a, u, l, c, f, m;
            for ("number" == typeof t && (o = t, i = e, t = n, e = o, n = i), s = H(s), r = void 0 === r ? [1, 1] : r, z((e * w).toFixed(3) + " " + ((d - n) * w).toFixed(3) + " m "), o = r[0], r = r[1], i = t.length, m = n, n = 0; i > n; n++)a = t[n], 2 === a.length ? (e = a[0] * o + e, m = a[1] * r + m, z((e * w).toFixed(3) + " " + ((d - m) * w).toFixed(3) + " l")) : (u = a[0] * o + e, l = a[1] * r + m, c = a[2] * o + e, f = a[3] * r + m, e = a[4] * o + e, m = a[5] * r + m, z((u * w).toFixed(3) + " " + ((d - l) * w).toFixed(3) + " " + (c * w).toFixed(3) + " " + ((d - f) * w).toFixed(3) + " " + (e * w).toFixed(3) + " " + ((d - m) * w).toFixed(3) + " c"));
            return z(s), this
        }, R.rect = function (t, e, n, r, s) {
            return s = H(s), z([O(t * w), O((d - e) * w), O(n * w), O(-r * w), "re", s].join(" ")), this
        }, R.triangle = function (t, e, n, r, s, o, i) {
            return this.lines([[n - t, r - e], [s - n, o - r], [t - s, e - o]], t, e, [1, 1], i), this
        }, R.roundedRect = function (t, e, n, r, s, o, i) {
            var a = 4 / 3 * (Math.SQRT2 - 1);
            return this.lines([[n - 2 * s, 0], [s * a, 0, s, o - o * a, s, o], [0, r - 2 * o], [0, o * a, -(s * a), o, -s, o], [-n + 2 * s, 0], [-(s * a), 0, -s, -(o * a), -s, -o], [0, -r + 2 * o], [0, -(o * a), s * a, -o, s, -o]], t + s, e, [1, 1], i), this
        }, R.ellipse = function (t, e, n, r, s) {
            s = H(s);
            var o = 4 / 3 * (Math.SQRT2 - 1) * n, i = 4 / 3 * (Math.SQRT2 - 1) * r;
            return z([O((t + n) * w), O((d - e) * w), "m", O((t + n) * w), O((d - (e - i)) * w), O((t + o) * w), O((d - (e - r)) * w), O(t * w), O((d - (e - r)) * w), "c"].join(" ")), z([O((t - o) * w), O((d - (e - r)) * w), O((t - n) * w), O((d - (e - i)) * w), O((t - n) * w), O((d - e) * w), "c"].join(" ")), z([O((t - n) * w), O((d - (e + i)) * w), O((t - o) * w), O((d - (e + r)) * w), O(t * w), O((d - (e + r)) * w), "c"].join(" ")), z([O((t + o) * w), O((d - (e + r)) * w), O((t + n) * w), O((d - (e + i)) * w), O((t + n) * w), O((d - e) * w), "c", s].join(" ")), this
        }, R.circle = function (t, e, n, r) {
            return this.ellipse(t, e, n, n, r)
        }, R.setProperties = function (t) {
            for (var e in A)A.hasOwnProperty(e) && t[e] && (A[e] = t[e]);
            return this
        }, R.setFontSize = function (t) {
            return E = t, this
        }, R.setFont = function (t, e) {
            return f = U(t, e), this
        }, R.setFontStyle = R.setFontType = function (t) {
            return f = U(void 0, t), this
        }, R.getFontList = function () {
            var t, e, n, r = {};
            for (t in T)if (T.hasOwnProperty(t))for (e in r[t] = n = [], T[t])T[t].hasOwnProperty(e) && n.push(e);
            return r
        }, R.setLineWidth = function (t) {
            return z((t * w).toFixed(2) + " w"), this
        }, R.setDrawColor = function (t, e, n, r) {
            return t = void 0 === e || void 0 === r && t === e === n ? "string" == typeof t ? t + " G" : O(t / 255) + " G" : void 0 === r ? "string" == typeof t ? [t, e, n, "RG"].join(" ") : [O(t / 255), O(e / 255), O(n / 255), "RG"].join(" ") : "string" == typeof t ? [t, e, n, r, "K"].join(" ") : [O(t), O(e), O(n), O(r), "K"].join(" "), z(t), this
        }, R.setFillColor = function (t, e, n, r) {
            return t = void 0 === e || void 0 === r && t === e === n ? "string" == typeof t ? t + " g" : O(t / 255) + " g" : void 0 === r ? "string" == typeof t ? [t, e, n, "rg"].join(" ") : [O(t / 255), O(e / 255), O(n / 255), "rg"].join(" ") : "string" == typeof t ? [t, e, n, r, "k"].join(" ") : [O(t), O(e), O(n), O(r), "k"].join(" "), z(t), this
        }, R.setTextColor = function (t, e, n) {
            return v = 0 === t && 0 === e && 0 === n || "undefined" == typeof e ? (t / 255).toFixed(3) + " g" : [(t / 255).toFixed(3), (e / 255).toFixed(3), (n / 255).toFixed(3), "rg"].join(" "), this
        }, R.CapJoinStyles = {
            0: 0,
            butt: 0,
            but: 0,
            bevel: 0,
            1: 1,
            round: 1,
            rounded: 1,
            circle: 1,
            2: 2,
            projecting: 2,
            project: 2,
            square: 2,
            milter: 2
        }, R.setLineCap = function (t) {
            var e = this.CapJoinStyles[t];
            if (void 0 === e)throw Error("Line cap style of '" + t + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
            return C = e, z(e.toString(10) + " J"), this
        }, R.setLineJoin = function (t) {
            var e = this.CapJoinStyles[t];
            if (void 0 === e)throw Error("Line join style of '" + t + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
            return I = e, z(e.toString(10) + " j"), this
        }, R.output = j, R.save = function (t) {
            R.output("save", t)
        };
        for (p in t.API)if (t.API.hasOwnProperty(p))if ("events" === p && t.API.events.length)for (r = F, s = t.API.events, i = a = o = void 0, i = s.length - 1; -1 !== i; i--)o = s[i][0], a = s[i][1], r.subscribe.apply(r, [o].concat("function" == typeof a ? [a] : a)); else R[p] = t.API[p];
        for (p = [["Helvetica", "helvetica", "normal"], ["Helvetica-Bold", "helvetica", "bold"], ["Helvetica-Oblique", "helvetica", "italic"], ["Helvetica-BoldOblique", "helvetica", "bolditalic"], ["Courier", "courier", "normal"], ["Courier-Bold", "courier", "bold"], ["Courier-Oblique", "courier", "italic"], ["Courier-BoldOblique", "courier", "bolditalic"], ["Times-Roman", "times", "normal"], ["Times-Bold", "times", "bold"], ["Times-Italic", "times", "italic"], ["Times-BoldItalic", "times", "bolditalic"]], r = 0, s = p.length; s > r; r++) {
            i = p[r][0];
            var K = p[r][1], a = p[r][2];
            o = "F" + (e(S) + 1).toString(10), i = S[o] = {
                id: o,
                PostScriptName: i,
                fontName: K,
                fontStyle: a,
                encoding: "StandardEncoding",
                metadata: {}
            };
            var W = o;
            void 0 === T[K] && (T[K] = {}), T[K][a] = W, F.publish("addFont", i), a = o, o = p[r][0].split("-"), i = o[0], o = o[1] || "", void 0 === T[i] && (T[i] = {}), T[i][o] = a
        }
        return F.publish("addFonts", {fonts: S, dictionary: T}), f = "F1", B(), F.publish("initialized"), R
    }

    "undefined" == typeof btoa && (window.btoa = function (t) {
        var e, n, r, s, o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".split(""), i = 0, a = 0, u = "", u = [];
        do e = t.charCodeAt(i++), n = t.charCodeAt(i++), r = t.charCodeAt(i++), s = e << 16 | n << 8 | r, e = s >> 18 & 63, n = s >> 12 & 63, r = s >> 6 & 63, s &= 63, u[a++] = o[e] + o[n] + o[r] + o[s]; while (i < t.length);
        return u = u.join(""), t = t.length % 3, (t ? u.slice(0, t - 3) : u) + "===".slice(t || 3)
    }), "undefined" == typeof atob && (window.atob = function (t) {
        var e, n, r, s, o, i = 0, a = 0;
        s = "";
        var u = [];
        if (!t)return t;
        t += "";
        do e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(t.charAt(i++)), n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(t.charAt(i++)), s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(t.charAt(i++)), o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(t.charAt(i++)), r = e << 18 | n << 12 | s << 6 | o, e = r >> 16 & 255, n = r >> 8 & 255, r &= 255, 64 === s ? u[a++] = String.fromCharCode(e) : 64 === o ? u[a++] = String.fromCharCode(e, n) : u[a++] = String.fromCharCode(e, n, r); while (i < t.length);
        return s = u.join("")
    });
    var e = "function" == typeof Object.keys ? function (t) {
        return Object.keys(t).length
    } : function (t) {
        var e, n = 0;
        for (e in t)t.hasOwnProperty(e) && n++;
        return n
    }, n = function (t) {
        this.topics = {}, this.context = t, this.publish = function (t, e) {
            if (this.topics[t]) {
                var n, r, s, o, i = this.topics[t], a = [], u = function () {
                };
                for (e = Array.prototype.slice.call(arguments, 1), r = 0, s = i.length; s > r; r++)o = i[r], n = o[0], o[1] && (o[0] = u, a.push(r)), n.apply(this.context, e);
                for (r = 0, s = a.length; s > r; r++)i.splice(a[r], 1)
            }
        }, this.subscribe = function (t, e, n) {
            return this.topics[t] ? this.topics[t].push([e, n]) : this.topics[t] = [[e, n]], {topic: t, callback: e}
        }, this.unsubscribe = function (t) {
            if (this.topics[t.topic]) {
                var e, n, r = this.topics[t.topic];
                for (e = 0, n = r.length; n > e; e++)r[e][0] === t.callback && r.splice(e, 1)
            }
        }
    };
    return t.API = {events: []}, t
}();
!function (t) {
    var e = function () {
        var t, e = this.internal.collections.addImage_images;
        for (t in e) {
            var n = e[t], r = this.internal.newObject(), s = this.internal.write, o = this.internal.putStream;
            if (n.n = r, s("<</Type /XObject"), s("/Subtype /Image"), s("/Width " + n.w), s("/Height " + n.h), "Indexed" === n.cs ? s("/ColorSpace [/Indexed /DeviceRGB " + (n.pal.length / 3 - 1) + " " + (r + 1) + " 0 R]") : (s("/ColorSpace /" + n.cs), "DeviceCMYK" === n.cs && s("/Decode [1 0 1 0 1 0 1 0]")), s("/BitsPerComponent " + n.bpc), "f"in n && s("/Filter /" + n.f), "dp"in n && s("/DecodeParms <<" + n.dp + ">>"), "trns"in n && n.trns.constructor == Array)for (var i = "", a = 0; a < n.trns.length; a++)i += n[i][a] + " " + n.trns[a] + " ", s("/Mask [" + i + "]");
            "smask"in n && s("/SMask " + (r + 1) + " 0 R"), s("/Length " + n.data.length + ">>"), o(n.data), s("endobj")
        }
    }, n = function () {
        var t, e, n = this.internal.collections.addImage_images, r = this.internal.write;
        for (e in n)t = n[e], r("/I" + t.i, t.n, "0", "R")
    };
    t.addImage = function (t, r, s, o, i, a) {
        if ("object" == typeof t && 1 === t.nodeType) {
            r = document.createElement("canvas"), r.width = t.clientWidth, r.height = t.clientHeight;
            var u = r.getContext("2d");
            if (!u)throw"addImage requires canvas to be supported by browser.";
            u.drawImage(t, 0, 0, r.width, r.height), t = r.toDataURL("image/jpeg"), r = "JPEG"
        }
        if ("JPEG" !== r.toUpperCase())throw Error("addImage currently only supports format 'JPEG', not '" + r + "'");
        var l;
        r = this.internal.collections.addImage_images;
        var u = this.internal.getCoordinateString, c = this.internal.getVerticalCoordinateString;
        if ("data:image/jpeg;base64," === t.substring(0, 23) && (t = atob(t.replace("data:image/jpeg;base64,", ""))), r)if (Object.keys)l = Object.keys(r).length; else {
            var f = r, d = 0;
            for (l in f)f.hasOwnProperty(l) && d++;
            l = d
        } else l = 0, this.internal.collections.addImage_images = r = {}, this.internal.events.subscribe("putResources", e), this.internal.events.subscribe("putXobjectDict", n);
        t:{
            var m, f = t;
            if (255 === !f.charCodeAt(0) || 216 === !f.charCodeAt(1) || 255 === !f.charCodeAt(2) || 224 === !f.charCodeAt(3) || 74 === !f.charCodeAt(6) || 70 === !f.charCodeAt(7) || 73 === !f.charCodeAt(8) || 70 === !f.charCodeAt(9) || 0 === !f.charCodeAt(10))throw Error("getJpegSize requires a binary jpeg file");
            m = 256 * f.charCodeAt(4) + f.charCodeAt(5);
            for (var d = 4, w = f.length; w > d;) {
                if (d += m, 255 !== f.charCodeAt(d))throw Error("getJpegSize could not find the size of the image");
                if (192 === f.charCodeAt(d + 1)) {
                    m = 256 * f.charCodeAt(d + 5) + f.charCodeAt(d + 6), f = 256 * f.charCodeAt(d + 7) + f.charCodeAt(d + 8), f = [f, m];
                    break t
                }
                d += 2, m = 256 * f.charCodeAt(d) + f.charCodeAt(d + 1)
            }
            f = void 0
        }
        return t = {
            w: f[0],
            h: f[1],
            cs: "DeviceRGB",
            bpc: 8,
            f: "DCTDecode",
            i: l,
            data: t
        }, r[l] = t, !i && !a && (a = i = -96), 0 > i && (i = -72 * t.w / i / this.internal.scaleFactor), 0 > a && (a = -72 * t.h / a / this.internal.scaleFactor), 0 === i && (i = a * t.w / t.h), 0 === a && (a = i * t.h / t.w), this.internal.write("q", u(i), "0 0", u(a), u(s), c(o + a), "cm /I" + t.i, "Do Q"), this
    }
}(jsPDF.API), function (t) {
    function e(t, e, n, r) {
        return this.pdf = t, this.x = e, this.y = n, this.settings = r, this.init(), this
    }

    function n(t) {
        var e = a[t];
        return e ? e : (e = {
            "xx-small": 9,
            "x-small": 11,
            small: 13,
            medium: 16,
            large: 19,
            "x-large": 23,
            "xx-large": 28,
            auto: 0
        }[t], void 0 !== e || (e = parseFloat(t)) ? a[t] = e / 16 : (e = t.match(/([\d\.]+)(px)/), 3 === e.length ? a[t] = parseFloat(e[1]) / 16 : a[t] = 1))
    }

    function r(t, e, a) {
        var u, l = t.childNodes;
        u = $(t), t = {};
        for (var c, f = u.css("font-family").split(","), d = f.shift(); !c && d;)c = s[d.trim().toLowerCase()], d = f.shift();
        for (t["font-family"] = c || "times", t["font-style"] = i[u.css("font-style")] || "normal", c = o[u.css("font-weight")] || "normal", "bold" === c && (t["font-style"] = "normal" === t["font-style"] ? c : c + t["font-style"]), t["font-size"] = n(u.css("font-size")) || 1, t["line-height"] = n(u.css("line-height")) || 1, t.display = "inline" === u.css("display") ? "inline" : "block", "block" === t.display && (t["margin-top"] = n(u.css("margin-top")) || 0, t["margin-bottom"] = n(u.css("margin-bottom")) || 0, t["padding-top"] = n(u.css("padding-top")) || 0, t["padding-bottom"] = n(u.css("padding-bottom")) || 0), (c = "block" === t.display) && (e.setBlockBoundary(), e.setBlockStyle(t)), f = 0, d = l.length; d > f; f++)if (u = l[f], "object" == typeof u)if (1 === u.nodeType && "SCRIPT" != u.nodeName) {
            var m = u, w = e, p = a, h = !1, y = void 0, _ = void 0, v = p["#" + m.id];
            if (v)if ("function" == typeof v)h = v(m, w); else for (y = 0, _ = v.length; !h && y !== _;)h = v[y](m, w), y++;
            if (v = p[m.nodeName], !h && v)if ("function" == typeof v)h = v(m, w); else for (y = 0, _ = v.length; !h && y !== _;)h = v[y](m, w), y++;
            h || r(u, e, a)
        } else 3 === u.nodeType && e.addText(u.nodeValue, t); else"string" == typeof u && e.addText(u, t);
        c && e.setBlockBoundary()
    }

    String.prototype.trim || (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    }), String.prototype.trimLeft || (String.prototype.trimLeft = function () {
        return this.replace(/^\s+/g, "")
    }), String.prototype.trimRight || (String.prototype.trimRight = function () {
        return this.replace(/\s+$/g, "")
    }), e.prototype.init = function () {
        this.paragraph = {text: [], style: []}, this.pdf.internal.write("q")
    }, e.prototype.dispose = function () {
        return this.pdf.internal.write("Q"), {x: this.x, y: this.y}
    }, e.prototype.splitFragmentsIntoLines = function (t, e) {
        for (var n, r, s, o, i, a = this.pdf.internal.scaleFactor, u = {}, l = [], c = [l], f = 0, d = this.settings.width; t.length;)if (o = t.shift(), i = e.shift(), o)if (n = i["font-family"], r = i["font-style"], s = u[n + r], s || (s = this.pdf.internal.getFont(n, r).metadata.Unicode, u[n + r] = s), n = {
                widths: s.widths,
                kerning: s.kerning,
                fontSize: 12 * i["font-size"],
                textIndent: f
            }, r = this.pdf.getStringUnitWidth(o, n) * n.fontSize / a, f + r > d) {
            for (o = this.pdf.splitTextToSize(o, d, n), l.push([o.shift(), i]); o.length;)l = [[o.shift(), i]], c.push(l);
            f = this.pdf.getStringUnitWidth(l[0][0], n) * n.fontSize / a
        } else l.push([o, i]), f += r;
        return c
    }, e.prototype.RenderTextFragment = function (t, e) {
        var n = this.pdf.internal.getFont(e["font-family"], e["font-style"]);
        this.pdf.internal.write("/" + n.id, (12 * e["font-size"]).toFixed(2), "Tf", "(" + this.pdf.internal.pdfEscape(t) + ") Tj")
    }, e.prototype.renderParagraph = function () {
        for (var t, e = this.paragraph.text, n = 0, r = e.length, s = !1, o = !1; !s && n !== r;)(t = e[n] = e[n].trimLeft()) && (s = !0), n++;
        for (n = r - 1; r && !o && -1 !== n;)(t = e[n] = e[n].trimRight()) && (o = !0), n--;
        for (s = /\s+$/g, o = !0, n = 0; n !== r; n++)t = e[n].replace(/\s+/g, " "), o && (t = t.trimLeft()), t && (o = s.test(t)), e[n] = t;
        if (n = this.paragraph.style, t = (r = this.paragraph.blockstyle) || {}, this.paragraph = {
                text: [],
                style: [],
                blockstyle: {},
                priorblockstyle: r
            }, e.join("").trim()) {
            e = this.splitFragmentsIntoLines(e, n), n = 12 / this.pdf.internal.scaleFactor, s = (Math.max((r["margin-top"] || 0) - (t["margin-bottom"] || 0), 0) + (r["padding-top"] || 0)) * n, r = ((r["margin-bottom"] || 0) + (r["padding-bottom"] || 0)) * n, t = this.pdf.internal.write;
            var i, a;
            for (this.y += s, t("q", "BT", this.pdf.internal.getCoordinateString(this.x), this.pdf.internal.getVerticalCoordinateString(this.y), "Td"); e.length;) {
                for (s = e.shift(), i = o = 0, a = s.length; i !== a; i++)s[i][0].trim() && (o = Math.max(o, s[i][1]["line-height"], s[i][1]["font-size"]));
                for (t(0, (-12 * o).toFixed(2), "Td"), i = 0, a = s.length; i !== a; i++)s[i][0] && this.RenderTextFragment(s[i][0], s[i][1]);
                this.y += o * n
            }
            t("ET", "Q"), this.y += r
        }
    }, e.prototype.setBlockBoundary = function () {
        this.renderParagraph()
    }, e.prototype.setBlockStyle = function (t) {
        this.paragraph.blockstyle = t
    }, e.prototype.addText = function (t, e) {
        this.paragraph.text.push(t), this.paragraph.style.push(e)
    };
    var s = {
        helvetica: "helvetica",
        "sans-serif": "helvetica",
        serif: "times",
        times: "times",
        "times new roman": "times",
        monospace: "courier",
        courier: "courier"
    }, o = {
        100: "normal",
        200: "normal",
        300: "normal",
        400: "normal",
        500: "bold",
        600: "bold",
        700: "bold",
        800: "bold",
        900: "bold",
        normal: "normal",
        bold: "bold",
        bolder: "bold",
        lighter: "normal"
    }, i = {normal: "normal", italic: "italic", oblique: "italic"}, a = {normal: 1};
    t.fromHTML = function (t, n, s, o) {
        if ("string" == typeof t) {
            var i = "jsPDFhtmlText" + Date.now().toString() + (1e3 * Math.random()).toFixed(0);
            $('<div style="position: absolute !important;clip: rect(1px 1px 1px 1px); /* IE6, IE7 */clip: rect(1px, 1px, 1px, 1px);padding:0 !important;border:0 !important;height: 1px !important;width: 1px !important; top:auto;left:-100px;overflow: hidden;"><iframe style="height:1px;width:1px" name="' + i + '" /></div>').appendTo(document.body), t = $(window.frames[i].document.body).html(t)[0]
        }
        return n = new e(this, n, s, o), r(t, n, o.elementHandlers), n.dispose()
    }
}(jsPDF.API), function (t) {
    t.addSVG = function (t, e, n, r, s) {
        if (void 0 === e || void 0 === e)throw Error("addSVG needs values for 'x' and 'y'");
        var o = document.createElement("iframe"), i = document.createElement("style");
        i.type = "text/css", i.styleSheet ? i.styleSheet.cssText = ".jsPDF_sillysvg_iframe {display:none;position:absolute;}" : i.appendChild(document.createTextNode(".jsPDF_sillysvg_iframe {display:none;position:absolute;}")), document.getElementsByTagName("head")[0].appendChild(i), o.name = "childframe", o.setAttribute("width", 0), o.setAttribute("height", 0), o.setAttribute("frameborder", "0"), o.setAttribute("scrolling", "no"), o.setAttribute("seamless", "seamless"), o.setAttribute("class", "jsPDF_sillysvg_iframe"), document.body.appendChild(o), o = (o.contentWindow || o.contentDocument).document, o.write(t), o.close(), o = o.getElementsByTagName("svg")[0], t = [1, 1];
        var i = parseFloat(o.getAttribute("width")), a = parseFloat(o.getAttribute("height"));
        for (i && a && (r && s ? t = [r / i, s / a] : r ? t = [r / i, r / i] : s && (t = [s / a, s / a])), o = o.childNodes, r = 0, s = o.length; s > r; r++)if (i = o[r], i.tagName && "PATH" === i.tagName.toUpperCase()) {
            for (var i = i.getAttribute("d").split(" "), a = parseFloat(i[1]), u = parseFloat(i[2]), l = [], c = 3, f = i.length; f > c;)"c" === i[c] ? (l.push([parseFloat(i[c + 1]), parseFloat(i[c + 2]), parseFloat(i[c + 3]), parseFloat(i[c + 4]), parseFloat(i[c + 5]), parseFloat(i[c + 6])]), c += 7) : "l" === i[c] ? (l.push([parseFloat(i[c + 1]), parseFloat(i[c + 2])]), c += 3) : c += 1;
            i = [a, u, l], i[0] = i[0] * t[0] + e, i[1] = i[1] * t[1] + n, this.lines.call(this, i[2], i[0], i[1], t)
        }
        return this
    }
}(jsPDF.API), function (t) {
    var e = t.getCharWidthsArray = function (t, e) {
        e || (e = {});
        var n, r, s, o = e.widths ? e.widths : this.internal.getFont().metadata.Unicode.widths, i = o.fof ? o.fof : 1, a = e.kerning ? e.kerning : this.internal.getFont().metadata.Unicode.kerning, u = a.fof ? a.fof : 1, l = 0, c = o[0] || i, f = [];
        for (n = 0, r = t.length; r > n; n++)s = t.charCodeAt(n), f.push((o[s] || c) / i + (a[s] && a[s][l] || 0) / u), l = s;
        return f
    }, n = function (t) {
        for (var e = t.length, n = 0; e;)e--, n += t[e];
        return n
    };
    t.getStringUnitWidth = function (t, r) {
        return n(e.call(this, t, r))
    };
    var r = function (t, r, s) {
        s || (s = {});
        var o = e(" ", s)[0], i = t.split(" "), a = [];
        t = [a];
        var u, l, c, f, d = s.textIndent || 0, m = 0, w = 0;
        for (c = 0, f = i.length; f > c; c++) {
            if (u = i[c], l = e(u, s), w = n(l), d + m + w > r) {
                if (w > r) {
                    for (var w = u, p = l, h = r, y = [], _ = 0, v = w.length, g = 0; _ !== v && g + p[_] < r - (d + m);)g += p[_], _++;
                    for (y.push(w.slice(0, _)), d = _, g = 0; _ !== v;)g + p[_] > h && (y.push(w.slice(d, _)), g = 0, d = _), g += p[_], _++;
                    for (d !== _ && y.push(w.slice(d, _)), d = y, a.push(d.shift()), a = [d.pop()]; d.length;)t.push([d.shift()]);
                    w = n(l.slice(u.length - a[0].length))
                } else a = [u];
                t.push(a), d = w
            } else a.push(u), d += m + w;
            m = o
        }
        for (r = [], c = 0, f = t.length; f > c; c++)r.push(t[c].join(" "));
        return r
    };
    t.splitTextToSize = function (t, e, n) {
        n || (n = {});
        var s, o = n.fontSize || this.internal.getFontSize(), i = n;
        s = {0: 1};
        var a = {};
        for (i.widths && i.kerning ? s = {
            widths: i.widths,
            kerning: i.kerning
        } : (i = this.internal.getFont(i.fontName, i.fontStyle), s = i.metadata.Unicode ? {
            widths: i.metadata.Unicode.widths || s,
            kerning: i.metadata.Unicode.kerning || a
        } : {
            widths: s,
            kerning: a
        }), t = t.match(/[\n\r]/) ? t.split(/\r\n|\r|\n/g) : [t], e = 1 * this.internal.scaleFactor * e / o, s.textIndent = n.textIndent ? 1 * n.textIndent * this.internal.scaleFactor / o : 0, a = [], n = 0, o = t.length; o > n; n++)a = a.concat(r(t[n], e, s));
        return a
    }
}(jsPDF.API), function (t) {
    var e = function (t) {
        for (var e = {}, n = 0; 16 > n; n++)e["klmnopqrstuvwxyz"[n]] = "0123456789abcdef"[n];
        for (var r, s, o, i = {}, a = 1, u = i, l = [], c = "", f = "", d = t.length - 1, n = 1; n != d;)s = t[n], n += 1, "'" == s ? r ? (o = r.join(""), r = void 0) : r = [] : r ? r.push(s) : "{" == s ? (l.push([u, o]), u = {}, o = void 0) : "}" == s ? (s = l.pop(), s[0][s[1]] = u, o = void 0, u = s[0]) : "-" == s ? a = -1 : void 0 === o ? e.hasOwnProperty(s) ? (c += e[s], o = parseInt(c, 16) * a, a = 1, c = "") : c += s : e.hasOwnProperty(s) ? (f += e[s], u[o] = parseInt(f, 16) * a, a = 1, o = void 0, f = "") : f += s;
        return i
    }, n = {
        codePages: ["WinAnsiEncoding"],
        WinAnsiEncoding: e("{19m8n201n9q201o9r201s9l201t9m201u8m201w9n201x9o201y8o202k8q202l8r202m9p202q8p20aw8k203k8t203t8v203u9v2cq8s212m9t15m8w15n9w2dw9s16k8u16l9u17s9z17x8y17y9y}")
    }, r = {
        Unicode: {
            Courier: n,
            "Courier-Bold": n,
            "Courier-BoldOblique": n,
            "Courier-Oblique": n,
            Helvetica: n,
            "Helvetica-Bold": n,
            "Helvetica-BoldOblique": n,
            "Helvetica-Oblique": n,
            "Times-Roman": n,
            "Times-Bold": n,
            "Times-BoldItalic": n,
            "Times-Italic": n
        }
    }, s = {
        Unicode: {
            "Courier-Oblique": e("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
            "Times-BoldItalic": e("{'widths'{k3o2q4ycx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2r202m2n2n3m2o3m2p5n202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5n4l4m4m4m4n4m4o4s4p4m4q4m4r4s4s4y4t2r4u3m4v4m4w3x4x5t4y4s4z4s5k3x5l4s5m4m5n3r5o3x5p4s5q4m5r5t5s4m5t3x5u3x5v2l5w1w5x2l5y3t5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q2l6r3m6s3r6t1w6u1w6v3m6w1w6x4y6y3r6z3m7k3m7l3m7m2r7n2r7o1w7p3r7q2w7r4m7s3m7t2w7u2r7v2n7w1q7x2n7y3t202l3mcl4mal2ram3man3mao3map3mar3mas2lat4uau1uav3maw3way4uaz2lbk2sbl3t'fof'6obo2lbp3tbq3mbr1tbs2lbu1ybv3mbz3mck4m202k3mcm4mcn4mco4mcp4mcq5ycr4mcs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz2w203k6o212m6o2dw2l2cq2l3t3m3u2l17s3x19m3m}'kerning'{cl{4qu5kt5qt5rs17ss5ts}201s{201ss}201t{cks4lscmscnscoscpscls2wu2yu201ts}201x{2wu2yu}2k{201ts}2w{4qx5kx5ou5qx5rs17su5tu}2x{17su5tu5ou}2y{4qx5kx5ou5qx5rs17ss5ts}'fof'-6ofn{17sw5tw5ou5qw5rs}7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qs}3v{17su5tu5os5qs}7p{17su5tu}ck{4qu5kt5qt5rs17ss5ts}4l{4qu5kt5qt5rs17ss5ts}cm{4qu5kt5qt5rs17ss5ts}cn{4qu5kt5qt5rs17ss5ts}co{4qu5kt5qt5rs17ss5ts}cp{4qu5kt5qt5rs17ss5ts}6l{4qu5ou5qw5rt17su5tu}5q{ckuclucmucnucoucpu4lu}5r{ckuclucmucnucoucpu4lu}7q{cksclscmscnscoscps4ls}6p{4qu5ou5qw5rt17sw5tw}ek{4qu5ou5qw5rt17su5tu}el{4qu5ou5qw5rt17su5tu}em{4qu5ou5qw5rt17su5tu}en{4qu5ou5qw5rt17su5tu}eo{4qu5ou5qw5rt17su5tu}ep{4qu5ou5qw5rt17su5tu}es{17ss5ts5qs4qu}et{4qu5ou5qw5rt17sw5tw}eu{4qu5ou5qw5rt17ss5ts}ev{17ss5ts5qs4qu}6z{17sw5tw5ou5qw5rs}fm{17sw5tw5ou5qw5rs}7n{201ts}fo{17sw5tw5ou5qw5rs}fp{17sw5tw5ou5qw5rs}fq{17sw5tw5ou5qw5rs}7r{cksclscmscnscoscps4ls}fs{17sw5tw5ou5qw5rs}ft{17su5tu}fu{17su5tu}fv{17su5tu}fw{17su5tu}fz{cksclscmscnscoscps4ls}}}"),
            "Helvetica-Bold": e("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
            Courier: e("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
            "Courier-BoldOblique": e("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
            "Times-Bold": e("{'widths'{k3q2q5ncx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2l202m2n2n3m2o3m2p6o202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5x4l4s4m4m4n4s4o4s4p4m4q3x4r4y4s4y4t2r4u3m4v4y4w4m4x5y4y4s4z4y5k3x5l4y5m4s5n3r5o4m5p4s5q4s5r6o5s4s5t4s5u4m5v2l5w1w5x2l5y3u5z3m6k2l6l3m6m3r6n2w6o3r6p2w6q2l6r3m6s3r6t1w6u2l6v3r6w1w6x5n6y3r6z3m7k3r7l3r7m2w7n2r7o2l7p3r7q3m7r4s7s3m7t3m7u2w7v2r7w1q7x2r7y3o202l3mcl4sal2lam3man3mao3map3mar3mas2lat4uau1yav3maw3tay4uaz2lbk2sbl3t'fof'6obo2lbp3rbr1tbs2lbu2lbv3mbz3mck4s202k3mcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3rek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3m3u2l17s4s19m3m}'kerning'{cl{4qt5ks5ot5qy5rw17sv5tv}201t{cks4lscmscnscoscpscls4wv}2k{201ts}2w{4qu5ku7mu5os5qx5ru17su5tu}2x{17su5tu5ou5qs}2y{4qv5kv7mu5ot5qz5ru17su5tu}'fof'-6o7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qu}3v{17su5tu5os5qu}fu{17su5tu5ou5qu}7p{17su5tu5ou5qu}ck{4qt5ks5ot5qy5rw17sv5tv}4l{4qt5ks5ot5qy5rw17sv5tv}cm{4qt5ks5ot5qy5rw17sv5tv}cn{4qt5ks5ot5qy5rw17sv5tv}co{4qt5ks5ot5qy5rw17sv5tv}cp{4qt5ks5ot5qy5rw17sv5tv}6l{17st5tt5ou5qu}17s{ckuclucmucnucoucpu4lu4wu}5o{ckuclucmucnucoucpu4lu4wu}5q{ckzclzcmzcnzcozcpz4lz4wu}5r{ckxclxcmxcnxcoxcpx4lx4wu}5t{ckuclucmucnucoucpu4lu4wu}7q{ckuclucmucnucoucpu4lu}6p{17sw5tw5ou5qu}ek{17st5tt5qu}el{17st5tt5ou5qu}em{17st5tt5qu}en{17st5tt5qu}eo{17st5tt5qu}ep{17st5tt5ou5qu}es{17ss5ts5qu}et{17sw5tw5ou5qu}eu{17sw5tw5ou5qu}ev{17ss5ts5qu}6z{17sw5tw5ou5qu5rs}fm{17sw5tw5ou5qu5rs}fn{17sw5tw5ou5qu5rs}fo{17sw5tw5ou5qu5rs}fp{17sw5tw5ou5qu5rs}fq{17sw5tw5ou5qu5rs}7r{cktcltcmtcntcotcpt4lt5os}fs{17sw5tw5ou5qu5rs}ft{17su5tu5ou5qu}7m{5os}fv{17su5tu5ou5qu}fw{17su5tu5ou5qu}fz{cksclscmscnscoscps4ls}}}"),
            Helvetica: e("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}"),
            "Helvetica-BoldOblique": e("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
            "Courier-Bold": e("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
            "Times-Italic": e("{'widths'{k3n2q4ycx2l201n3m201o5t201s2l201t2l201u2l201w3r201x3r201y3r2k1t2l2l202m2n2n3m2o3m2p5n202q5t2r1p2s2l2t2l2u3m2v4n2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w4n3x4n3y4n3z3m4k5w4l3x4m3x4n4m4o4s4p3x4q3x4r4s4s4s4t2l4u2w4v4m4w3r4x5n4y4m4z4s5k3x5l4s5m3x5n3m5o3r5p4s5q3x5r5n5s3x5t3r5u3r5v2r5w1w5x2r5y2u5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q1w6r3m6s3m6t1w6u1w6v2w6w1w6x4s6y3m6z3m7k3m7l3m7m2r7n2r7o1w7p3m7q2w7r4m7s2w7t2w7u2r7v2s7w1v7x2s7y3q202l3mcl3xal2ram3man3mao3map3mar3mas2lat4wau1vav3maw4nay4waz2lbk2sbl4n'fof'6obo2lbp3mbq3obr1tbs2lbu1zbv3mbz3mck3x202k3mcm3xcn3xco3xcp3xcq5tcr4mcs3xct3xcu3xcv3xcw2l2m2ucy2lcz2ldl4mdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr4nfs3mft3mfu3mfv3mfw3mfz2w203k6o212m6m2dw2l2cq2l3t3m3u2l17s3r19m3m}'kerning'{cl{5kt4qw}201s{201sw}201t{201tw2wy2yy6q-t}201x{2wy2yy}2k{201tw}2w{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}2x{17ss5ts5os}2y{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}'fof'-6o6t{17ss5ts5qs}7t{5os}3v{5qs}7p{17su5tu5qs}ck{5kt4qw}4l{5kt4qw}cm{5kt4qw}cn{5kt4qw}co{5kt4qw}cp{5kt4qw}6l{4qs5ks5ou5qw5ru17su5tu}17s{2ks}5q{ckvclvcmvcnvcovcpv4lv}5r{ckuclucmucnucoucpu4lu}5t{2ks}6p{4qs5ks5ou5qw5ru17su5tu}ek{4qs5ks5ou5qw5ru17su5tu}el{4qs5ks5ou5qw5ru17su5tu}em{4qs5ks5ou5qw5ru17su5tu}en{4qs5ks5ou5qw5ru17su5tu}eo{4qs5ks5ou5qw5ru17su5tu}ep{4qs5ks5ou5qw5ru17su5tu}es{5ks5qs4qs}et{4qs5ks5ou5qw5ru17su5tu}eu{4qs5ks5qw5ru17su5tu}ev{5ks5qs4qs}ex{17ss5ts5qs}6z{4qv5ks5ou5qw5ru17su5tu}fm{4qv5ks5ou5qw5ru17su5tu}fn{4qv5ks5ou5qw5ru17su5tu}fo{4qv5ks5ou5qw5ru17su5tu}fp{4qv5ks5ou5qw5ru17su5tu}fq{4qv5ks5ou5qw5ru17su5tu}7r{5os}fs{4qv5ks5ou5qw5ru17su5tu}ft{17su5tu5qs}fu{17su5tu5qs}fv{17su5tu5qs}fw{17su5tu5qs}}}"),
            "Times-Roman": e("{'widths'{k3n2q4ycx2l201n3m201o6o201s2l201t2l201u2l201w2w201x2w201y2w2k1t2l2l202m2n2n3m2o3m2p5n202q6o2r1m2s2l2t2l2u3m2v3s2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v1w3w3s3x3s3y3s3z2w4k5w4l4s4m4m4n4m4o4s4p3x4q3r4r4s4s4s4t2l4u2r4v4s4w3x4x5t4y4s4z4s5k3r5l4s5m4m5n3r5o3x5p4s5q4s5r5y5s4s5t4s5u3x5v2l5w1w5x2l5y2z5z3m6k2l6l2w6m3m6n2w6o3m6p2w6q2l6r3m6s3m6t1w6u1w6v3m6w1w6x4y6y3m6z3m7k3m7l3m7m2l7n2r7o1w7p3m7q3m7r4s7s3m7t3m7u2w7v3k7w1o7x3k7y3q202l3mcl4sal2lam3man3mao3map3mar3mas2lat4wau1vav3maw3say4waz2lbk2sbl3s'fof'6obo2lbp3mbq2xbr1tbs2lbu1zbv3mbz2wck4s202k3mcm4scn4sco4scp4scq5tcr4mcs3xct3xcu3xcv3xcw2l2m2tcy2lcz2ldl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek2wel2wem2wen2weo2wep2weq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr3sfs3mft3mfu3mfv3mfw3mfz3m203k6o212m6m2dw2l2cq2l3t3m3u1w17s4s19m3m}'kerning'{cl{4qs5ku17sw5ou5qy5rw201ss5tw201ws}201s{201ss}201t{ckw4lwcmwcnwcowcpwclw4wu201ts}2k{201ts}2w{4qs5kw5os5qx5ru17sx5tx}2x{17sw5tw5ou5qu}2y{4qs5kw5os5qx5ru17sx5tx}'fof'-6o7t{ckuclucmucnucoucpu4lu5os5rs}3u{17su5tu5qs}3v{17su5tu5qs}7p{17sw5tw5qs}ck{4qs5ku17sw5ou5qy5rw201ss5tw201ws}4l{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cm{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cn{4qs5ku17sw5ou5qy5rw201ss5tw201ws}co{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cp{4qs5ku17sw5ou5qy5rw201ss5tw201ws}6l{17su5tu5os5qw5rs}17s{2ktclvcmvcnvcovcpv4lv4wuckv}5o{ckwclwcmwcnwcowcpw4lw4wu}5q{ckyclycmycnycoycpy4ly4wu5ms}5r{cktcltcmtcntcotcpt4lt4ws}5t{2ktclvcmvcnvcovcpv4lv4wuckv}7q{cksclscmscnscoscps4ls}6p{17su5tu5qw5rs}ek{5qs5rs}el{17su5tu5os5qw5rs}em{17su5tu5os5qs5rs}en{17su5qs5rs}eo{5qs5rs}ep{17su5tu5os5qw5rs}es{5qs}et{17su5tu5qw5rs}eu{17su5tu5qs5rs}ev{5qs}6z{17sv5tv5os5qx5rs}fm{5os5qt5rs}fn{17sv5tv5os5qx5rs}fo{17sv5tv5os5qx5rs}fp{5os5qt5rs}fq{5os5qt5rs}7r{ckuclucmucnucoucpu4lu5os}fs{17sv5tv5os5qx5rs}ft{17ss5ts5qs}fu{17sw5tw5qs}fv{17sw5tw5qs}fw{17ss5ts5qs}fz{ckuclucmucnucoucpu4lu5os5rs}}}"),
            "Helvetica-Oblique": e("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}")
        }
    };
    t.events.push(["addFonts", function (t) {
        var e, n, o, i;
        for (n in t.fonts)t.fonts.hasOwnProperty(n) && (e = t.fonts[n], (o = s.Unicode[e.PostScriptName]) && (i = e.metadata.Unicode ? e.metadata.Unicode : e.metadata.Unicode = {}, i.widths = o.widths, i.kerning = o.kerning), (o = r.Unicode[e.PostScriptName]) && (i = e.metadata.Unicode ? e.metadata.Unicode : e.metadata.Unicode = {}, i.encoding = o, o.codePages && o.codePages.length && (e.encoding = o.codePages[0])))
    }])
}(jsPDF.API);
var BlobBuilder = BlobBuilder || self.WebKitBlobBuilder || self.MozBlobBuilder || self.MSBlobBuilder || function (t) {
        var e = function (t) {
            return Object.prototype.toString.call(t).match(/^\[object\s(.*)\]$/)[1]
        }, n = function () {
            this.data = []
        }, r = function (t, e, n) {
            this.data = t, this.size = t.length, this.type = e, this.encoding = n
        }, s = n.prototype, o = r.prototype, i = t.FileReaderSync, a = function (t) {
            this.code = this[this.name = t]
        }, u = "NOT_FOUND_ERR SECURITY_ERR ABORT_ERR NOT_READABLE_ERR ENCODING_ERR NO_MODIFICATION_ALLOWED_ERR INVALID_STATE_ERR SYNTAX_ERR".split(" "), l = u.length, c = t.URL || t.webkitURL || t, f = c.createObjectURL, d = c.revokeObjectURL, m = c, w = t.btoa, p = t.atob, h = !1, y = function (t) {
            h = !t
        }, _ = t.ArrayBuffer, v = t.Uint8Array;
        for (n.fake = o.fake = !0; l--;)a.prototype[u[l]] = l + 1;
        try {
            v && y.apply(0, new v(1))
        } catch (g) {
        }
        return c.createObjectURL || (m = t.URL = {}), m.createObjectURL = function (t) {
            var e = t.type;
            return null === e && (e = "application/octet-stream"), t instanceof r ? (e = "data:" + e, "base64" === t.encoding ? e + ";base64," + t.data : "URI" === t.encoding ? e + "," + decodeURIComponent(t.data) : w ? e + ";base64," + w(t.data) : e + "," + encodeURIComponent(t.data)) : f ? f.call(c, t) : void 0
        }, m.revokeObjectURL = function (t) {
            "data:" !== t.substring(0, 5) && d && d.call(c, t)
        }, s.append = function (t) {
            var n = this.data;
            if (v && t instanceof _)if (h)n.push(String.fromCharCode.apply(String, new v(t))); else {
                n = "", t = new v(t);
                for (var s = 0, o = t.length; o > s; s++)n += String.fromCharCode(t[s])
            } else if ("Blob" === e(t) || "File" === e(t)) {
                if (!i)throw new a("NOT_READABLE_ERR");
                s = new i, n.push(s.readAsBinaryString(t))
            } else t instanceof r ? "base64" === t.encoding && p ? n.push(p(t.data)) : "URI" === t.encoding ? n.push(decodeURIComponent(t.data)) : "raw" === t.encoding && n.push(t.data) : ("string" != typeof t && (t += ""), n.push(unescape(encodeURIComponent(t))))
        }, s.getBlob = function (t) {
            return arguments.length || (t = null), new r(this.data.join(""), t, "raw")
        }, s.toString = function () {
            return "[object BlobBuilder]"
        }, o.slice = function (t, e, n) {
            var s = arguments.length;
            return 3 > s && (n = null), new r(this.data.slice(t, s > 1 ? e : this.data.length), n, this.encoding)
        }, o.toString = function () {
            return "[object Blob]"
        }, n
    }(self), saveAs = saveAs || navigator.msSaveBlob && navigator.msSaveBlob.bind(navigator) || function (t) {
        var e = t.document, n = t.URL || t.webkitURL || t, r = e.createElementNS("http://www.w3.org/1999/xhtml", "a"), s = "download"in r, o = t.webkitRequestFileSystem, i = t.requestFileSystem || o || t.mozRequestFileSystem, a = function (e) {
            (t.setImmediate || t.setTimeout)(function () {
                throw e
            }, 0)
        }, u = 0, l = [], c = function (t, e, n) {
            e = [].concat(e);
            for (var r = e.length; r--;) {
                var s = t["on" + e[r]];
                if ("function" == typeof s)try {
                    s.call(t, n || t)
                } catch (o) {
                    a(o)
                }
            }
        }, f = function (n, a) {
            var f, d, m, w = this, p = n.type, h = !1, y = function () {
                var e = (t.URL || t.webkitURL || t).createObjectURL(n);
                return l.push(e), e
            }, _ = function () {
                c(w, ["writestart", "progress", "write", "writeend"])
            }, v = function () {
                (h || !f) && (f = y(n)), d && (d.location.href = f), w.readyState = w.DONE, _()
            }, g = function (t) {
                return function () {
                    return w.readyState !== w.DONE ? t.apply(this, arguments) : void 0
                }
            }, q = {create: !0, exclusive: !1};
            return w.readyState = w.INIT, a || (a = "download"), s && (f = y(n), r.href = f, r.download = a, m = e.createEvent("MouseEvents"), m.initMouseEvent("click", !0, !1, t, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), r.dispatchEvent(m)) ? (w.readyState = w.DONE, void _()) : (t.chrome && p && "application/octet-stream" !== p && (m = n.slice || n.webkitSlice, n = m.call(n, 0, n.size, "application/octet-stream"), h = !0), o && "download" !== a && (a += ".download"), d = "application/octet-stream" === p || o ? t : t.open(), void(i ? (u += n.size, i(t.TEMPORARY, u, g(function (t) {
                t.root.getDirectory("saved", q, g(function (t) {
                    var e = function () {
                        t.getFile(a, q, g(function (t) {
                            t.createWriter(g(function (e) {
                                e.onwriteend = function (e) {
                                    d.location.href = t.toURL(), l.push(t), w.readyState = w.DONE, c(w, "writeend", e)
                                }, e.onerror = function () {
                                    var t = e.error;
                                    t.code !== t.ABORT_ERR && v()
                                }, ["writestart", "progress", "write", "abort"].forEach(function (t) {
                                    e["on" + t] = w["on" + t]
                                }), e.write(n), w.abort = function () {
                                    e.abort(), w.readyState = w.DONE
                                }, w.readyState = w.WRITING
                            }), v)
                        }), v)
                    };
                    t.getFile(a, {create: !1}, g(function (t) {
                        t.remove(), e()
                    }), g(function (t) {
                        t.code === t.NOT_FOUND_ERR ? e() : v()
                    }))
                }), v)
            }), v)) : v()))
        }, d = f.prototype;
        return d.abort = function () {
            this.readyState = this.DONE, c(this, "abort")
        }, d.readyState = d.INIT = 0, d.WRITING = 1, d.DONE = 2, d.error = d.onwritestart = d.onprogress = d.onwrite = d.onabort = d.onerror = d.onwriteend = null, t.addEventListener("unload", function () {
            for (var t = l.length; t--;) {
                var e = l[t];
                "string" == typeof e ? n.revokeObjectURL(e) : e.remove()
            }
            l.length = 0
        }, !1), function (t, e) {
            return new f(t, e)
        }
    }(self), MAX_BITS = 15, D_CODES = 30, BL_CODES = 19, LENGTH_CODES = 29, LITERALS = 256, L_CODES = LITERALS + 1 + LENGTH_CODES, HEAP_SIZE = 2 * L_CODES + 1, END_BLOCK = 256, MAX_BL_BITS = 7, REP_3_6 = 16, REPZ_3_10 = 17, REPZ_11_138 = 18, Buf_size = 16, Z_DEFAULT_COMPRESSION = -1, Z_FILTERED = 1, Z_HUFFMAN_ONLY = 2, Z_DEFAULT_STRATEGY = 0, Z_NO_FLUSH = 0, Z_PARTIAL_FLUSH = 1, Z_FULL_FLUSH = 3, Z_FINISH = 4, Z_OK = 0, Z_STREAM_END = 1, Z_NEED_DICT = 2, Z_STREAM_ERROR = -2, Z_DATA_ERROR = -3, Z_BUF_ERROR = -5, _dist_code = [0, 1, 2, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 0, 0, 16, 17, 18, 18, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29];
Tree._length_code = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28], Tree.base_length = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 0], Tree.base_dist = [0, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144, 8192, 12288, 16384, 24576], Tree.d_code = function (t) {
    return 256 > t ? _dist_code[t] : _dist_code[256 + (t >>> 7)]
}, Tree.extra_lbits = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0], Tree.extra_dbits = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13], Tree.extra_blbits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7], Tree.bl_order = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15], StaticTree.static_ltree = [12, 8, 140, 8, 76, 8, 204, 8, 44, 8, 172, 8, 108, 8, 236, 8, 28, 8, 156, 8, 92, 8, 220, 8, 60, 8, 188, 8, 124, 8, 252, 8, 2, 8, 130, 8, 66, 8, 194, 8, 34, 8, 162, 8, 98, 8, 226, 8, 18, 8, 146, 8, 82, 8, 210, 8, 50, 8, 178, 8, 114, 8, 242, 8, 10, 8, 138, 8, 74, 8, 202, 8, 42, 8, 170, 8, 106, 8, 234, 8, 26, 8, 154, 8, 90, 8, 218, 8, 58, 8, 186, 8, 122, 8, 250, 8, 6, 8, 134, 8, 70, 8, 198, 8, 38, 8, 166, 8, 102, 8, 230, 8, 22, 8, 150, 8, 86, 8, 214, 8, 54, 8, 182, 8, 118, 8, 246, 8, 14, 8, 142, 8, 78, 8, 206, 8, 46, 8, 174, 8, 110, 8, 238, 8, 30, 8, 158, 8, 94, 8, 222, 8, 62, 8, 190, 8, 126, 8, 254, 8, 1, 8, 129, 8, 65, 8, 193, 8, 33, 8, 161, 8, 97, 8, 225, 8, 17, 8, 145, 8, 81, 8, 209, 8, 49, 8, 177, 8, 113, 8, 241, 8, 9, 8, 137, 8, 73, 8, 201, 8, 41, 8, 169, 8, 105, 8, 233, 8, 25, 8, 153, 8, 89, 8, 217, 8, 57, 8, 185, 8, 121, 8, 249, 8, 5, 8, 133, 8, 69, 8, 197, 8, 37, 8, 165, 8, 101, 8, 229, 8, 21, 8, 149, 8, 85, 8, 213, 8, 53, 8, 181, 8, 117, 8, 245, 8, 13, 8, 141, 8, 77, 8, 205, 8, 45, 8, 173, 8, 109, 8, 237, 8, 29, 8, 157, 8, 93, 8, 221, 8, 61, 8, 189, 8, 125, 8, 253, 8, 19, 9, 275, 9, 147, 9, 403, 9, 83, 9, 339, 9, 211, 9, 467, 9, 51, 9, 307, 9, 179, 9, 435, 9, 115, 9, 371, 9, 243, 9, 499, 9, 11, 9, 267, 9, 139, 9, 395, 9, 75, 9, 331, 9, 203, 9, 459, 9, 43, 9, 299, 9, 171, 9, 427, 9, 107, 9, 363, 9, 235, 9, 491, 9, 27, 9, 283, 9, 155, 9, 411, 9, 91, 9, 347, 9, 219, 9, 475, 9, 59, 9, 315, 9, 187, 9, 443, 9, 123, 9, 379, 9, 251, 9, 507, 9, 7, 9, 263, 9, 135, 9, 391, 9, 71, 9, 327, 9, 199, 9, 455, 9, 39, 9, 295, 9, 167, 9, 423, 9, 103, 9, 359, 9, 231, 9, 487, 9, 23, 9, 279, 9, 151, 9, 407, 9, 87, 9, 343, 9, 215, 9, 471, 9, 55, 9, 311, 9, 183, 9, 439, 9, 119, 9, 375, 9, 247, 9, 503, 9, 15, 9, 271, 9, 143, 9, 399, 9, 79, 9, 335, 9, 207, 9, 463, 9, 47, 9, 303, 9, 175, 9, 431, 9, 111, 9, 367, 9, 239, 9, 495, 9, 31, 9, 287, 9, 159, 9, 415, 9, 95, 9, 351, 9, 223, 9, 479, 9, 63, 9, 319, 9, 191, 9, 447, 9, 127, 9, 383, 9, 255, 9, 511, 9, 0, 7, 64, 7, 32, 7, 96, 7, 16, 7, 80, 7, 48, 7, 112, 7, 8, 7, 72, 7, 40, 7, 104, 7, 24, 7, 88, 7, 56, 7, 120, 7, 4, 7, 68, 7, 36, 7, 100, 7, 20, 7, 84, 7, 52, 7, 116, 7, 3, 8, 131, 8, 67, 8, 195, 8, 35, 8, 163, 8, 99, 8, 227, 8], StaticTree.static_dtree = [0, 5, 16, 5, 8, 5, 24, 5, 4, 5, 20, 5, 12, 5, 28, 5, 2, 5, 18, 5, 10, 5, 26, 5, 6, 5, 22, 5, 14, 5, 30, 5, 1, 5, 17, 5, 9, 5, 25, 5, 5, 5, 21, 5, 13, 5, 29, 5, 3, 5, 19, 5, 11, 5, 27, 5, 7, 5, 23, 5], StaticTree.static_l_desc = new StaticTree(StaticTree.static_ltree, Tree.extra_lbits, LITERALS + 1, L_CODES, MAX_BITS), StaticTree.static_d_desc = new StaticTree(StaticTree.static_dtree, Tree.extra_dbits, 0, D_CODES, MAX_BITS), StaticTree.static_bl_desc = new StaticTree(null, Tree.extra_blbits, 0, BL_CODES, MAX_BL_BITS);
var MAX_MEM_LEVEL = 9, DEF_MEM_LEVEL = 8, STORED = 0, FAST = 1, SLOW = 2, config_table = [new Config(0, 0, 0, 0, STORED), new Config(4, 4, 8, 4, FAST), new Config(4, 5, 16, 8, FAST), new Config(4, 6, 32, 32, FAST), new Config(4, 4, 16, 16, SLOW), new Config(8, 16, 32, 32, SLOW), new Config(8, 16, 128, 128, SLOW), new Config(8, 32, 128, 256, SLOW), new Config(32, 128, 258, 1024, SLOW), new Config(32, 258, 258, 4096, SLOW)], z_errmsg = "need dictionary;stream end;;;stream error;data error;;buffer error;;".split(";"), NeedMore = 0, BlockDone = 1, FinishStarted = 2, FinishDone = 3, PRESET_DICT = 32, INIT_STATE = 42, BUSY_STATE = 113, FINISH_STATE = 666, Z_DEFLATED = 8, STORED_BLOCK = 0, STATIC_TREES = 1, DYN_TREES = 2, MIN_MATCH = 3, MAX_MATCH = 258, MIN_LOOKAHEAD = MAX_MATCH + MIN_MATCH + 1;
ZStream.prototype = {
    deflateInit: function (t, e) {
        return this.dstate = new Deflate, e || (e = MAX_BITS), this.dstate.deflateInit(this, t, e)
    }, deflate: function (t) {
        return this.dstate ? this.dstate.deflate(this, t) : Z_STREAM_ERROR
    }, deflateEnd: function () {
        if (!this.dstate)return Z_STREAM_ERROR;
        var t = this.dstate.deflateEnd();
        return this.dstate = null, t
    }, deflateParams: function (t, e) {
        return this.dstate ? this.dstate.deflateParams(this, t, e) : Z_STREAM_ERROR
    }, deflateSetDictionary: function (t, e) {
        return this.dstate ? this.dstate.deflateSetDictionary(this, t, e) : Z_STREAM_ERROR
    }, read_buf: function (t, e, n) {
        var r = this.avail_in;
        return r > n && (r = n), 0 === r ? 0 : (this.avail_in -= r, t.set(this.next_in.subarray(this.next_in_index, this.next_in_index + r), e), this.next_in_index += r, this.total_in += r, r)
    }, flush_pending: function () {
        var t = this.dstate.pending;
        t > this.avail_out && (t = this.avail_out), 0 !== t && (this.next_out.set(this.dstate.pending_buf.subarray(this.dstate.pending_out, this.dstate.pending_out + t), this.next_out_index), this.next_out_index += t, this.dstate.pending_out += t, this.total_out += t, this.avail_out -= t, this.dstate.pending -= t, 0 === this.dstate.pending && (this.dstate.pending_out = 0))
    }
}, void function (t, e) {
    "object" == typeof module ? module.exports = e() : "function" == typeof define ? define(e) : t.adler32cs = e()
}(this, function () {
    var t, e = "function" == typeof ArrayBuffer && "function" == typeof Uint8Array, n = null;
    if (e) {
        try {
            var r = require("buffer");
            "function" == typeof r.Buffer && (n = r.Buffer)
        } catch (s) {
        }
        t = function (t) {
            return t instanceof ArrayBuffer || null !== n && t instanceof n
        }
    } else t = function () {
        return !1
    };
    var o;
    o = null !== n ? function (t) {
        return new n(t, "utf8").toString("binary")
    } : function (t) {
        return unescape(encodeURIComponent(t))
    };
    var i = function (t, e) {
        for (var n = 65535 & t, r = t >>> 16, s = 0, o = e.length; o > s; s++)n = (n + (255 & e.charCodeAt(s))) % 65521, r = (r + n) % 65521;
        return (r << 16 | n) >>> 0
    }, a = function (t, e) {
        for (var n = 65535 & t, r = t >>> 16, s = 0, o = e.length; o > s; s++)n = (n + e[s]) % 65521, r = (r + n) % 65521;
        return (r << 16 | n) >>> 0
    }, r = {}, u = function (t) {
        if (!(this instanceof u))throw new TypeError("Constructor cannot called be as a function.");
        if (!isFinite(t = null == t ? 1 : +t))throw Error("First arguments needs to be a finite number.");
        this.checksum = t >>> 0
    }, l = u.prototype = {};
    l.constructor = u;
    var c = function (t) {
        if (!(this instanceof u))throw new TypeError("Constructor cannot called be as a function.");
        if (null == t)throw Error("First argument needs to be a string.");
        this.checksum = i(1, t.toString())
    };
    c.prototype = l, u.from = c, c = function (t) {
        if (!(this instanceof u))throw new TypeError("Constructor cannot called be as a function.");
        if (null == t)throw Error("First argument needs to be a string.");
        t = o(t.toString()), this.checksum = i(1, t)
    }, c.prototype = l, u.fromUtf8 = c, e && (c = function (e) {
        if (!(this instanceof u))throw new TypeError("Constructor cannot called be as a function.");
        if (!t(e))throw Error("First argument needs to be ArrayBuffer.");
        return e = new Uint8Array(e), this.checksum = a(1, e)
    }, c.prototype = l, u.fromBuffer = c), l.update = function (t) {
        if (null == t)throw Error("First argument needs to be a string.");
        return t = t.toString(), this.checksum = i(this.checksum, t)
    }, l.updateUtf8 = function (t) {
        if (null == t)throw Error("First argument needs to be a string.");
        return t = o(t.toString()), this.checksum = i(this.checksum, t)
    }, e && (l.updateBuffer = function (e) {
        if (!t(e))throw Error("First argument needs to be ArrayBuffer.");
        return e = new Uint8Array(e), this.checksum = a(this.checksum, e)
    }), l.clone = function () {
        return new f(this.checksum)
    };
    var f = r.Adler32 = u;
    return r.from = function (t) {
        if (null == t)throw Error("First argument needs to be a string.");
        return i(1, t.toString())
    }, r.fromUtf8 = function (t) {
        if (null == t)throw Error("First argument needs to be a string.");
        return t = o(t.toString()), i(1, t)
    }, e && (r.fromBuffer = function (e) {
        if (!t(e))throw Error("First argument need to be ArrayBuffer.");
        return e = new Uint8Array(e), a(1, e)
    }), r
});
var selectionDate = function (t, e, n, r, s) {
    function o(t, e) {
        var n = i("0", e);
        return n.id = t, n.disabled = !0, n.selected = !0, n
    }

    function i(t, e) {
        var n = document.createElement("option");
        return n.value = t, n.textContent = e, n
    }

    function a(t) {
        return new Error("[selectionDate] " + t)
    }

    var u, l, c, f, d;
    if (u = document.getElementById("sdate-day"), l = document.getElementById("sdate-month"), c = document.getElementById("sdate-year"), n = n || "Day", r = r || "Month", s = s || "Year", d = (new Date).getFullYear(), !t)throw a('Argument "onChange" not found!');
    if ("function" != typeof t)throw a('Argument "onChange" not a function!');
    if (!e)throw a('Argument "months" not found!');
    if ("object" != typeof e && e.hasOwnProperty("length"))throw a('Argument "months" not an array!');
    if (12 !== e.length)throw a('Length of argument "months" not equals 12');
    if (!u)throw a('Page not contains the element with id="sdate-day"');
    if (!l)throw a('Page not contains the element with id="sdate-month"');
    if (!c)throw a('Page not contains the element with id="sdate-year"');
    for (u.innerHTML = l.innerHTML = c.innerHTML = "", u.appendChild(o("dp_day", n)), l.appendChild(o("dp_month", r)), c.appendChild(o("dp_year", s)), f = 1; 31 >= f; ++f)u.appendChild(i(f, f));
    for (f = 0; 12 > f; ++f)l.appendChild(i(f + 1, e[f]));
    for (f = d; f > 1930; --f)c.appendChild(i(f, f));
    return u.onchange = l.onchange = c.onchange = function (e) {
        u.value > 0 && l.value > 0 && c.value > 0 && t(new Date(c.value, l.value, u.value))
    }, {
        setDate: function (e) {
            u.selectedIndex = e.getDate(), l.selectedIndex = e.getMonth() + 1, c.selectedIndex = d - e.getFullYear() + 1, t(e)
        }
    }
};
!function () {
    "use strict";
    function t() {
        var t, n, r, s = [];
        for (t = 0; _ > t; t++) {
            for (r = [], n = 1; y >= n; n++) {
                var o = e(t, n);
                r.push(o)
            }
            s.push(r)
        }
        return s
    }

    function e(t, e) {
        var r = t * y + e, s = n(F, 7 * r);
        return {isPast: R - s > 0}
    }

    function n(t, e) {
        var n = new Date(t.getTime());
        return n.setDate(t.getDate() + e), n
    }

    function r() {
        s(), o(), i(), a()
    }

    function s() {
        I.clearRect(0, 0, v, g), I.fillStyle = q, I.fillRect(0, 0, v, g), I.fillStyle = b
    }

    function o() {
        I.textAlign = "center", I.fillText(T, C.width / 2, 20)
    }

    function i(t, e) {
        I.textAlign = "left", I.save(), I.translate(20, h + 40), I.rotate(-Math.PI / 2), I.translate(-20, -(h + 40)), I.fillText(E, 20, h + 40), I.restore(), I.fillText(A, p, h - 35)
    }

    function a() {
        I.textAlign = "right";
        for (var t = 0; _ > t; t++)t % 5 === 0 && I.fillText(t, p - 5, h + 10 + t * d);
        I.textAlign = "left";
        for (var t = 1; y > t; t++)(t % 5 === 0 || 1 === t) && I.fillText(t, p + (t - 1) * d, h - 10)
    }

    function u(t, e, n) {
        for (var r = 0; t > r; r++)l(r, e, n[r])
    }

    function l(t, e, n) {
        for (var r = 0; e > r; r++)I.fillStyle = n[r].isPast ? S : k, I.strokeStyle = x, c(p + r * d, h + t * d)
    }

    function c(t, e) {
        I.fillRect(f + t, f + e, w, w), I.strokeRect(f + t, f + e, w, w)
    }

    var f = .5, d = 13, m = 3, w = d - m, p = 40, h = 70, y = 52, _ = 90, v = 725, g = 1250, q = "#FFF", x = "#000", b = "#000", k = "#FFF", S = "#000", T = "LIFE CALENDAR", E = "← Age", A = "Week of the Year →", C = document.createElement("canvas");
    C.width = v, C.height = g;
    var I = C.getContext("2d");
    I.strokeStyle = x, I.fillStyle = b, I.font = "13px sans-serif";
    var R = new Date, F = null;
    window.LC = {
        init: function (t, e) {
            t.appendChild(C), e ? this.changeLang(e) : (r(), this.update(null))
        }, update: function (e) {
            F = e || new Date, u(_, y, t())
        }, changeTheme: function (e) {
            x = e.box.borderColor, k = e.box.backgroundDefaultColor, S = e.box.backgroundPastDayColor, u(_, y, t())
        }, changeLang: function (t) {
            T = t.title, E = t.left_text, A = t.top_text, r(), this.update(F)
        }
    }
}(), function () {
    function t() {
        return a.querySelector("canvas").toDataURL(s)
    }

    function e(t) {
        for (var e in t)if (t.hasOwnProperty(e)) {
            var n = document.getElementById(e);
            null != n ? n.textContent = t[e] : console.error("[updateLang] " + e)
        }
    }

    var n, r = "my-life-calendar", s = "image/jpeg", o = document.getElementById("theme"), i = document.getElementById("lang"), a = document.getElementById("calendar-canvas"), u = document.getElementById("lang_save_jpg"), l = document.getElementById("lang_save_pdf"), c = document.getElementById("lang_print"), f = [{
        box: {
            borderColor: "#000",
            backgroundDefaultColor: "#FFF",
            backgroundPastDayColor: "#000"
        }
    }, {
        box: {
            borderColor: "#CCC",
            backgroundDefaultColor: "#FFF",
            backgroundPastDayColor: "#87ccc9"
        }
    }, {
        box: {
            borderColor: "#CCC",
            backgroundDefaultColor: "#FFF",
            backgroundPastDayColor: "#a52126"
        }
    }], d = [{
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        settings: {
            lang_set_birthday: "Дата вашего рождения:",
            dp_day: "День",
            dp_month: "Месяц",
            dp_year: "Год",
            lang_set_theme: "Дизайн календаря:",
            lang_select_lang: "Язык:",
            lang_save_print: "Сохранение или печать:",
            lang_save_jpg: "JPG",
            lang_save_pdf: "PDF",
            lang_print: "Печать"
        },
        calendar: {title: "КАЛЕНДАРЬ ЖИЗНИ", left_text: "← Возраст", top_text: "Недели года →"}
    }, {
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        settings: {
            lang_set_birthday: "Set your birthday:",
            dp_day: "Day",
            dp_month: "Month",
            dp_year: "Year",
            lang_set_theme: "Select theme:",
            lang_select_lang: "Select language:",
            lang_save_print: "Save or print:",
            lang_save_jpg: "JPG",
            lang_save_pdf: "PDF",
            lang_print: "Print"
        },
        calendar: {title: "LIFE CALENDAR", left_text: "← Age", top_text: "Week of the Year →"}
    }];
    n = selectionDate(LC.update, d[0].months), e(d[0].settings), LC.init(a, d[0].calendar), o.onchange = function (t) {
        var e = t.target.value || 0, n = f[e];
        LC.changeTheme(n)
    }, i.onchange = function (t) {
        var r = t.target.value || 0, s = d[r];
        n = selectionDate(LC.update, s.months), e(s.settings), LC.changeLang(s.calendar)
    }, u.onclick = function (e) {
        var n = t();
        n = n.replace(s, "image/octet-stream"), e.target.href = n, e.target.download = r + ".jpg"
    }, l.onclick = function (e) {
        e.preventDefault();
        var n = t(), s = new jsPDF;
        s.addImage(n, "JPEG", 20, 5, 170, 280), s.save(r + ".pdf")
    }, c.onclick = function (t) {
        t.preventDefault(), window.print()
    }
}();