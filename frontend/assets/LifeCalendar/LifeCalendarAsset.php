<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets\LifeCalendar;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class LifeCalendarAsset extends AssetBundle
{

    public $js = [
        'life-calendar.js'
    ];

    public $css = [
        'life-calendar.css'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';

        $this->depends = [
            JqueryAsset::className()
        ];
        return parent::init();
    }

}
