<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.05.15
 * Time: 18:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class StickyKitAsset extends AssetBundle{

    public $sourcePath = '@bower/sticky-kit';
    public $js = [
        'jquery.sticky-kit.min.js'
    ];
//    public $depends = [
//       'yii\web\JqueryAsset',
//    ];

}
