<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.07.15
 * Time: 15:08
 */

namespace frontend\assets;

class JsCookieAssets extends \yii\web\AssetBundle{

    public $sourcePath = '@bower/js-cookie/src';
    public $js = [
        'js.cookie.js'
    ];

}