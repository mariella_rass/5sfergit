<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.06.15
 * Time: 11:41
 */
use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;


$form = ActiveForm::begin(
    [
        'type' => ActiveForm::TYPE_VERTICAL,
        'enableClientValidation' => true,
        'enableClientScript' => true,
        'options' => [
            'data-pjax' => 1
        ]
//            'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
    ]
);

$rows = [
    [
        'columns' => 1,
        'autoGenerateColumns' => false,
        'attributes' => [
            'name' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'Твое имя']],
            'email' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'Твой email']],
        ]
    ]

];

echo FormGrid::widget(
    [
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => $rows
    ]
);

echo Html::submitButton(
    'Подписаться',
    ['class' => 'btn-u btn-u-blue btn-u-lg rounded']
);

ActiveForm::end();