<time class="cbp_tmtime">
    <span></span>
    <span>
        <div><?= \common\components\DateFormater::format($model->eventDate) ?>
        </div>
        <div><?= strftime(
                "%G",
                $model->eventDate
            ) ?>
        </div>
    </span>
</time>

<i class="cbp_tmicon rounded-x hidden-xs"></i>

<div class="cbp_tmlabel">

    <div class="row">
        <div class="col-md-12">
            <h3><a href="<?= $model->eventUrl ?>"><?= $model->eventName ?></a></h3>

            <?= $this->render(
                '_author_thumb',
                [
                    'model' => $model
                ]
            ) ?>
        </div>
    </div>
</div>