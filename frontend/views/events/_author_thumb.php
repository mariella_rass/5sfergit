<div class="row margin-top-20">
    <div class="col-xs-3">
        <a href="<?= \yii\helpers\Url::to(['author/index', 'slug' => $model->user->profile->slug], true) ?>">
            <?= $this->render(
                '/author/_ava',
                [
                    'model' => $model->user,
                    'showName' => isset($showName) ? $showName : false,
                    'active' => false,
                ]
            ) ?>
        </a>
    </div>
    <div class="col-xs-9">
        <p><strong><?= $model->user->profile->name ?>. </strong>
            <?php preg_match('/([\p{L}\d]+[^\p{L}\d]*){1,20}/mu', strip_tags($model->user->profile->bio), $bio); ?>
            <?= isset($bio[0]) ? trim($bio[0]) . '...' : $model->user->profile->bio ?>
        </p>
        <a href="<?= $model->eventUrl ?>" class="btn-u btn-u-blue rounded"><i class="fa fa-check-square-o"></i>
            Зарегистрироваться</a>
    </div>
</div>