<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.15
 * Time: 12:45
 *
 * @var $category common\models\Category
 */
?>

<a href="<?= $category->getUrl() ?>" class="btn btn-u-xs cat-label rounded" style="background-color: #<?= $category->getHexColor() ?>!important;"><?= $category->getName() ?></a>
