<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.06.15
 * Time: 17:41
 */

if (!isset($model) || !$model) {
    $model = \frontend\models\Post::find()->status(\frontend\models\Post::PUBLISH)->orderBy(
        ['postPopularity' => SORT_DESC]
    )->one();
}

$posts = \frontend\models\Post::find()->andWhere(
    'postPopularity < ' . $model->postPopularity
)->limit(6)->status(\frontend\models\Post::PUBLISH)->orderBy(['postPopularity' => SORT_DESC])->all();

if (count($posts) != 6) {
    $posts = \frontend\models\Post::find()->limit(6)->orderBy(['postPopularity' => SORT_DESC])->all();
}

?>

<div class="row">
    <div class="heading heading-v4 margin-bottom-10 overflow-hidden">
        <h3 class="struct">Самое интересное</h3>
    </div>
</div>
<?php $c = 0 ?>
<?php foreach ($posts as $post): ?>
    <?php if ($c % 3 == 0): ?>
        <div class="row">
    <?php endif ?>
    <div class="col-sm-4">
        <div class="thumbnails thumbnail-style thumbnail-kenburn">
            <div class="thumbnail-img">
                <div class="overflow-hidden rounded-2x"
                     style="-webkit-mask-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC);">
                    <a class="" href="<?= $post->getUrl() ?>">
                        <?php $path = str_replace( "postImage/", "postImage/thumb-" , $post->getImgSrc('postImage')) ;?>

                        <img class="img-responsive rounded-2x" src="<?= $path; ?>"  alt="top-interesting">
                    </a>
                </div>
                <a class="btn-more" href="<?= $post->getUrl() ?>">читать</a>
            </div>
            <div class="">
                <h4 class="text-center"><a class="" href="<?= $post->getUrl() ?>"><?= $post->postTitle ?></a>
                </h4>
            </div>
        </div>
    </div>
    <?php if ($c % 3 == 2): ?>
        </div>
    <?php endif ?>
    <?php if (false && $c == 2): ?>
        <div class="row">
            <div class="col-xs-12 margin-bottom-40">
                <ins class="adsbygoogle overflow-hidden"
                     style="display:block"
                     data-ad-client="ca-pub-9641652080415765"
                     data-ad-slot="9681759930"
                     data-ad-format="auto"></ins>
                <?php
                $this->registerJsFile(
                    "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js",
                    ['position' => \yii\web\View::POS_HEAD]
                );
                $this->registerJs("(adsbygoogle = window.adsbygoogle || []).push({});");
                ?>
            </div>
        </div>
    <?php endif ?>
    <?php $c++ ?>
<?php endforeach ?>

