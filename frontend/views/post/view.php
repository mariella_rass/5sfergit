<?php
/**
 * @var $this yii\web\View
 * @var $post frontend\models\Post
 */


// Если автор статьи "5 сфер", то не добавляем имя в title
if ($post->user->profile->name == '5 сфер') {
    $this->title = $post->postTitle . ' - 5 сфер';
} else {
    $this->title = $post->postTitle . ' - ' . $post->user->profile->name . ' - 5 сфер';
}
$this->registerMetaTag(
    [
        'name' => 'Description',
        'content' => $post->postLead . '...'
    ]
);

Yii::$app->opengraph->title = $post->postTitle;
Yii::$app->opengraph->site_name = '5 сфер';
Yii::$app->opengraph->description = $post->postLead;
// http://5sfer.bigdig.com.ua/media/post/postImage/scndfc.jpg
Yii::$app->opengraph->image = str_replace( "postImage/", "postImage/thumb-" , $post->getImgSrc('postImage')) ;
//Yii::$app->opengraph->url = 'http://5sfer.com/' . $post->primaryKey;
Yii::$app->opengraph->twitter->card = 'summary';
Yii::$app->opengraph->twitter->site = '5sfer.com';
if ($this->context->getCanonical()) {
    Yii::$app->opengraph->url = $this->context->getCanonical();
}


$this->params['postModel'] = $post;

$this->params['author'] = $post->user->profile->name;
$this->params['categories'] = array_map(
    function ($v) {
        return $v->name;
    },
    $post->categories
);
$this->params['tags'] = array_map(
    function ($v) {
        return $v->name;
    },
    $post->tags
);

// AB testing
//$this->registerJsFile(
//    '//cdn.optimizely.com/js/3371751359.js',
//    [
//        'position' => \yii\web\View::POS_HEAD
//    ]
//);

?>
<div class="row margin-bottom-40" data-sticky_parent>
    <div data-sticky_column class="col-md-2 visible-lg" style="float: left">
        <?= $this->render(
            '/author/_ava',
            [
                'model' => $post->user,
                'showName' => true,
                'class' => 'margin-bottom-40',
                'active' => true
            ]
        ) ?>
        <div class="row margin-bottom-20">
            <div class="col-sm-11 text-center">
                <?= \frontend\widgets\VerticalShare::widget(
                    [
                        'url' => $this->context->getCanonical(),
                        'title' => $post->postTitle,
                        'id' => 'sln'
                    ]
                ) ?>
            </div>
        </div>
        <?php if ($post->user->profile->triggerComment == 'on'): ?>
            <div class="row">
                <div class="col-sm-11 text-center">
                    <div class="margin-bottom-20">
                        <img src="/img/comment2.png" alt="comment2">
                    </div>
                    <a href="#mc-container"><i
                            class="icon-custom icon-lg rounded-2x icon-color-yellow fa fa-comments-o"></i></a>
                    <span style="font-size: 13px">Расскажи нам что полезного ты узнал!</span>
                </div>
            </div>
        <?php endif ?>
    </div>
    <div id="post_content" class="col-md-12 col-lg-10">
        <div class="row">
            <div id="width_ethalon" class="col-xs-12"></div>
        </div>
        <div class="row article">

            <div class="col-xs-12">
                <?php foreach ($post->categories as $category) {
                    echo $this->render('_category', ['category' => $category]);
                };
                ?>
                <h1><?= $post->postTitle ?></h1>
            </div>

            <div class="col-xs-12 margin-bottom-10">
                    <span class="margin-right-10 hidden-lg">
                    <i class="fa fa-user margin-right-5"></i>
                    <a href="<?= $post->user->url ?>">
                        <?= $post->user->profile->name ?>
                    </a>
                    </span>
                    <span id="totalViews" class="margin-right-10"><i
                            class="fa fa-eye margin-right-5"></i><span><?= $post->getFormatedViews() ?></span></span>
                    <span id="totalShare" class="margin-right-10"><i
                            class="fa fa-share-alt margin-right-5"></i><span><?= $post->getFormatedShares(
                            ) ?></span></span>
                    <span id="totalComment" class="margin-right-10"><i
                            class="fa fa-comments-o margin-right-5"></i><span id="commentsCount">0</span></span>
            </div>

            <div class="col-xs-12 margin-bottom-10 hidden-lg">
                <?= \frontend\widgets\HorizontalShare::widget(
                    [
                        'url' => $this->context->getCanonical(),
                        'title' => $post->postTitle
                    ]
                ) ?>
            </div>

            <?php if (!$post->containCodeBlock()): ?>
                <div class="col-xs-12 margin-bottom-20">
                    <?= 

                    $post->getMedia() ;

                    ?>
                </div>
            <?php endif; ?>

            <div class="col-xs-12">
                <?php
                //                $follow = true;
                //                // если хоть одна из активных категорий no-follow, то no-follow
                //                foreach ($post->categories as $category) {
                //                    $follow = $category->categoryStatus == 'on' && $category->categoryRel == 'no-follow' ? false : $follow;
                //                };
                //                // если пост no-follow, то no-follow
                //                if ($post->postRel == 'no-follow') {
                //                    $follow = false;
                //                }
                //                if (!$follow) {
                //                    \frontend\widgets\RelNoFollow::begin();
                //                }
                //                echo $post->postText;
                //                if (!$follow) {
                //                    \frontend\widgets\RelNoFollow::end();
                //                }
                echo \frontend\widgets\SocialLockerWidget::widget(['content' => $post->getPre2Raw()]);
                ?>
            </div>

            <?php if (isset($post->user->profile->postForm) && !empty($post->user->profile->postForm)): ?>
                <div class="col-xs-12 margin-bottom-20">
                    <div class="rounded-2x"
                         style="background-color: rgb(250, 250, 250);padding: 20px;border: dashed 1px #ccc">
                        <div class="row">
                            <div class="col-xs-12 codeform-block col-sm-offset-0 col-sm-12">
                                <?= str_replace(
                                    'class="wf-button"',
                                    'class="btn-u btn-u-blue btn-u-lg rounded margin-top-20"',
                                    $post->user->profile->postForm
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php elseif (isset($post->categories[0]) && !empty($post->categories[0]->categoryAppendHtml)): ?>
                <div class="col-xs-12 margin-bottom-20">
                    <div class="rounded-2x"
                         style="background-color: rgb(250, 250, 250);padding: 20px;border: dashed 1px #ccc">
                        <div class="row">
                            <div class=" col-xs-12 col-sm-offset-0 col-sm-12">
                                <?= str_replace(
                                    'class="wf-button"',
                                    'class="btn-u btn-u-blue btn-u-lg rounded margin-top-20"',
                                    $post->categories[0]->categoryAppendHtml
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="col-xs-12">
                <div class="row margin-bottom-10">
                    <?php if (!empty($post->postSourceUrl)): ?>
                        <div id="tag" class="col-xs-12 col-sm-8 color-blue">
                            <?php if ($post->tags): ?>
                                <i class="fa fa-tags"></i>
                            <?php endif ?>
                            <?php foreach ($post->tags as $tag) {
                                $tags[] = '<a href="' . $tag->url . '">' . $tag->name . '</a>';
                            };
                            echo isset($tags) ? implode(', ', $tags) : '';
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 text-right">
                            <a href="<?= $post->postSourceUrl ?>" class="color-blue" target="_blank" rel="nofollow"><i
                                    class="fa fa-external-link"></i> Источник</a>
                        </div>
                    <?php else: ?>
                        <div id="tag" class="col-xs-12 color-blue">
                            <?php if ($post->tags): ?>
                                <i class="fa fa-tags"></i>
                            <?php endif ?>
                            <?php foreach ($post->tags as $tag) {
                                $tags[] = '<a href="' . $tag->url . '">' . $tag->name . '</a>';
                            };
                            echo isset($tags) ? implode(', ', $tags) : '';
                            ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>

            <div class="col-xs-12 hidden-lg">
                <?= \frontend\widgets\HorizontalShare::widget(
                    [
                        'url' => $this->context->getCanonical(),
                        'title' => $post->postTitle
                    ]
                ) ?>
            </div>
        </div>
    </div>
</div>

<?php
\frontend\assets\StickyKitAsset::register($this);
$this->registerJs(
    '$("#left").stick_in_parent({
        spacer: false
     });
'
);

$this->registerJs(
    "
    (function(){
        var srd = function(data){
            var url = '" . \yii\helpers\Url::to(['post/stat', 'slug' => $post->primaryKey]) . "';
            $.post(url, data);
        };

        $('body').on('complete.social-likes',
            function (event, totalLikes) {
                $('#totalShare>span').html(totalLikes);
                srd({'count': totalLikes, 't':'s'});
            });

        srd({'t':'v'});
    })();
"
);

?>

<?php if ($post->postComment == 'on'): ?>
    <div class="row margin-bottom-40">
        <div class="col-xs-12">
            <div id="mc-container">
                <div id="mc-content">
                    <ul id="cackle-comments">
                    </ul>
                </div>
            </div>
            <script type="text/javascript">
                cackle_widget = window.cackle_widget || [];
                cackle_widget.push({
                    widget: 'Comment',
                    id: '22299',
                    countContainer: 'commentsCount',
                    url: '<?= $this->context->getCanonical() ?>'
                });
                document.getElementById('mc-container').innerHTML = '';
                (function () {
                    var mc = document.createElement('script');
                    mc.type = 'text/javascript';
                    mc.async = true;
                    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(mc, s.nextSibling);
                })();
            </script>
        </div>
    </div>
<?php else: ?>
    <div class="col-xs-12 margin-bottom-20"></div>
<?php endif; ?>

<?php

echo $this->render(
    '_more_posts',
    [
        'model' => $post
    ]
);


// extra data begin
//echo \frontend\widgets\AnalyticsExtraData::widget(
//    [
//        'data' => [
//            'author' => [
//                'id' => $post->user->id,
//                'name' => $post->user->profile->name,
//            ],
//            'categories' => array_map(
//                function ($v) {
//                    return $v->name;
//                },
//                $post->categories
//            ),
//            'tags' => array_map(
//                function ($v) {
//                    return $v->name;
//                },
//                $post->tags
//            )
//        ]
//    ]
//);
// extra data end

$data = [
    'author' => $post->user->profile->name,
    'dateofpublication' => date('d.m.Y', $post->postPublishTime),
    'section' => isset($post->categories[0]) ? $post->categories[0]->name : false,
    'articletitle' => $post->postTitle,
    'articletype' => \common\models\Post::getPostTypeLabel($post->postType),
    'dateofcreation' => date('d.m.Y', $post->postCreateTime),
    'status' => $post->postStatus
];

if ($post->editorId) {
    $editor = \common\models\User::findOne(['id' => $post->editorId]);
    $data['redactor'] = $editor->username;
} else {
    $data['redactor'] = null;
}

\frontend\widgets\DataLayer\DataLayerWidget::widget(['data' => $data]);

if (isset($post->categories[0])) {
    echo \frontend\widgets\Branding\Branding::widget(
        [
            'custom' => [
                1 => $post->categories[0]->categorySlug
            ]
        ]
    );
}

// попап фейсбука/вконтакта
// временно не выводим
// TODO: clean fb/vk popups
//echo \frontend\widgets\SocialPopup\SocialPopup::widget();

$this->registerJs(
    "
    (function(w,doc) {
    if (!w.__utlWdgt ) {
        w.__utlWdgt = true;
        var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
        s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
        var h=d[g]('body')[0];
        h.appendChild(s);
    }})(window,document);
"
);

$this->registerCss(
    "
.mc-submit{
    padding-left: 20px;
}
"
);

// responsive video CSS
$this->registerCss(
    '
    #content video {
        width: 100%    !important;
        height: auto   !important;
    }
'
);

// responsive video JS
$this->registerJs(
    '
    var $allVideos = $("iframe[src*=\'//player.vimeo.com\'], iframe[src*=\'//www.youtube.com\'], object, embed"),
        $fluidEl = $("#width_ethalon");

        console.log($allVideos);

    	$allVideos.each(
            function () {
                $(this)
                // jQuery .data does not work on object/embed elements
                .attr(\'data-aspectRatio\', this.height / this.width)
                .removeAttr(\'height\')
                .removeAttr(\'width\');
            }
        );

    	$(window).resize(
            function () {
                var
                newWidth = $fluidEl.width();
                $allVideos.each(
                    function () {
                        var
                        $el = $(this);
                        $el
                        .width(newWidth)
                        .height(newWidth * $el.attr(\'data-aspectRatio\'));

                    }
                );
                console.log("resize:" + $fluidEl.width());
            }
        ).resize();
',
    yii\web\View::POS_READY
);


// Вуйрич
$this->registerJs(
    '
(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="//files.jumpoutpopup.ru/573ab83d632fe-user.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);

(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="//files.jumpoutpopup.ru/573ab94949d60-user.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);

(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="//files.jumpoutpopup.ru/575a7f0cc2e25-user.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);
');

?>
<div data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff"
     data-share-counter-size="12" data-top-button="false" data-share-counter-type="disable" data-share-style="1"
     data-mode="share_picture" data-like-text-enable="false" data-hover-effect="scale" data-mobile-view="true"
     data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000"
     data-share-shape="round-rectangle" data-sn-ids="fb.vk.tw." data-share-size="30" data-background-color="#ffffff"
     data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.gp." data-pid="1405479"
     data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="false"
     data-selection-enable="false" class="uptolike-buttons"></div>




<script type="text/javascript">
    <?php 
$init = <<<JS
 // $('.codeform-block').css('background-color', 'red');
 $('.webform-wrapper on-customer-page').appendTo( $('.codeform-block'));
JS;

$this->registerJs($init, yii\web\View::POS_READY);
?>
   
</script>