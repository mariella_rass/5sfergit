<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.05.15
 * Time: 15:34
 *
 * @var $model frontend\models\Post
 */
?>
<?php
if (false && $index % 5 == 1 && $index != 1 && !Yii::$app->request->isAjax):
    ?>
    <div class="row margin-bottom-40">
        <div class="col-xs-12">
            <div style="background-color: #f5f5f5;padding-top: 5px;padding-bottom: 5px;">
                <ins class="adsbygoogle overflow-hidden padding-bottom-20"
                     style="display:block"
                     data-ad-client="ca-pub-9641652080415765"
                          data-ad-slot="9501678338"
                          data-ad-format="auto"></ins>
            </div>
        </div>
        <?php
        $this->registerJsFile(
            "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js",
            ['position' => \yii\web\View::POS_HEAD]
        );
        $this->registerJs(
            "(adsbygoogle = window.adsbygoogle || []).push({});",
            \yii\web\View::POS_READY,
            'adsense in list ' . $index
        );
        ?>
    </div>
    <?php
endif;
?>
<div class="row margin-bottom-20">
    <div class="col-md-2 visible-lg">
        <?= $this->render(
            '/author/_ava',
            [
                'model' => $model->user,
                'showName' => true,
                'active' => true
            ]
        ) ?>
    </div>
    <div class="col-md-12 col-lg-10">
        <div class="row">
            <div class="col-xs-12">
                <?php foreach ($model->categories as $category) {
                    echo $this->render('_category', ['category' => $category]);
                };
                ?>
            </div>

            <div class="col-xs-12">
                <h2><a href="<?= $model->getUrl() ?>"><?= $model->postTitle ?></a></h2>
            </div>

            <div class="col-xs-12 margin-bottom-10">
                <span class="margin-right-10 hidden-lg">
                    <i class="fa fa-user margin-right-5"></i>
                    <a href="<?= $model->user->url ?>"><?= $model->user->profile->name ?></a></span>
                <span class="margin-right-10"><i class="fa fa-eye margin-right-5"></i><?= $model->postRawViews ?></span>
                <span class="margin-right-10"><i
                        class="fa fa-share-alt margin-right-5"></i><?= $model->getFormatedShares() ?></span>
            </div>

<?php $path = str_replace( "postImage/", "postImage/thumb-" , $model->getImgSrc('postImage')) ;
if(!(@getimagesize($path))){
    $path = $model->getImgSrc('postImage');
}

?>
            <div class="col-xs-12 margin-bottom-10">
                <a href="<?= $model->getUrl() ?>"><img  class="img-responsive rounded-2x lazyload"
                                                        alt="<?= $model->postTitle ?>"
                                                        src="<?php echo  $path;

                                                         // $model->getImgSrc('postImage') ?>"
                                                        data-src="<?php echo  $path; ?>" />
                </a>
            </div>

            <div class="col-xs-12">
                <p>
                    <?= strip_tags($model->postLead) ?> <a href="<?= $model->getUrl() ?>">Читать дальше</a>
                </p>
            </div>

        </div>
    </div>
</div>