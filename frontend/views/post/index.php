<?php
/*
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 *
 * */

if ($sort == 'popular') {
    $popularTitle = ' Популярные статьи.';
    if ($term) {
        $popularDescription = 'Популярное в ' . $term->name . ' ';
    } else {
        $popularDescription = 'Самое популярное на 5 сфер. ';
    }
} else {
    $popularTitle = '';
    $popularDescription = '';
}

if ($term) {
    $this->title = $term->title . (!isset($term->categoryEditable) || $term->categoryEditable == false ? '.' : '') . $popularTitle;
    $description = $popularDescription . $term->description;
} else {
    $this->title = 'Твой онлайн-журнал по развитию и личностному росту.' . $popularTitle;
    $description = $popularDescription . 'Советы по развитию, личностному росту, построению бизнеса, продажам и здоровому образу жизни от профессиональных экспертов';
}

if (Yii::$app->request->get('page', 1) > 1) {
    $this->title = $this->title . ' Страница ' . Yii::$app->request->get('page', null);
    $description = $description . ' Страница ' . Yii::$app->request->get('page', null);
}

$this->registerMetaTag(
    [
        'name' => 'Description',
        'content' => $description
    ]
);

$this->title = trim(mb_ucfirst($this->title));

if (mb_substr(mb_ucfirst($this->title), -1, 1) == '.') {
    $this->title = mb_substr($this->title, 0, mb_strlen($this->title) - 1);
}

$this->title = $this->title . ' - 5 сфер';

Yii::$app->opengraph->title = $term ? $term->title : '5sfer.com, твой онлайн-журнал по развитию и личностному росту';
Yii::$app->opengraph->description = $term ? $term->description : 'Советы по развитию, личностному росту, построению бизнеса, продажам и здоровому образу жизни от профессиональных экспертов';
Yii::$app->opengraph->image = Yii::$app->params['frontendBaseUrl'] . '/img/5sfer-1200x900.jpg';
Yii::$app->opengraph->twitter->card = 'summary';
Yii::$app->opengraph->twitter->site = '5sfer.com';
if ($this->context->getCanonical()) {
    Yii::$app->opengraph->url = $this->context->getCanonical();
}

?>

<div class="margin-bottom-40">
    <div class="head">
        <h1 class="visible-md visible-lg">
            <?php if ($filter == 'tag'): ?>
                <i class="fa fa-tags"></i>
                <?= mb_ucfirst(($sort == 'popular' ? 'Популярное по тэгу ' : '') . $term->name); ?>
            <?php elseif ($filter == 'category'): ?>
                <?= mb_ucfirst(($sort == 'popular' ? 'Популярное в разделе ' : '') . $term->name); ?>
            <?php else: ?>
                <?= $sort == 'popular' ? 'Читай самое популярное' : 'Читай самое новое'; ?>
            <?php endif ?>
        </h1>

        <div class="hidden-md hidden-lg">
            <?php if ($filter == 'tag'): ?>
                <i class="fa fa-tags"></i>
                <?= mb_ucfirst($term->name); ?>
            <?php elseif ($filter == 'category'): ?>
                <?= mb_ucfirst($term->name); ?>
            <?php else: ?>
                <?= $sort == 'popular' ? 'Cамое популярное' : 'Cамое новое'; ?>
            <?php endif ?>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <?php
        if (isset($term->slug) && $term->slug == 'novosti' && isset($filter) && $filter == 'category') {
            // новости
            echo \yii\helpers\Html::tag(
                'li',
                \yii\helpers\Html::a(
                    '<span class="hidden-lg"><i class="fa fa-calendar"></i></span><span class="visible-lg">Новые статьи</span>',
                    \yii\helpers\Url::to(
                        [
                            'post/index',
                            'term' => null,
                            'filter' => null,
                            'sort' => '',
                            'page' => 1
                        ]
                    )
                ),
                ['class' => 'pull-right']
            );
            echo \yii\helpers\Html::tag(
                'li',
                \yii\helpers\Html::a(
                    '<span class="hidden-lg"><i class="fa fa-bar-chart"></i></span><span class="visible-lg">Популярные статьи</span>',
                    \yii\helpers\Url::to(
                        [
                            'post/index',
                            'term' => null,
                            'filter' => null,
                            'sort' => 'popular',
                            'page' => 1
                        ]
                    )
                ),
                ['class' => 'pull-right']
            );
            echo \yii\helpers\Html::tag(
                'li',
                \yii\helpers\Html::a(
                    '<span class="hidden-lg"><i class="fa fa-newspaper-o"></i></span><span class="visible-lg">Новости</span>',
                    \yii\helpers\Url::to(
                        [
                            'post/index',
                            'term' => 'novosti',
                            'filter' => 'category',
                            'sort' => '',
                            'page' => 1
                        ]
                    )
                ),
                ['class' => 'pull-right active']
            );
        } else {
            // не новости
            echo \yii\helpers\Html::tag(
                'li',
                \yii\helpers\Html::a(
                    '<span class="hidden-lg"><i class="fa fa-calendar"></i></span><span class="visible-lg">Новые статьи</span>',
                    \yii\helpers\Url::to(
                        [
                            'post/index',
                            'term' => $term ? $term->slug : null,
                            'filter' => $filter,
                            'sort' => '',
                            'page' => 1
                        ]
                    )
                ),
                ['class' => 'pull-right' . ($sort != 'popular' ? ' active' : '')]
            );
            echo \yii\helpers\Html::tag(
                'li',
                \yii\helpers\Html::a(
                    '<span class="hidden-lg"><i class="fa fa-bar-chart"></i></span><span class="visible-lg">Популярные статьи</span>',
                    \yii\helpers\Url::to(
                        [
                            'post/index',
                            'term' => $term ? $term->slug : null,
                            'filter' => $filter,
                            'sort' => 'popular',
                            'page' => 1
                        ]
                    )
                ),
                ['class' => 'pull-right' . ($sort == 'popular' ? ' active' : '')]
            );
            // показываем вкладку только на главной и в разделе новости
            if (!isset($term->slug) || $term->slug == 'novosti') {
                echo \yii\helpers\Html::tag(
                    'li',
                    \yii\helpers\Html::a(
                        '<span class="hidden-lg"><i class="fa fa-newspaper-o"></i></span><span class="visible-lg">Новости</span>',
                        \yii\helpers\Url::to(
                            [
                                'post/index',
                                'term' => 'novosti',
                                'filter' => 'category',
                                'sort' => '',
                                'page' => 1
                            ]
                        )
                    ),
                    ['class' => 'pull-right']
                );
            }
        }
        ?>
    </ul>
</div>

<?php

echo \yii\widgets\ListView::widget(
    [
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_post_thumb',
        'summary' => '',
        'pager' => [
            'class' => \frontend\components\ScrollPager::className(),
            'negativeMargin' => 10,
            'triggerOffset' => 0,
            'spinnerTemplate' => '<div class="row"><div class="col-xs-12 text-center"><img width="48" height="48" src="{src}"></div></div>',
            'spinnerSrc' => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHdpZHRoPSI0MHB4IiBoZWlnaHQ9IjQwcHgiIHZpZXdCb3g9IjAgMCA0MCA0MCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWw6c3BhY2U9InByZXNlcnZlIiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEuNDE0MjE7IiB4PSIwcHgiIHk9IjBweCI+CiAgICA8ZGVmcz4KICAgICAgICA8c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWwogICAgICAgICAgICBALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7CiAgICAgICAgICAgICAgZnJvbSB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoLTM1OWRlZykKICAgICAgICAgICAgICB9CiAgICAgICAgICAgIH0KICAgICAgICAgICAgQGtleWZyYW1lcyBzcGluIHsKICAgICAgICAgICAgICBmcm9tIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC0zNTlkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CiAgICAgICAgICAgIHN2ZyB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7CiAgICAgICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAxLjVzIGxpbmVhciBpbmZpbml0ZTsKICAgICAgICAgICAgICAgIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuOwogICAgICAgICAgICAgICAgYW5pbWF0aW9uOiBzcGluIDEuNXMgbGluZWFyIGluZmluaXRlOwogICAgICAgICAgICB9CiAgICAgICAgXV0+PC9zdHlsZT4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJvdXRlciI+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwwQzIyLjIwNTgsMCAyMy45OTM5LDEuNzg4MTMgMjMuOTkzOSwzLjk5MzlDMjMuOTkzOSw2LjE5OTY4IDIyLjIwNTgsNy45ODc4MSAyMCw3Ljk4NzgxQzE3Ljc5NDIsNy45ODc4MSAxNi4wMDYxLDYuMTk5NjggMTYuMDA2MSwzLjk5MzlDMTYuMDA2MSwxLjc4ODEzIDE3Ljc5NDIsMCAyMCwwWiIgc3R5bGU9ImZpbGw6YmxhY2s7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNNS44NTc4Niw1Ljg1Nzg2QzcuNDE3NTgsNC4yOTgxNSA5Ljk0NjM4LDQuMjk4MTUgMTEuNTA2MSw1Ljg1Nzg2QzEzLjA2NTgsNy40MTc1OCAxMy4wNjU4LDkuOTQ2MzggMTEuNTA2MSwxMS41MDYxQzkuOTQ2MzgsMTMuMDY1OCA3LjQxNzU4LDEzLjA2NTggNS44NTc4NiwxMS41MDYxQzQuMjk4MTUsOS45NDYzOCA0LjI5ODE1LDcuNDE3NTggNS44NTc4Niw1Ljg1Nzg2WiIgc3R5bGU9ImZpbGw6cmdiKDIxMCwyMTAsMjEwKTsiLz4KICAgICAgICA8L2c+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwzMi4wMTIyQzIyLjIwNTgsMzIuMDEyMiAyMy45OTM5LDMzLjgwMDMgMjMuOTkzOSwzNi4wMDYxQzIzLjk5MzksMzguMjExOSAyMi4yMDU4LDQwIDIwLDQwQzE3Ljc5NDIsNDAgMTYuMDA2MSwzOC4yMTE5IDE2LjAwNjEsMzYuMDA2MUMxNi4wMDYxLDMzLjgwMDMgMTcuNzk0MiwzMi4wMTIyIDIwLDMyLjAxMjJaIiBzdHlsZT0iZmlsbDpyZ2IoMTMwLDEzMCwxMzApOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksMjguNDkzOUMzMC4wNTM2LDI2LjkzNDIgMzIuNTgyNCwyNi45MzQyIDM0LjE0MjEsMjguNDkzOUMzNS43MDE5LDMwLjA1MzYgMzUuNzAxOSwzMi41ODI0IDM0LjE0MjEsMzQuMTQyMUMzMi41ODI0LDM1LjcwMTkgMzAuMDUzNiwzNS43MDE5IDI4LjQ5MzksMzQuMTQyMUMyNi45MzQyLDMyLjU4MjQgMjYuOTM0MiwzMC4wNTM2IDI4LjQ5MzksMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxMDEsMTAxLDEwMSk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMy45OTM5LDE2LjAwNjFDNi4xOTk2OCwxNi4wMDYxIDcuOTg3ODEsMTcuNzk0MiA3Ljk4NzgxLDIwQzcuOTg3ODEsMjIuMjA1OCA2LjE5OTY4LDIzLjk5MzkgMy45OTM5LDIzLjk5MzlDMS43ODgxMywyMy45OTM5IDAsMjIuMjA1OCAwLDIwQzAsMTcuNzk0MiAxLjc4ODEzLDE2LjAwNjEgMy45OTM5LDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoMTg3LDE4NywxODcpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTUuODU3ODYsMjguNDkzOUM3LjQxNzU4LDI2LjkzNDIgOS45NDYzOCwyNi45MzQyIDExLjUwNjEsMjguNDkzOUMxMy4wNjU4LDMwLjA1MzYgMTMuMDY1OCwzMi41ODI0IDExLjUwNjEsMzQuMTQyMUM5Ljk0NjM4LDM1LjcwMTkgNy40MTc1OCwzNS43MDE5IDUuODU3ODYsMzQuMTQyMUM0LjI5ODE1LDMyLjU4MjQgNC4yOTgxNSwzMC4wNTM2IDUuODU3ODYsMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxNjQsMTY0LDE2NCk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMzYuMDA2MSwxNi4wMDYxQzM4LjIxMTksMTYuMDA2MSA0MCwxNy43OTQyIDQwLDIwQzQwLDIyLjIwNTggMzguMjExOSwyMy45OTM5IDM2LjAwNjEsMjMuOTkzOUMzMy44MDAzLDIzLjk5MzkgMzIuMDEyMiwyMi4yMDU4IDMyLjAxMjIsMjBDMzIuMDEyMiwxNy43OTQyIDMzLjgwMDMsMTYuMDA2MSAzNi4wMDYxLDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoNzQsNzQsNzQpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksNS44NTc4NkMzMC4wNTM2LDQuMjk4MTUgMzIuNTgyNCw0LjI5ODE1IDM0LjE0MjEsNS44NTc4NkMzNS43MDE5LDcuNDE3NTggMzUuNzAxOSw5Ljk0NjM4IDM0LjE0MjEsMTEuNTA2MUMzMi41ODI0LDEzLjA2NTggMzAuMDUzNiwxMy4wNjU4IDI4LjQ5MzksMTEuNTA2MUMyNi45MzQyLDkuOTQ2MzggMjYuOTM0Miw3LjQxNzU4IDI4LjQ5MzksNS44NTc4NloiIHN0eWxlPSJmaWxsOnJnYig1MCw1MCw1MCk7Ii8+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K',
            'triggerText' => 'Мне нужно больше интересных статей!',
            'triggerTemplate' => '<div class="ias-trigger row"><div class="col-md-push-2 col-md-10"><a class="btn-u btn-u-lg rounded btn-u-blue">{text}</a></div></div>',
            'eventOnRendered' => 'function(){$(document.body).trigger("sticky_kit:recalc")}'
        ]
    ]
);

// extra data begin
if (isset($term) && isset($term->primaryKey) && isset($term->name)) {
    $data = [];
    if ($filter == 'tag') {
        $data['tags'][] = $term->name;
    } elseif ($term) {
        $data['categories'][] = $term->name;
    }
    echo \frontend\widgets\AnalyticsExtraData::widget(['data' => $data]);
}
// extra data end

if ($filter != 'tag' && isset($term) && isset($term->primaryKey) && isset($term->slug)) {
    echo \frontend\widgets\Branding\Branding::widget(
        [
            'custom' => [
                1 => $term->slug
            ]
        ]
    );
}

function mb_ucfirst($string, $encoding = 'utf8')
{
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

?>

