<?php
/**
 * @var $this yii\web\View
 */

$this->title = 'Найди на пяти сферах';
$this->params['showSubscribe'] = false;
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);

?>
    <div class="row">
        <div class="col-xs-12 margin-bottom-40">
            <span class="head"><?= Yii::$app->request->get('q') ? 'Результаты поиска' : 'Поиск'; ?></span>
        </div>
        <div class="col-xs-12">
            <form action="/search">
                <div class="input-group">
                    <input id="gsefield" name="q" class="form-control" placeholder="Что искать?"
                           value="<?= htmlentities(Yii::$app->request->get('q')) ?>">
                    <span class="input-group-btn">
                        <button class="btn-u btn-u-default" type="submit"><i
                                class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
        <div class="col-xs-12">
            <gcse:searchresults-only enableOrderBy="false">
            </gcse:searchresults-only>
            <?php
            $this->registerJs(
                "
              (function() {
                var cx = '009665364522658859654:yyg4kzme6lw';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                    '//cse.google.com/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
              })();
        "
            );
            ?>
        </div>
        <div class="col-xs-12">
            <a id="firstPageBtn" class="btn btn-lg rounded btn-primary" style="display: none">Смотреть с начала</a>
            <a id="nextPageBtn" class="btn btn-lg rounded btn-primary" style="display: none">Больше результатов</a>
        </div>
    </div>
<?php
$this->registerJs(
    "
        $('.gs-spelling>a').on('click', function(e){\$('#gsefield').val(\$(e.target).text()); if (window.history){history.pushState({}, window.title, location.href.replace(/q=[^&]*/, 'q='+\$(e.target).text()))}});
        $('#nextPageBtn').on('click', function(e){
            $('.gsc-cursor-current-page').next().click()
        });
        $('#firstPageBtn').on('click', function(e){
            $('.gsc-cursor>.gsc-cursor-page:first').click()
        })
        setInterval(function(){
            var nxt = $('.gsc-cursor-current-page').next();
            if (nxt.length == 1) {
                $('#nextPageBtn').css('display','inline-block');
            } else {
                $('#nextPageBtn').css('display','none');
            }
            var first = $('.gsc-cursor>.gsc-cursor-page:first');
            if($('.gsc-cursor').length && !$('.gsc-cursor>.gsc-cursor-page:first').is('.gsc-cursor-current-page')){
                $('#firstPageBtn').css('display', 'inline-block');
            } else {
                $('#firstPageBtn').css('display', 'none');
            }
        }, 200);
    "
);
?>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.05.15
 * Time: 15:06
 */

//$('a.gs-title').live('click', function(e){location.href = $(e.target).attr('data-ctorig').replace('http://www.likar.info',''); return false;})

