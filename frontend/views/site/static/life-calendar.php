<?php
/*
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 *
 * */

\frontend\assets\LifeCalendar\LifeCalendarAsset::register($this);
$post = \frontend\models\Post::findOne(22526);

$post->postText = $post->postText . <<<EOD
    <div class=row>
        <div class="col-xs-12 hidden-print" id=calendar-settings>
            <form>
                <div class=form-group><label for=b-date id=lang_set_birthday></label>

                    <div id=sdate-picker class="row">
                        <div class="col-md-4">
                            <select id=sdate-day class="form-control col-xs-4"></select>

                            <div class="margin-bottom-20 visible-xs"></div>
                        </div>
                        <div class="col-md-4">
                            <select id=sdate-month class="form-control col-xs-4"></select>

                            <div class="margin-bottom-20 visible-xs"></div>
                        </div>
                        <div class="col-md-4">
                            <select id=sdate-year class="form-control col-xs-4"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for=theme id=lang_set_theme></label>
                        <select class="form-control" id=theme>
                            <option value=0>Dark</option>
                            <option value=1>Light</option>
                            <option value=2>Red</option>
                        </select>

                    </div>
                    <div class="form-group col-md-4">
                        <label for=lang id=lang_select_lang></label>
                        <select class=form-control id=lang>
                            <option value=0>Русский</option>
                            <option value=1>English</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for id=lang_save_print></label>

                        <div class="btn-group btn-group-justified"><a href=# class="btn btn-default" id=lang_save_jpg></a>
                            <a href=#
                               class="btn btn-default"
                               id=lang_save_pdf></a>
                            </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12">
            <div id=calendar-canvas class="text-center"></div>
        </div>
    </div>
</pre>
EOD;

echo $this->render("/post/view", ['post' => $post]);

?>





