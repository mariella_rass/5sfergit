<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.06.15
 * Time: 16:54
 */

$this->params['showSubscribe'] = false;

$this->registerJsFile("//vk.com/js/api/openapi.js?116", ['position' => \yii\web\View::POS_HEAD]);

$this->registerJs(
    "
    // facebook

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = \"//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=132147813635823\";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    var page_like_or_unlike_callback = function(url, html_element) {
      console.log(\"page_like_or_unlike_callback\");
      console.log(url);
    }

    // vk

    VK.init({apiId: 2647723, onlyWidgets: true});

    VK.Widgets.Group(\"vk_groups_ips\", {
        mode: 0,
        width: \"300\",
        height: \"200\",
        color1: 'FFFFFF',
        color2: '2B587A',
        color3: '5B7FA6'
    }, 32165164);

    VK.Widgets.Group(\"vk_groups_5sfer\", {
            mode: 0,
            width: \"300\",
            height: \"200\",
            color1: 'FFFFFF',
            color2: '2B587A',
            color3: '5B7FA6'
        }, 100926688);

"
);

$this->registerJs(
    "
    var page_like_or_unlike_callback = function(url, html_element) {
      console.log(\"page_like_or_unlike_callback\");
      console.log(url);
      //document.show_subscribe_form();
    }

    // In your onload handler
    FB.Event.subscribe('edge.create', page_like_or_unlike_callback);
    //FB.Event.subscribe('edge.remove', page_like_or_unlike_callback);

    VK.Observer.subscribe(\"widgets.groups.joined\", function f() {
        console.log(\"Thank you for your subscribe\");
        //document.show_subscribe_form();
    });

    ",
    \yii\web\View::POS_LOAD
);

$this->registerCss(
    "
    #nextBtns > a{
        margin-bottom: 20px;
        }
    #vk_groups_ips, #vk_groups_5sfer {
            margin: auto;
        }
    "
);
?>
<div class="row">
    <div class="col-xs-12 margin-bottom-20">
        <div class="headline"><h2>Спасибо за подписку!</h2></div>
        <p>Дорогой друг,</p>

        <p>Все получилось, твои данные успешно добавлены. <br>Проверь почту, письмо с бонусами отправлено на указанный
            электронный адрес.</p>

        <p>Условия конкурса <a href="http://5sfer.com/22227">здесь</a>.</p>

        <p>Детальную информацию о выступлении Ника Вуйчича и Ицхака Пинтосевича смотри <a
                href="http://nick-vujicic.com.ua">тут</a>.</p>
    </div>

    <div class="col-xs-12 text-center margin-bottom-20">
        <h3>5 сфер в социальных сетях</h3>
    </div>

    <div class="col-xs-12 col-sm-6 text-center margin-bottom-20">
        <div class="fb-page" data-href="https://www.facebook.com/5sfer" data-small-header="false"
             data-width="300" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"
             data-height="250">
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div id="vk_groups_5sfer"></div>
    </div>

    <div class="col-xs-12 text-center margin-bottom-20">
        <h3>IPS в социальных сетях</h3>
    </div>

    <div class="col-xs-12 col-sm-6 text-center margin-bottom-20">
        <div class="fb-page" data-href="https://www.facebook.com/isaac.pintosevich.systems" data-small-header="false"
             data-width="300" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"
             data-height="250">
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div id="vk_groups_ips"></div>
    </div>
    <div class="col-xs-12 text-center margin-top-20 margin-bottom-20">
        <h3 class="margin-top-20">Выбери что делать дальше</h3>
    </div>
    <div id="nextBtns" class="col-xs-12 text-center">
        <a class="btn-u btn-u-blue btn-u-lg rounded-2x margin-right-10"
           href="<?= \yii\helpers\Url::to('/', true) ?>">На
            главную</a>
    </div>

</div>
