<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */


if ($exception->statusCode != 404) {
    $this->title = $name;
    ?>
    <div class="site-error margin-bottom-40">

        <h1>Увы, но что-то пошло не так</h1>

        <div>
            Будьте уверены, наши инжинеры уже занимаются решением этой проблемы.
        </div>
    </div>
    <div class="site-error margin-bottom-40">
        <h1>Что делать?</h1>

        <p class="margin-bottom-20">Вы можете перейди на <a href="/">главную
                страницу</a> или воспользоватся поиском</p>

        <form action="/search">
            <div class="input-group">
                <input id="gsefield" name="q" class="form-control" placeholder="Что искать?" value="">
                            <span class="input-group-btn">
                                <button class="btn-u btn-u-default" type="submit"><i class="fa fa-search"></i></button>
                            </span>
            </div>
        </form>

    </div>
    <?php
} else {
    $this->title = "Страница не найдена (404)"
    ?>
    <div class="site-error margin-bottom-40">

        <h1><?= Html::encode($this->title) ?></h1>

        <div>
            Увы, но запрашиваемая страница не найдена. Этому может быть несколько объяснений:
        </div>
        <p>
        <ol>
            <li>Страница удалена</li>
            <li>Страница перенесена в другое место</li>
            <li>Возможно, в адресе упущена какая-то буква</li>
        </ol>
        </p>
    </div>
    <div class="site-error margin-bottom-40">
        <h1>Что делать?</h1>

        <p class="margin-bottom-20">Попробуй воспользоваться поиском по сайту или перейди на <a href="/">главную
                страницу</a></p>

        <form action="/search">
            <div class="input-group">
                <input id="gsefield" name="q" class="form-control" placeholder="Что искать?" value="">
                            <span class="input-group-btn">
                                <button class="btn-u btn-u-default" type="submit"><i class="fa fa-search"></i></button>
                            </span>
            </div>
        </form>

    </div>
    <?php
};
?>
