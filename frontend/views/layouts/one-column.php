<?php $this->beginContent('@app/views/layouts/standart.php'); ?>
<div id="content one-column">
    <?= $this->render('//layouts/subscribe') ?>
    <div id="main" class="one-column <?= $this->context->getContainerClass() ?>">
        <?= $content ?>
    </div>
</div>
<?php $this->endContent(); ?>