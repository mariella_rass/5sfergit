<div id="footer" class="footer-v3 footer hidden-print">
    <?php /*
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-sm-12 md-margin-bottom-40">
                    <a href="/"><img id="logo-footer" class="footer-logo" src="/img/logo-white.png" alt=""></a>

                    <p>Каждый день мы публикуем для тебя лучшие статьи, советы и упражнения от наших экспертов которые
                        научат тебя быть эффективным, правильно ставить цели, добиваться успеха и поддерживать баланс во
                        всех сферах жизни.</p>

                    <p>
                        <a class="btn-u rounded btn-u-brown btn-u-lg" href="#">Подписаться!</a>
                    </p>

                </div>
                <!--/col-md-3-->
                <!-- End About -->

                <!-- Simple List -->
                <div class="col-sm-6 md-margin-bottom-40">
                    <div class="thumb-headline"><h2>Разделы</h2></div>
                    <ul class="list-unstyled simple-list margin-bottom-20">
                        <?php
                        foreach (['Действуй!', 'Влияй!', 'Живи!', 'Богатей!', 'Люби!'] as $key) {
                            $cat = \common\models\Category::getByName($key);
                            echo \yii\helpers\Html::tag('li', \yii\helpers\Html::a($cat->name, $cat->url));
                        }
                        ?>
                    </ul>
                </div>
                <!--/col-md-3-->

                <div class="col-sm-6">
                    <div class="thumb-headline"><h2>Специальные проекты</h2></div>
                    <ul class="list-unstyled simple-list">
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Customer Service</a></li>
                        <li><a href="#">New on Unify</a></li>
                        <li><a href="#">Tour the New Journey</a></li>
                        <li><a href="#">Registration Requirements</a></li>
                    </ul>
                </div>
                <!--/col-md-3-->
                <!-- End Simple List -->
            </div>
        </div>
    </div>
    <!--/footer-->
 */ ?>

    <div class="copyright">
        <div class="container">
            <div class="row">
                <!-- Terms Info-->
                <div class="col-md-8 small">
                    <p>
                        <?= date('Y') ?> © Все права защищены. Портал компании
                        <a target="_blank" href="http://pintosevich.com/?utm_medium=affiliate&utm_source=5sfer">Isaac Pintosevich Systems</a><br>
                        При цитировании материалов сайта, включая охраняемые авторские произведения,
                        ссылка на сайт обязательна.<br>
                        Написать нам <a href="mailto:info@5sfer.com">info@5sfer.com</a> | <a
                            href="<?= \yii\helpers\Url::to(
                                ['site/view', 'view' => 'politika-konfidencialnosti']
                            ) ?>">Политика
                            конфиденциальности</a> | <a
                            href="<?= \yii\helpers\Url::to(['site/view', 'view' => 'about']) ?>">О проекте</a> | <a href="https://drive.google.com/file/d/0B7bSD-8y7yDiOW1RcHZJQkVxYjg/view?usp=sharing" target="_blank">Реклама на портале 5 Сфер</a>
                    </p>
                </div>
                <!-- End Terms Info-->

                <!-- Social Links -->
                <div class="col-md-4">
                    <ul class="social-icons pull-right" itemscope itemtype="http://schema.org/Organization">
                        <link itemprop="url" href="http://5sfer.com">
                        <li><a itemprop="sameAs" href="https://www.facebook.com/5sfer" data-original-title="Facebook"
                               class="rounded-x social_facebook" target="_blank"></a></li>
                        <li><a itemprop="sameAs" href="https://vk.com/5sfer" data-original-title="Vk"
                               class="rounded-x social_vk" target="_blank"></a></li>
                        <li><a itemprop="sameAs" href="https://twitter.com/pintosevich_7" data-original-title="Twitter"
                               class="rounded-x social_twitter" target="_blank"></a></li>
                        <li><a itemprop="sameAs" href="https://plus.google.com/u/0/105743010412667579294/posts"
                               data-original-title="Goole Plus" class="rounded-x social_googleplus" target="_blank"></a>
                        </li>
                        <li><a itemprop="sameAs" href="https://www.youtube.com/user/etopobeda" data-original-title="Youtube"
                               class="rounded-x social_youtube" target="_blank"></a></li>
                    </ul>
                    <div class="clear-both"></div>
                    <div class="padding-top-5 pull-right hidden-xs hidden-sm">
                        <!-- Yandex.Metrika informer -->
                        <a href="https://metrika.yandex.ru/stat/?id=24572219&amp;from=informer"
                           target="_blank" rel="nofollow"><img
                                src="https://informer.yandex.ru/informer/24572219/3_0_222222FF_222222FF_1_pageviews"
                                style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                                title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                                onclick="try{Ya.Metrika.informer({i:this,id:24572219,lang:'ru'});return false}catch(e){}"/></a>
                        <!-- /Yandex.Metrika informer -->
                    </div>
                </div>
                <!-- End Social Links -->

            </div>
        </div>
    </div>
    <!--/copyright-->
    <?php $this->registerJs(
        "window.setTimeout(function(){
                jQuery('#footer-banner').removeClass('hidden');
                window.setInterval(
                    function(){
                        jQuery('#footer').css('height', 21 + (jQuery('#footer .copyright').height())+(jQuery('#footer-banner').height()) + 'px');
                        jQuery('body').css('margin-bottom', (jQuery('#footer').height()) + 'px');

                        }, 100);
                jQuery('#footer-banner-close').on('click', function(){
                    jQuery('#footer-banner').remove();
                    jQuery('#footer-spacer').remove();
                })
            },
            7000)
            "
    ); ?>
</div>
<div id="footer-banner" class="visible-xs visible-sm text-center hidden hidden-print">
    <div id="banner-container">
        <?= \frontend\widgets\DFP::widget(
            [
                'iu' => '/59738484/mobile-bottom',
                'sz' => [320, 100],
                //'cid' => 'div-gpt-ad-1436364343146-0'
            ]
        ); ?>
    </div>

    <div id="footer-banner-close" class="rounded-x"><i class="fa fa-times"></i></div>
</div>