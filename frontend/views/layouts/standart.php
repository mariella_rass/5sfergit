<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.08.15
 * Time: 13:01
 *
 * include heheder and footer and other fishechki :)
 *
 */

/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\StandartAssets::register($this);

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>
    <div id="fb-root"></div>

<?= $this->render('header'); ?>

    <div id="wrapper" class="container bg-color-white">
        <section id="middle" data-sticky_parent>
            <?= $content ?>
        </section>
    </div>


<?php echo \frontend\widgets\StreamEvents\StreamEvents::widget(); ?>


<?php
echo $this->render('footer');

$this->registerJsFile(
    "/plugins/back-to-top.js",
    [
        'position' => \yii\web\View::POS_END,
        'depends' => \yii\web\JqueryAsset::className()
    ]
);
$this->registerJsFile(
    "/plugins/smoothScroll.js",
    [
        'position' => \yii\web\View::POS_END,
        'depends' => \yii\web\JqueryAsset::className()
    ]
);
$this->registerJsFile(
    "/js/custom.js",
    [
        'position' => \yii\web\View::POS_END,
        'depends' => \yii\web\JqueryAsset::className()
    ]
);
$this->registerJsFile(
    "/js/app.js",
    [
        'position' => \yii\web\View::POS_END,
        'depends' => \yii\bootstrap\BootstrapPluginAsset::className()
    ]
);
$this->registerJs("App.init();", \yii\web\View::POS_LOAD);


////  кода "JumpOut" (id:986) подписка привыходе со странице
//$this->registerJs(
//    '
//(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
//if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="http://popupfiles.makedreamprofits.ru/55a6b7fa89748-group.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);
//',
//    \yii\web\View::POS_LOAD
//);

$this->registerJs(
    '
    window.jo_callback = function (action) {
        console.log("JO: " + action);
    };
    '
);

$this->endContent(); ?>