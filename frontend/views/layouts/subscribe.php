<?php
if (isset($this->params['showSubscribe']) && $this->params['showSubscribe'] === false) {
    return;
}
if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/'){
    return;
}
$subscribed = \Yii::$app->getRequest()->getCookies()->getValue('subscribed', 0);
?>
    <div id="subscribe" class="margin-bottom-40 hidden-print">
        <?= \frontend\widgets\DFP::widget(
            [
                'iu' => '/59738484/Head-subsribe',
                'sz' => [728, 90],
                't' => [
                    'subscribed' => $subscribed ? 'true' : 'false'
                ],
                'imgClass' => ($subscribed ? 'subscribed' : '')
            ]
        ); ?>
    </div>
<?php
//$this->registerJs("
//    jQuery('#subscribe').on('click', function(e){
//        var subscribed = jQuery(e.target).hasClass('subscribed');
//        if (subscribed){
//            return true;
//        } else {
//            jo_show(994);
//            return false;
//        }
//    })
//");

//  "JumpOut" (id:994) подписка по клику и скролу?
$this->registerJs(
    '
(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="http://popupfiles.makedreamprofits.ru/55a6366562c86-user.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);
',
    \yii\web\View::POS_LOAD
);

$this->registerJs(
    '
    window.jo_callback = function (action) {

    console.log("JO: " + action);

    // не показываем попап при выходе если хоть один уже вызывался
    if (typeof(jo_eh) != "undefined"){
        if ("undefined" != typeof(attachEvent)) {
            document.detachEvent("onmouseout", jo_eh);
        } else {
            document.removeEventListener("mouseout", jo_eh, true);
        }
    }

    // и блокируем показы выходного на других страницах
//    if (typeof(jooff) == "function"){
//        jooff(986);
//    }
};
'
)
?>