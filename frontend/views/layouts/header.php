<?php
/* @var $this \yii\web\View */

?>

<!--=== Header v5 ===-->
<div class="header-v5 hidden-print">

    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container bg-color-white">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php
                // на главной логотип не кликабельный
                if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/'):
                    ?>
                    <span class="navbar-brand">
                    <img id="logo-header" src="/img/logo.png" alt="5 сфер">
                    </span>
                <?php else: ?>
                    <a class="navbar-brand " href="/">
                        <img id="logo-header" src="/img/logo.png" alt="5 сфер">
                    </a>
                <?php endif; ?>
                <?= \yii\helpers\Html::script(
                    '
                   {
                     "@context": "http://schema.org",
                     "@type": "Organization",
                     "url": "http://5sfer.com",
                     "logo": "http://5sfer.com/img/logo.png"
                   }
                ',
                    ['type' => 'application/ld+json']
                )
                ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">

                <ul class="nav navbar-nav">
                    <!-- Pages -->
                    <li class="top">
                        <a href="<?= \yii\helpers\Url::to(['top10/']) ?>">
                            <i class="fa fa-bar-chart-o"> </i> Топ
                        </a>
                    </li>
                    <?php
                    if ($this->beginCache('mainMenu')) {
                        foreach (['Действуй!', 'Влияй!', 'Живи!', 'Богатей!', 'Люби!'] as $key) {
                            $cat = \common\models\Category::getByName($key);
                            echo \yii\helpers\Html::tag(
                                'li',
                                \yii\helpers\Html::a(
                                    $cat->name,
                                    $cat->url,
                                    [
                                        'onmouseover' => 'this.style.color="#' . $cat->getHexColor() . '"',
                                        'onmouseout' => 'this.style.color="#555"',
                                        'class' => 'header-cat'
                                    ]
                                )
                            );
                        }
                        $this->endCache();
                    };
                    ?>
                    <!-- End Home -->

                    <!-- Misc Pages -->
                    <li class="dropdown mega-menu-fullwidth">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            Еще
                        </a>
                        <ul class="dropdown-menu">
                            <!--div style="height:1px;background-image: url('/img/line.png');background-size:100%"></div-->
                            <li>
                                <div class="mega-menu-content disable-icons">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="mega-menu-heading">Специальные проекты</div>
                                                <ul class="list-unstyled style-list row">
                                                    <?php
                                                    $categories = \common\models\Category::find()->special()->all();
                                                    foreach ($categories as $cat) {
                                                        echo '<li class="col-md-4"><a href="' . $cat->getUrl(
                                                            ) . '">' . $cat->getName() . '</a>';
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="mega-menu-heading">Разное</div>
                                                <ul class="list-unstyled style-list row">
                                                    <li class="col-md-12"><?= \yii\helpers\Html::a(
                                                            'Академия экспертов',
                                                            \yii\helpers\Url::to(['akademiya-ekspertov/'])
                                                        ) ?></li>
                                                    <li class="col-md-12"><?= \yii\helpers\Html::a(
                                                            'Обучение',
                                                            \yii\helpers\Url::to(['events/'])
                                                        ) ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- End Misc Pages -->
                    <li class="hidden-xs hidden-sm">
                        <a href="/search" onclick="return false;" class="navbar-icon">
                            <!--i class="rounded-x icon-bg-grey search fa fa-search search-btn"></i-->
                            <i class="search icon-custom rounded icon-sm icon-bg-grey fa fa-search search-btn "></i>
                        </a>

                        <div class="search-open" style="display: none;">
                            <form action="/search" class="animated fadeIn">
                                <div class="input-group">
                                    <input id="site-search-input" name="q" type="text" class="form-control"
                                           placeholder="Что искать?">
                                    <span class="input-group-btn">
                                        <button class="btn-u btn-u-default" type="submit"><i
                                                class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="hidden-md hidden-lg">
                        <a href="/search">Поиск по сайту</a>
                    </li>
                </ul>
            </div>
            <!--/navbar-collapse-->
        </div>
    </div>
    <!-- End Navbar -->
    <?php if (false && isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/'): ?>
    <div id="promo-alert" class="container padding-top-20 bg-color-white">
        <div class="row margin-bottom-30" style="">
            <div class="col-xs-12 text-center rounded" style="padding-top: 15px;padding-bottom: 10px;border: #cccccc 1px solid;">
                <p class="lead text-primary">Не пропусти <strong>МЕГА классный конкурс</strong> на портале 5сфер!
                </p>

                <p><a class="btn btn-primary btn-lg"
                      href="/21970-konkurs-dlya-vsekh-podpischikov-stranitsy-5-sfer.html?utm_source=5sfer&utm_medium=RP&utm_campaign=konkurs_fb_7_04"
                      target="_blank">Узнай подробности прямо сейчас!</a></p>

            </div>
        </div>
    </div>
    <?php endif ?>
</div>
<?php if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/'): ?>
    <?= \frontend\widgets\Puzomerka\Puzomerka::widget() ?>
<?php endif ?>




