<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="Y_bK0HyZ97Va9Si6m6F5DO4HgKWkFZw7TSb4gVzbGyc" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <?php
    $this->registerLinkTag(
        [
            'rel' => 'canonical',
            'href' => $this->context->getCanonical()
        ]
    );
    ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N24HHC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N24HHC');</script>
<!-- End Google Tag Manager -->
<?php

// выводим контент
echo $content;

$this->registerJsFile('/plugins/respond.js', ['condition' => 'lt IE 9']);
$this->registerJsFile('/plugins/html5shiv.js', ['condition' => 'lt IE 9']);
$this->registerJsFile('/plugins/placeholder-IE-fixes.js', ['condition' => 'lt IE 9']);

$this->registerMetaTag(
    [
        'name' => 'yandex-verification',
        'content' => '4914627a79cafa8e'
    ]
);

$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
