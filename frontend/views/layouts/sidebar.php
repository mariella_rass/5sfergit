<div>
    <div>
        <?= \frontend\widgets\SidebarEvents\SidebarEvents::widget() ?>
    </div>

    <div id="s_banner_top" data-sticky_column>
        <div class="spacer"></div>
        <?= \frontend\widgets\DFP::widget(
            [
                'iu' => '/59738484/300x250-sidebar-up',
                'sz' => [300, 250],
                'cid' => 'div-gpt-ad-1436437461712-0',
                't' => array_intersect_key($this->params, array_flip(['categories', 'author', 'tags']))
            ]
        ); ?>
    </div>

    <div id="s_reccomended">

        <div class="margin-bottom-40">
            <h3>Последние комментарии</h3>

            <div id="mc-last" style="width: 300px"></div>
        </div>

        <?= \frontend\widgets\SidebarRecommended\SidebarRecommended::widget() ?>

        <div id="s_banner_bottom" data-sticky_column_second>
            <div>
                <?= \frontend\widgets\DFP::widget(
                    [
                        'iu' => '/59738484/300x250-sidebar-down',
                        'sz' => [300, 250],
                        'cid' => 'div-gpt-ad-1436437546834-0',
                        't' => array_intersect_key($this->params, array_flip(['categories', 'author', 'tags']))
                    ]
                ); ?>

            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        cackle_widget = window.cackle_widget || [];
        cackle_widget.push({widget: 'CommentRecent', id: 22299});
        (function () {
            var mc = document.createElement('script');
            mc.type = 'text/javascript';
            mc.async = true;
            mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(mc, s.nextSibling);
        })();
    </script>

</div>
<?php
\frontend\assets\StickyKitAsset::register($this);
$this->registerJs(
    '$("[data-sticky_column]").stick_in_parent({
          parent: "[data-sticky_parent]",
          offset_top: 0,
          spacer: false
     });
'
);
$this->registerJs(
    '$("[data-sticky_column_second]").stick_in_parent({
          parent: "[data-sticky_parent]",
          offset_top: 300,
          spacer: true
     });
'
);

$this->registerJs("
                window.setInterval(
                    function(){
                        $('#s_reccomended').css('min-height', $('#s_reccomended').height());
                    }, 200);
")
?>


