<?php $this->beginContent('@app/views/layouts/standart.php'); ?>
<div id="content" class="two-column">
    <?= $this->render('//layouts/subscribe') ?>
    <div id="main" class="<?= $this->context->getContainerClass() ?>">
        <?= $content ?>
    </div>
</div>
<div id="sidebar" class="hidden-print">
    <?= $this->render('sidebar') ?>
</div>
<?php $this->endContent(); ?>

