<?php

/* @var $this \yii\web\View */

\frontend\assets\AppAsset::register($this);

$this->beginContent('@app/views/layouts/main.php');
echo $content;
$this->endContent();