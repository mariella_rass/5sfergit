<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.06.15
 * Time: 15:50
 */

use \common\models\Position;

$startFrom = 1 + $dataProvider->pagination->pageSize * $dataProvider->pagination->page;
$colors = [-1 => 'red', 0 => 'gray', '1' => 'green'];
$arrows = [-1 => 'fa-arrow-down', 0 => 'fa-minus', '1' => 'fa-arrow-up'];
$sign = function ($val) {
    return $val > 0 ? 1 : ($val < 0 ? -1 : 0);
};

$position24 = Position::delta($startFrom + $index, $model->position1);
$position7 = Position::delta($startFrom + $index, $model->position7);
$position30 = Position::delta($startFrom + $index, $model->position30);

$resPosHTML = '';
foreach (['24ч' => $position24, '7д' => $position7, '30д' => $position30] as $k => $v) {
    $resPosHTML .= ' <span class="small"> ' . $k . ' ';
    $resPosHTML .= '<span class="color-' . $colors[$sign($v)] . '"><i class="fa ' . $arrows[$sign(
            $v
        )] . '">' . (abs($v) > 0 ? abs($v) : '') . '</i></span></span>';
}
?>

<?= $this->render(
    '/author/_author_list',
    [
        'model' => $model,
        'showName' => isset($showName) ? $showName : false,
        'active' => true,
        'label' => '<h4><span class="color-dark-blue">' . ($startFrom + $index) . ' место </span><span class="pull-right"> ' . $resPosHTML . '</span></h4>'
    ]
) ?>
