<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.06.15
 * Time: 16:47
 *
 * @var $this yii\web\View
 * @var $model frontend\models\AnketaForm
 * @var $form yii\widgets\ActiveForm
 *
 **/

use yii\helpers\Html;
use \kartik\form\ActiveForm;
use \kartik\builder\Form;
use \kartik\builder\FormGrid;

?>

<div class="category-form rounded-2x">

    <?php

    $form = ActiveForm::begin(
        [
            'type' => ActiveForm::TYPE_VERTICAL,
            'enableClientValidation' => true,
            'enableClientScript' => true,
//            'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->primaryKey]
        ]
    );

    $rows = [
        [
            'contentBefore' => '<legend class="text-info"><small>1. Контактные данные</small></legend>',
            'columns' => 3,
            'autoGenerateColumns' => false,
            'attributes' => [
                'name' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'Имя']],
                'surname' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'Фамилия']],
                'middlename' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => false,
                    'options' => ['placeholder' => 'Отчество']
                ],
            ]
        ],
        [
            'columns' => 2,
            'autoGenerateColumns' => false,
            'attributes' => [
                'email' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'E-mail']],
                'phone' => ['type' => Form::INPUT_TEXT, 'label' => false, 'options' => ['placeholder' => 'Телефон']],
            ]
        ],
        [
            'columns' => 1,
            'autoGenerateColumns' => false,
            'attributes' => [
                'facebook' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => false,
                    'options' => ['placeholder' => 'Ссылка на твою страницу Facebook']
                ],
                'vk' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => false,
                    'options' => ['placeholder' => 'Ссылка на твою страницу Вконтакте']
                ],
                'youtube' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => false,
                    'options' => ['placeholder' => 'Ссылка на твой Youtube канал']
                ],
            ]
        ],
        [
            'contentBefore' => '<legend class="text-info"><small>2. Оценка экспертности</small></legend>',
            'columns' => 1,
            'autoGenerateColumns' => false,
            'attributes' => [
                'education' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Опиши свое образование (высшее, смежное или прямое)']
                ],
                'methodology' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Если есть инновационная авторская методика, то опиши ее суть']
                ],
                'experience' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Опыт приобретения практических знаний в течение 3-7 лет и больше по своей теме (портфолио, наличие квалификации)']
                ],
                'additionalEducation' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'тренинги, семинары']
                ],
                'awards' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => '...']
                ],
                'knowledgeOfCompetitors' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Что вы знаете о экспертах в своей области? Кто они? Кто лучший? Почему? ']
                ],
            ]
        ],
        [
            'contentBefore' => '<legend class="text-info"><small>3. Оценка ментальности</small></legend>',
            'columns' => 1,
            'autoGenerateColumns' => false,
            'attributes' => [
                'history' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Напиши свою историю успеха в своей нише']
                ],
                'result' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Какие изменение у тебя произошли после применения своей системы']
                ],
                'hobby' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Чем еще ты увлекаешься?']
                ],
            ]
        ],
        [
            'contentBefore' => '<legend class="text-info"><small>4. Оценка контактности</small></legend>',
            'columns' => 1,
            'autoGenerateColumns' => false,
            'attributes' => [
                'site' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Сайт или блог по твоей теме']
                ],
                'profile' => [
                    'type' => Form::INPUT_TEXTAREA,
                    'options' => ['placeholder' => 'Наличие профилей-страниц эксперта в социальных сетях']
                ],

            ]
        ],
        [
            'columns' => 2,
            'autoGenerateColumns' => false,
            'attributes' => [
                'subscribers100' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => ['более 100 подписчиков' => 'более 100 подписчиков', 'менее 100 подписчиков, или нет рассылки' => 'менее 100 подписчиков, или нет рассылки'],
                ],
                'famoust_recomendation' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => ['есть' => 'есть', 'нет' => 'нет'],
                ],
                'video_recomendation' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => ['есть' => 'есть', 'нет' => 'нет'],
                ],
                'book' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => ['есть' => 'есть', 'нет' => 'нет'],
                ],
                'video_content' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => ['есть' => 'есть', 'нет' => 'нет'],
                ],

            ]
        ]

    ];

    echo FormGrid::widget(
        [
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => $rows
        ]
    );

    echo Html::submitButton(
        'Отправить',
        ['class' => 'btn-u btn-u-blue btn-u-lg rounded']
    );

    ActiveForm::end();

    ?>

</div>