<?php
$this->title = "Академия экспертов на 5 сфер";

$this->registerMetaTag(
    [
        'name' => 'Description',
        'content' => 'Присоединяйся к «Академии Экспертов» — cовместный обучающий проект Isaac Pintosevich Systems и 5 сфер'
    ]
);

$assets = \frontend\assets\AkademiyaEkspertov\AkademiyaEkspertovAsset::register($this);
$this->registerCss(
    '
.slick-prev:before, .slick-next:before {
    font-family: "slick";
    font-size: 20px;
    line-height: 1;
    color: #333;
    opacity: 0.5;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
'
);

$this->registerCss(
    '
.jc_subblock_4 input{
    border: 1px solid rgb(204, 204, 204);
    width: 100%;
    font-family: \'Open Sans Condensed\', sans-serif;
    text-transform: uppercase;
    padding: 12px 10px;
    color: #777;
    background: #f1f1f1;
    height: 50px;
    border-radius: 0px;
    box-shadow: none;
    -webkit-box-shadow: none;
    margin-bottom: 10px;
}

.jc_subblock_4 table {
    width: 330px;
    margin-top:-20px;
}
'
);
?>
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 heroitem">
                <a class="" href="/"><img
                        src="<?= $assets->baseUrl ?>/images/academy/5sferlogo-157x41-white.png" alt="logo"
                        data-selector="img"></a>

                <h1>Стань участником <br>
                    Академии Экспертов</h1>

                <p>Эксклюзив от лидера в сфере развития — Isaac Pintosevich Systems</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

                <!-- form -->

                <div id="WFItem28" class="wf-formTpl">
                    <a name="register"></a>

                    <form
                        action="http://pay.pintosevich.com/subscribe/process/?rid[0]=5sfer_academy&doneurl=http%3A%2F%2F5sfer.com%2Fakademiya-ekspertov%2Ffinish-subscribe"
                        method="post" target="_blank" id="subscr-form-4832"
                        onsubmit="return jc_chkscrfrm(this, true, false, false, false)" class="ctaform">
                        <h3>Отправь заявку сейчас!</h3>

                        <div class="wf-box">
                            <!-- start new maillist engine -->
                            <div class="jc_subblock_4 jc_subblocktexture_0" id="subscr-block-4832">

                                <table align="center" border="0" cellspacing="0" cellpadding="3">
                                    <tbody>
                                    <tr>
                                        <td><input name="lead_name" type="text"
                                                   onblur='if(this.value=="") this.value="Имя"'
                                                   onfocus='if(this.value=="Имя") this.value=""'
                                                   value="Имя"/></td>
                                    </tr>
                                    <tr>
                                        <td><input name="lead_email" type="text"
                                                   onblur='if(this.value=="") this.value="Email"'
                                                   onfocus='if(this.value=="Email") this.value=""'
                                                   value="Email"/></td>
                                    </tr>
                                    <tr>
                                        <td><input name="lead_phone" type="text"
                                                   onblur='if(this.value=="") this.value="Телефон"'
                                                   onfocus='if(this.value=="Телефон") this.value=""'
                                                   value="Телефон"/></td>
                                    </tr>
                                    </br>
                                    <placeholder/>
                                    <tr>
                                        <td>
                                            <button type="submit" class="wf-button btn btn-default">Отправить
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <script language="JavaScript"
                                        src="http://pay.pintosevich.com/media/subscribe/helper2.js.php"></script>
                                <script language="JavaScript">jc_setfrmfld()</script>

                            </div>
                            <!-- end new maillist engine -->
                        </div>
                    </form>
                </div>
                </li>
                <li class="wf-captcha" rel="undefined" style="display: none !important;">
                    <div class="wf-contbox wf-captcha-1" id="wf-captcha-1"
                         wf-captchaword="Введите слова:"
                         wf-captchasound="Введите цифры, которые вы слышите:"
                         wf-captchaerror="Неверно, пожалуйста, попробуйте еще раз"></div>
                </li>
                <li class="wf-privacy" rel="temporary" style="display: none !important;">
                    <div class="wf-contbox">
                        <div>
                            <a class="wf-privacy wf-privacyico"
                               href="http://pintosevich.getresponse360.pl/permission-seal?lang=ru"
                               target="_blank"
                               style="height: 0px !important; display: inline !important;">Мы
                                уважаем вашу конфиденциальность<em
                                    class="clearfix clearer"></em></a>
                        </div>
                        <em class="clearfix clearer"></em>
                    </div>
                </li>
                <li class="wf-poweredby" rel="temporary" style="display: none !important;">
                    <div class="wf-contbox">
                        <div>
                    <span class="wf-poweredby wf-poweredbyico" style="display: none !important;">
                    <a class="wf-poweredbylink wf-poweredby" href="http://pintosevich.getresponse360.pl/"
                       style="display: inline !important; color: inherit !important;" target="_blank">Email
                        Marketing</a>by GetResponse</span>
                        </div>
                    </div>
                </li>
                </ul>
            </div>
            <div id="WFIfooter" class="wf-footer el"
                 style="height: 20px; display: none !important;">
                <div class="actTinyMceElBodyContent"></div>
                <em class="clearfix clearer"></em>
            </div>
        </div>
        <input type="hidden" name="webform_id" value="28"/>
        </form>
    </div>
    <script type="text/javascript"
            src="http://www.gr.pintosevich.com/view_webform.js?wid=28&mg_param1=1&u=T"></script>

    <!-- form -->

    </div>
    </div>
    </div>
    <div class="col-md-12 col-lg-12">
        <div class="scroll-arrow"><a href="#2ndscreen" title=""><i class="fa fa-arrow-down"></i></a></div>
    </div>
</header>
<section class="who">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><a name="2ndscreen"></a>

                <h2>Кому обязательно участвовать?</h2>

                <div class="subline"></div>
            </div>
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 target">
                <img src="<?= $assets->baseUrl ?>/images/academy/who02.jpg" alt="who02">
					<span class="textwrap">
						<h3>Тренеру</h3>
						<p>Ты знаешь свою тему, проводишь тренинги, хочешь стать лидером мнения и написать книгу? Теперь
                            это сделать легко! Ты получишь от нас помощь и поддержку, мы направим тебя и ты напишешь
                            свою книгу, а мы поможем ее издать.</p>
					</span>
            </article>
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 target">
                <img src="<?= $assets->baseUrl ?>/images/academy/who03.jpg" alt="who03">
						<span class="textwrap">
							<h3>Коучу</h3>
							<p>Ты работаешь в определенной нише и знаешь в ней толк? Ты уже готов подтвердить свою
                                экспертность, написать книгу и увеличить цену на свои услуги. Статьи и книга в
                                результате станут началом нового этапа твоей коучинговой практики и твоей личной
                                визитной карточкой.</p>
						</span>
            </article>
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 target">
                <img src="<?= $assets->baseUrl ?>/images/academy/who01.jpg" alt="who01">
						<span class="textwrap">
							<h3>Консультанту</h3>
							<p>Ты уже «съел собаку» в консультировании. Для тебя важно расширить свою аудиторию и
                                получить надежный источник клиентов. Узнай как помогать людям через Интернет и
                                продвигать себя как эксперта. Мы поможем трансформироватьтвой опыт в качественный,
                                готовый продукт.</p>
						</span>
            </article>
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 target">
                <img src="<?= $assets->baseUrl ?>/images/academy/who04.jpg" alt="who04">
						<span class="textwrap">
							<h3>Эксперту</h3>
							<p>У тебя есть опыт, знания, и ты - пример для других. Ты знаешь все о своей нише и тебе
                                есть что дать другим. Дело за малым – оформить это все в продукт (статьи, книги,
                                обучающие тренинги) и зарабатывать хорошие деньги, помогая другим людям стать не на одну
                                ступень выше.</p>
						</span>
            </article>
        </div>
    </div>
</section>
<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Почему я делаю этот проект?</h2>

                <div class="subline"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="<?= $assets->baseUrl ?>/images/academy/isaacp-crop.jpg" alt="team" data-selector="img">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3>Ицхак Пинтосевич</h3>

                <p class="starrating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                        class="fa fa-star"></i><i class="fa fa-star"></i></p>

                <p>ЭКСПЕРТ В СИСТЕМНОМ РАЗВИТИИ ЛИЧНОСТИ И БИЗНЕСА</p>

                <p>7 лет назад я прочитал книгу «Как стать ГУРУ за 60 дней». Я составил план и шел по нему 7 лет, никуда
                    не сворачивая. Как видишь, в книге была «небольшая неточность». Зато она помогла автору продать
                    книгу мне ))) За 60 дней невозможно стать гуру. За 60 дней можно только получить знания, составить
                    план и начать действовать. Если тебе обещают другое — то тебя просто обманывают.</p>

                <p>На данный момент я написал 12 книг и закончил 7-ми летний этап развития. Я структурировал свои знания
                    и опыт. И сейчас у тебя есть удивительная возможность получить этот опыт и опыт лучших мастеров,
                    которых я изучал. Академия Экспертов – проект, с помощью которого я собираюсь это сделать.</p>

                <p>В итоге — ты построишь прочный фундамент, на котором через 7 лет (если ты стартуешь с «0») ты станешь
                    Экспертом №1. Или раньше, если ты уже посвятил своей нише несколько лет жизни. <a href="#" title="">Присоединяйся!</a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="funfacts">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    63 000
                </div>
                <div class="funfact wow bounceInDown">
                    Прошли обучение
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    250 000
                </div>
                <div class="funfact wow bounceInDown">
                    Читателей в месяц
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    134 000
                </div>
                <div class="funfact wow bounceInDown">
                    Фанов Facebook
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="funnumber wow bounceInUp">
                    109 270
                </div>
                <div class="funfact wow bounceInDown">
                    Подписчиков рассылки
                </div>
            </div>
        </div>
    </div>
</section>
<section class="steps">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Как это работает?</h2>

                <div class="subline"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="step">
                    1
                </div>
                <h3>Учись</h3>

                <p><a href="#" class="jo_show" data-id="1082">Зарегистрируйся</a>, заполни анкету и получи доступ к
                    программе
                    обучения. Обучайся и иди к своей цели.</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="step">
                    2
                </div>
                <h3>Применяй знания</h3>

                <p>Становись автором на проекте 5 Сфер. Пиши экспертные статьи, тестируй идеи на своей аудитории,
                    становись лучшим в своей нише!</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="step">
                    3
                </div>
                <h3>Получи поддержку IPS</h3>

                <p>Получи поддержку IPS в продвижении — реклама статей на 5 сфер, публикации в соц.сетях и в рассылках
                    от Ицхака Пинтосевича!</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="step">
                    4
                </div>
                <h3>Продай свой продукт</h3>

                <p>Создай свой продукт - издай книгу, проведи свой тренинг с нашей поддержкой и зарабатывай своей
                    экспертностью.</p>
            </div>
        </div>
    </div>
</section>
<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Отзывы участников</h2>

                <div class="subline"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 carousel slide" data-ride="carousel"
                 id="carousel-testimonials">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <i class="fa fa-quote-left quote"></i>

                        <p>Мой путь в Академии Экспертов IPS начался около 2-х лет назад. В тот момент я работал ведущим
                            тренером по продажам и сервисному обслуживанию... <a
                                href="http://5sfer.com/20579-istoriya-uspekha-dena-dubravina.html" target="_blank">читать
                                полностью</a></p>

                        <p class="name">Ден Дубравин — Киев, Украина</p>
                        <img src="<?= $assets->baseUrl ?>/images/academy/den-dubravin.jpg" alt="den-dubravin" class="avatar">
                    </div>
                    <div class="item">
                        <i class="fa fa-quote-left quote"></i>

                        <p>Мне нравится как меняется моя жизнь ... Сегодня я автор и инструктор эколифтинга лица (йога
                            для лица). Простыми естественными методами я улучшила свою внешность и помогаю другим
                            женщинам... <a
                                href="http://5sfer.com/20639-kak-ya-izmenila-svoyu-zhizn-i-teper-pomogayu-drugim-zhenshchinam.html"
                                target="_blank">читать полностью</a></p>

                        <p class="name">Елена Савчук — Киев, Украина</p>
                        <img src="<?= $assets->baseUrl ?>/images/academy/elenasavchuk.jpg" alt="elenasavchuk" class="avatar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="lastcall">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>Присоединяйся к Академии Экспертов</h2>

                <div class="subline"></div>
                <p>Получи эксклюзивные условия на обучение у лидера в сфере развития — Isaac Pintosevich Systems. Найди
                    свою нишу и стань в ней лучшим. Создай свой продукт — книгу, авторскую программу или тренинг.
                    Продвигайся на ресурсах компании Isaac Pintosevich Systems. Получи известность, опыт и увеличь свой
                    доход. Стань высокооплачиванным спикером. Многое другое...</p>

                <p>И все это — ТЫ!</p>
                <a href="#" type="button" class="btn btn-default jo_show" data-id="1082">Регистрируйся сейчас</a>
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <span class="copyrights">© 2015, ВСЕ ПРАВА ЗАЩИЩЕНЫ. ISAAC PINTOSEVICH SYSTEMS.</span>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="footer-social">
                    <ul>
                        <li><a href="https://vk.com/isaac.pintosevich" target="_blank"><i class="fa fa-vk"></i></a></li>
                        <li><a href="https://twitter.com/pintosevich_7" target="_blank"><i
                                    class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/isaac.pintosevich.systems" target="_blank"><i
                                    class="fa fa-facebook"></i></a>
                        <li><a href="https://plus.google.com/u/0/105743010412667579294/posts"
                               class="rounded-x social_googleplus" target="_blank"><i
                                    class="fa fa-google-plus"></i></a></a>
                        </li>
                        <li><a href="https://www.youtube.com/user/etopobeda" class="rounded-x social_youtube"
                               target="_blank"><i
                                    class="fa fa-youtube"></i></a></a></li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php
$this->registerJs(
    '
(function(d,w){n=d.getElementsByTagName("script")[0],s=d.createElement("script"),f=function(){n.parentNode.insertBefore(s,n);};s.type="text/javascript";s.async=true;qs=document.location.search.split("+").join(" ");re=/[?&]?([^=]+)=([^&]*)/g;while(tokens=re.exec(qs))
if("email"===decodeURIComponent(tokens[1]))m=decodeURIComponent(tokens[2]);s.src="http://popupfiles.makedreamprofits.ru/55c371bcf250e-user.js";if("[object Opera]"===w.opera)d.addEventListener("DomContentLoaded",f,false);else f();})(document,window);
'
);
?>
