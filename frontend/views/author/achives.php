<?php


$this->title = 'Достижения эксперта - ' . $model->profile->name . ' - 5 сфер';
$this->registerMetaTag(
    [
        'name' => 'Description',
        'content' => 'Достижения эксперта ' . $model->profile->name . '. ' . $model->profile->bio
    ]
);


Yii::$app->opengraph->title = $model->profile->name;
Yii::$app->opengraph->description = $model->profile->bio;
Yii::$app->opengraph->image = $model->profile->avatar ? Yii::$app->params['frontendBaseUrl'] . '/user/avatar/' . $model->profile->avatar : Yii::$app->params['frontendBaseUrl'] . '/img/5sfer-1200x900.jpg';
Yii::$app->opengraph->twitter->card = 'summary';
Yii::$app->opengraph->twitter->site = '5sfer.com';
if ($this->context->getCanonical()) {
    Yii::$app->opengraph->url = $this->context->getCanonical();
}

$this->params['author'] = $model->profile->name;
?>

<?= $this->render('_author_list', ['model' => $model, 'links' => true, 'events' => true, 'h' => 'h1']) ?>

    <div class="margin-bottom-40 img-center">
        <?= \frontend\widgets\DFP::widget(
            [
                'iu' => '/59738484/author-banner',
                'sz' => [728, 90],
                't' => array_intersect_key($this->params, array_flip(['author']))
            ]
        ); ?>
    </div>

    <div class="margin-bottom-40">
        <span class="head hidden-xs"><h2>От автора</h2></span>
        <ul class="nav nav-tabs _navbar-right">
            <li class="pull-right active">
                <a href="<?= $model->urlAchives ?>"><i class="fa fa-star-o"></i> Достижения</a>
            </li>
            <?php if ($model->profile->triggerComment == 'on'): ?>
                <li class="pull-right">
                    <a href="<?= $model->urlComment ?>"><i class="fa fa-comments-o"></i> Консультации</a>
                </li>
            <?php endif ?>
            <li class="pull-right">
                <a href="<?= $model->url ?>"><i class="fa fa-file-text-o"></i> Публикации</a>
            </li>
        </ul>
    </div>

<?php foreach ($model->achives as $achive): ?>
    <div class="col-xs-12 rounded-2x margin-bottom-40" style="background: #f3f3f3;">
        <div class="row" style="padding: 1px 10px 10px 10px">
            <div class="col-xs-12" style="padding-top: 10px"><h3 style="border-bottom: 1px solid #ddd;padding: 0 0 5px;margin-bottom: 15px;"><?= \yii\helpers\Html::a(
                        $achive->achiveTitle,
                        $achive->achiveUrl,
                        ['target' => '_blank']
                    ) ?></h3></div>
            <div class="col-xs-12 margin-bottom-5">
                <p><?= $achive->achiveDescription ?></p>
            </div>
            <div class="col-xs-12" style="margin: 0px 0px 10px 0px">
                <?= \yii\helpers\Html::a(
                                    'Узнать больше',
                                    $achive->achiveUrl,
                                    ['target' => '_blank', 'class'=>'btn-u btn-u-md btn-u-blue rounded']
                                ) ?>
            </div>
        </div>
    </div>
<?php endforeach ?>

<?php

// extra data begin
$this->render('_datalayer', [
    'model' => $model
]);
// extra data end

