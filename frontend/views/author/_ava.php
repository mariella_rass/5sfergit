<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.06.15
 * Time: 17:56
 *
 *  Блок с аватарой именем и рейтингом автора
 */
?>

<div class="row<?= isset($class) ? ' ' . $class : '' ?>">
    <div class="col-sm-11 col-xs-2 margin-bottom-5 text-center">
        <?php if (isset($active) && $active && $model->achives): ?>
            <a data-toggle="tooltip" data-placement="top"
               title="Достижения: «<?= $model->achives[0]->achiveTitle ?>»<?= count(
                   $model->achives
               ) == 1 ? '' : ' и еще ' . (count($model->achives) - 1) ?>" class="author-achives dodgerblue-tooltip"
               href="<?= $model->urlAchives ?>">
                <i class="fa fa-star color-orange"></i>
            </a>
            <?php
            $this->registerJs(
                "$('[data-toggle=\"tooltip\"]').tooltip();"
            );
            ?>
        <?php endif; ?>
        <?php if (isset($active) && $active): ?>
        <a href="<?= $model->url ?>">
            <?php endif ?>
            <?= $model->profile->getImgHtml(
                'avatar',
                0,
                false,
                [
                    'class' => "img-responsive rounded-x",
                    'alt' => $model->profile->name
                ]
            ) ?>
            <?php if (isset($active) && $active): ?>
        </a>
    <?php endif ?>
    </div>
    <?php if (isset($showName) && $showName): ?>
        <div class="col-sm-11 col-xs-2 text-center margin-bottom-5">
            <div class="overflow-h">
                <?php if (isset($active) && $active): ?>
                <a class="author-name" href="<?= $model->url ?>">
                    <?php endif ?>
                    <span class="author-name">
                            <?= $model->profile->name ?>
                    </span>
                    <?php if (isset($active) && $active): ?>
                </a>
            <?php endif ?>
            </div>
        </div>
    <?php endif ?>
</div>