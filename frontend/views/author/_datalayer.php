<?php

if (isset($model)) {
    $data = [
        'author' => $model->profile->name,
    ];

    echo \frontend\widgets\DataLayer\DataLayerWidget::widget(['data' => $data]);
}