<?php


$this->title = 'Консультирует ' . $model->profile->name . ' - 5 сфер';
$this->registerMetaTag(
    [
        'name' => 'Description',
        'content' => 'Консультации эксперта ' . $model->profile->name . '. ' . $model->profile->bio
    ]
);


Yii::$app->opengraph->title = $model->profile->name;
Yii::$app->opengraph->description = $model->profile->bio;
Yii::$app->opengraph->image = $model->profile->avatar ? Yii::$app->params['frontendBaseUrl'] . '/user/avatar/' . $model->profile->avatar : Yii::$app->params['frontendBaseUrl'] . '/img/5sfer-1200x900.jpg';
Yii::$app->opengraph->twitter->card = 'summary';
Yii::$app->opengraph->twitter->site = '5sfer.com';
if ($this->context->getCanonical()) {
    Yii::$app->opengraph->url = $this->context->getCanonical();
}

$this->params['author'] = $model->profile->name;
?>

<?= $this->render('_author_list', ['model' => $model, 'links' => true, 'events' => true, 'h' => 'h1']) ?>

    <div class="margin-bottom-40 img-center">
        <?= \frontend\widgets\DFP::widget(
            [
                'iu' => '/59738484/author-banner',
                'sz' => [728, 90],
                't' => array_intersect_key($this->params, array_flip(['author']))
            ]
        ); ?>
    </div>

    <div class="margin-bottom-40">
        <span class="head hidden-xs"><h2>От автора</h2></span>
        <ul class="nav nav-tabs _navbar-right">
            <?php if ($model->achives != false): ?>
            <li class="pull-right">
                <a href="<?= $model->urlAchives ?>"><i class="fa fa-star-o"></i> Достижения</a>
            </li>
            <?php endif ?>
            <?php if ($model->profile->triggerComment == 'on'): ?>
                <li class="pull-right active">
                    <a href="<?= $model->urlComment ?>"><i class="fa fa-comments-o"></i> Консультации</a>
                </li>
            <?php endif ?>
            <li class="pull-right">
                <a href="<?= $model->url ?>"><i class="fa fa-file-text-o"></i> Публикации</a>
            </li>
        </ul>
    </div>

    <div>
        <div id="mc-container">
            <div id="mc-content">
                <ul id="cackle-comments">
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            cackle_widget = window.cackle_widget || [];
            cackle_widget.push({
                widget: 'Comment',
                id: '22299',
                url: '<?= $model->urlComment ?>'
            });
            document.getElementById('mc-container').innerHTML = '';
            (function () {
                var mc = document.createElement('script');
                mc.type = 'text/javascript';
                mc.async = true;
                mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(mc, s.nextSibling);
            })();
        </script>

    </div>
<?php

// extra data begin
$this->render('_datalayer', [
    'model' => $model
]);
// extra data end

