<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.05.15
 * Time: 15:34
 *
 * @var $model frontend\models\Post
 */
setlocale(LC_TIME, "");
setlocale(LC_TIME, 'ru_RU.UTF-8', 'Russian_Russia', 'Russian');
?>

<time class="cbp_tmtime">
    <span>
        <small>
            <i class="fa fa-eye margin-right-10"> <?= $model->getFormatedViews() ?></i>
            <i class="fa fa-share-alt"> <?= $model->postShare ?></i>
        </small>
    </span>
    <span>
        <div><?= strftime("%B, %e", $model->postPublishTime) ?>
        </div>
        <div><?= strftime(
                "%G",
                $model->postPublishTime
            ) ?>
        </div>
    </span>
</time>
<i class="cbp_tmicon rounded-x hidden-xs"></i>

<div class="cbp_tmlabel">

    <div class="row">
        <div class="col-md-12">
            <h3><a href="<?= $model->getUrl() ?>"><?= $model->postTitle ?></a></h3>

            <p><?= strip_tags($model->postLead) ?>... </p>
            <a href="<?= $model->getUrl() ?>" class="btn-u btn-u-blue rounded">Читать дальше</a>
        </div>
    </div>
</div>
