<div class="row">
    <div class="col-md-12 margin-bottom-40">
        <div class="row">
            <div class="col-lg-2 visible-lg">
                <?= $this->render(
                    '/author/_ava',
                    [
                        'model' => $model,
                        'showName' => isset($showName) ? $showName : false,
                        'active' => isset($active) ? $active : false,
                    ]
                ) ?>
            </div>
            <div class="col-md-12 col-lg-10">
                <?= isset($label) ? $label : '' ?>
                <?= isset($h) ? '<' . $h . '>' : '<h2>' ?>
                <?= isset($active) && $active ? \yii\helpers\Html::a(
                    $model->profile->name,
                    $model->url
                ) : $model->profile->name ?>
                <?= isset($h) ? '</' . $h . '>' : '</h2>' ?>

                <p><?= $model->profile->bio ?></p>
                <?php if (isset($links) && $links): ?>
                    <ul class="social-icons">

                        <?php if ($model->profile->facebook): ?>
                            <li><a href="<?= $model->profile->facebook ?>" data-original-title="Facebook"
                                   class="rounded-x social_facebook" target="_blank"></a></li>
                        <?php endif; ?>
                        <?php if ($model->profile->vk): ?>
                            <li><a href="<?= $model->profile->vk ?>" data-original-title="Vk"
                                   class="rounded-x social_vk" target="_blank"></a></li>
                        <?php endif; ?>
                        <?php if ($model->profile->twitter): ?>
                            <li><a href="<?= $model->profile->twitter ?>" data-original-title="Twitter"
                                   class="rounded-x social_twitter" target="_blank"></a></li>
                        <?php endif; ?>
                        <?php if ($model->profile->gplus): ?>
                            <li><a href="<?= $model->profile->gplus ?>"
                                   data-original-title="Goole Plus" class="rounded-x social_googleplus"
                                   target="_blank"></a>
                            </li>
                        <?php endif; ?>
                        <?php if ($model->profile->youtube): ?>
                            <li><a href="<?= $model->profile->youtube ?>" data-original-title="Youtube"
                                   class="rounded-x social_youtube" target="_blank"></a></li>
                        <?php endif; ?>
                        <?php if ($model->profile->website): ?>
                            <li><a href="<?= $model->profile->website ?>" data-original-title="Rss"
                                   class="rounded-x social_rss" target="_blank"></a></li>
                        <?php endif; ?>
                    </ul>
                <?php endif ?>
                <?php if (isset($events) && $events && count($model->activeEvents) > 0): ?>
                    <div class="margin-top-20">
                        <h3><i class="fa fa-calendar color-darkgrey"></i> Ближайшие события</h3>
                    </div>
                    <div class="event-list">
                        <ul class="list-unstyled">
                            <?php foreach ($model->activeEvents as $event): ?>
                                <li>
                                    <small><?= strftime('%d.%m', $event->eventDate) ?></small>
                                    - <a href="<?= $event->eventUrl ?>" target="_blank"> <?= $event->eventName ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>