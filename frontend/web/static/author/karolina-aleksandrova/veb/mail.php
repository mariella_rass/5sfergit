<?php

	/*
		by LetMeCode.ru
		skype: letmecode.ru
		phone: +7 (938) 500-4-700
	*/



	$mail_from	= 'support@lmc.su';					// sender's email
	$mail_fromn	= 'JAS 5.4.0';						// sender's name
	$mail_to	= 'support@lmc.su';					// to
	$mail_subj	= 'Заявка с лендинга Бесплатный воркшоп 23 марта 2016 года';				// subject
	$mail_fields = array(
		'jas_source'	=> 'Источник',
		'name'			=> 'Имя',
		'mail'			=> 'E-mail',
		'phone'			=> 'Телефон'
	);



	include('jas-5.4.0/jas.php');

	$m = '';
	foreach($_POST as $key => $value) {
		if (isset($mail_fields[$key])) {
			$m .= '<b>' . $mail_fields[$key] . '</b>: ' . $value . '<br>';
		}
	}
	if ($m == '') exit('fail');

	$utm = jas::utm_load();
	if (count($utm)) {
		$m .= '<br><b>UTM Data:</b><br>';
		foreach ($utm as $k => $v) {
			$m .= '<b>' . $k . '</b>: ' . $v . '<br>';
		}
	}

	jas::send_mail($mail_from, $mail_fromn, $mail_to, $mail_subj, $m);

	exit('done');
