<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.07.15
 * Time: 12:39
 */

namespace frontend\components;


use frontend\models\Post;
use yii\helpers\Url;
use yii\web\Controller;

class FrontendController extends Controller
{
    public $enableCsrfValidation = false;
    public $layout = 'two-column';

    public function getCanonical()
    {
        $alowParams = array_intersect_key(
            \Yii::$app->request->get(),
            array_flip(['sort', 'page', 'filter', 'term', 'slug', 'view'])
        );
        if ($this->id == 'post' && $this->action->id == 'view') {
            $model = Post::findOne(\Yii::$app->request->get('id'));
            return $model->getUrl(false);
        }
        return Url::to(array_merge([$this->id . '/' . $this->action->id], $alowParams), true);
    }

    public function getContainerClass()
    {
        return 'container-' . $this->id . ' ' . 'container-' . $this->id . '-' . $this->action->id;
    }
}