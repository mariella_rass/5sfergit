<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.07.15
 * Time: 13:18
 */

namespace frontend\components;


use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\View;
use yii\widgets\LinkPager;

class ScrollPager extends \kop\y2sp\ScrollPager
{
    public function run()
    {
        $links = $this->pagination->getLinks();
        $view = $this->getView();

        if (\Yii::$app->request->get($this->pagination->pageParam) == 1) {
            $view->context->redirect($links['self']);
        }
        if ($this->pagination->page != 0 && \Yii::$app->request->get(
                $this->pagination->pageParam
            ) != $this->pagination->page + 1
        ) {
            throw new NotFoundHttpException;
        }
        if ($this->pagination->page != 0 || \Yii::$app->request->get('sort')){
            $view->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
        }
        foreach ($links as $rel => $href) {
            $view->registerLinkTag(['rel' => $rel, 'href' => $href], $rel);
        }
        return parent::run();
    }
}