<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.06.15
 * Time: 09:36
 */

namespace frontend\controllers;


use common\models\Author;
use common\models\AuthorSearch;
use frontend\components\FrontendController;

class Top10Controller extends FrontendController
{
    public function actionIndex($slug = null)
    {
        $searchModelAuthor = new AuthorSearch();

        if ($slug == '7days') {
            $searchModelAuthor->sorter->defaultOrder = ['popularity7' => SORT_DESC];
            $type = '7';
        } else {
            $searchModelAuthor->sorter->defaultOrder = ['popularity30' => SORT_DESC];
            $type = '30';
        }

        $searchModelAuthor->query = $searchModelAuthor->query
            ->with('profile')
            ->with(
                [
                    'position1' => function ($query) use ($type) {
                        $query->addParams(
                            [
                                ':type' => $type
                            ]
                        );
                    },
                    'position7' => function ($query) use ($type) {
                        $query->addParams(
                            [
                                ':type' => $type
                            ]
                        );
                    },
                    'position30' => function ($query) use ($type) {
                        $query->addParams(
                            [
                                ':type' => $type
                            ]
                        );
                    }
                ]
            )
            ->status(Author::APPROVED)
            ->limit(10);

        $dataProviderAuthor = $searchModelAuthor->search();
        return $this->render(
            'index',
            [
                'searchModelAuthor' => $searchModelAuthor,
                'dataProviderAuthor' => $dataProviderAuthor
            ]
        );
    }
}