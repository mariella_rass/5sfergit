<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.06.15
 * Time: 16:53
 */

namespace frontend\controllers;


use frontend\components\FrontendController;
use frontend\models\SubscribeForm;
use yii\web\Cookie;

class SubscribeController extends FrontendController
{

    public function actionIndex()
    {
        $subscribed = false;
        $model = new SubscribeForm();
        $model->load(\Yii::$app->request->post());
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            // all inputs are valid
            $subscribed = true;
        } else {
            // validation failed: $errors is an array containing error messages
            $errors = $model->errors;
        }
        return $this->render('index', ['model' => $model, 'subscribed' => $subscribed]);
    }

    public function actionBonus()
    {
        $cookies = \Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'subscribed',
            'value' => '1',
            'expire' => time() + 86400 * 365 * 10,
        ]));

        return $this->render('bonus');
    }
}