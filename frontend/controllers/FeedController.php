<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.07.15
 * Time: 21:45
 */

namespace frontend\controllers;


use frontend\models\Post;
use frontend\models\PostSearch;
use frontend\components\FrontendController;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Response;

class FeedController extends FrontendController
{
    public function actionIndex()
    {
        \Yii::$app->response->format = Response::FORMAT_XML;

        $searchModel = new PostSearch();
        $searchModel->query = $searchModel->query
            ->with('categories', 'user.profile')
            ->status(Post::PUBLISH);

        $dataProvider = $searchModel->search([]);

        $response = \Yii::$app->getResponse();
        $headers = $response->getHeaders();

        $headers->set('Content-Type', 'application/rss+xml; charset=utf-8');

        $response->content = \Zelenin\yii\extensions\Rss\RssView::widget(
            [
                'dataProvider' => $dataProvider,
                'channel' => [
                    'title' => \Yii::$app->name,
                    'link' => Url::toRoute('', true),
                    'description' => 'Советы по развитию, личностному росту, построению бизнеса, продажам и здоровому образу жизни от профессиональных экспертов',
                    'language' => 'ru'
                ],
                'items' => [
                    'title' => function ($model, $widget) {
                        return $model->postTitle;
                    },
                    'description' => function ($model, $widget) {
                        return StringHelper::truncateWords(strip_tags($model->postText), 50);
                    },
                    'link' => function ($model, $widget) {
                        return \Yii::$app->urlManager->createAbsoluteUrl(
                            ['post/view', 'id' => $model->postId, 'slug' => $model->postName, 'utm_source' => 'rss', 'utm_medium'=>'article']);
                    },
                    'author' => function ($model, $widget) {
                        return $model->user->email . ' (' . $model->user->profile->name . ')';
                    },
                    'guid' => function ($model, $widget) {
                        $date = new \DateTime();
                        $date->setTimestamp($model->postPublishTime);
                        return \Yii::$app->urlManager->createAbsoluteUrl(
                                                    ['post/view', 'id' => $model->postId, 'slug' => $model->postName]);
                    },
                    'pubDate' => function ($model, $widget) {
                        $date = new \DateTime();
                        $date->setTimestamp($model->postPublishTime);
                        return $date->format(DATE_RSS);
                    }
                ]
            ]
        );
    }
}