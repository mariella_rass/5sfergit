<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.06.15
 * Time: 11:19
 */

namespace frontend\controllers;


use common\models\Author;
use frontend\components\FrontendController;
use frontend\models\Post;
use frontend\models\PostSearch;

class AuthorController extends FrontendController
{

    public function actionIndex($slug = null)
    {
        if (!$slug) {
            return $this->redirect(['top10/']);
        } else {
            $model = Author::find()->joinWith('profile')->where(['profile.slug' => $slug])->one();
            if ($model == false) {
                throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
            }

            $searchModel = new PostSearch();
            $searchModel->query = $searchModel->query
                ->with('categories', 'user.profile')
                ->status(Post::PUBLISH)
                ->author($model->primaryKey);

            $dataProvider = $searchModel->search([]);

            return $this->render(
                'view',
                [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]
            );
        }
    }

    public function actionComment($slug)
    {
        $model = Author::find()->joinWith('profile')->where(['profile.slug' => $slug])->one();
        if ($model == false || $model->profile->triggerComment == 'off') {
            throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
        }

        return $this->render(
            'comment',
            [
                'model' => $model
            ]
        );
    }

    public function actionAchives($slug)
    {
        $model = Author::find()->joinWith('profile')->where(['profile.slug' => $slug])->with('achives')->one();
        if ($model == false || !$model->achives) {
            throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
        }

        return $this->render(
            'achives',
            [
                'model' => $model
            ]
        );
    }

    public function actionFinishSubscribe($slug)
    {
        $model = Author::find()->joinWith('profile')->where(['profile.slug' => $slug])->one();
        if ($model == false) {
            throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
        }

        return $this->render(
            'finish-subscribe',
            [
                'model' => $model
            ]
        );
    }
}