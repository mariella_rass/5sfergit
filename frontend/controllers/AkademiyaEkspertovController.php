<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.06.15
 * Time: 09:36
 */

namespace frontend\controllers;


use frontend\components\FrontendController;
use frontend\models\AnketaForm;

class AkademiyaEkspertovController extends FrontendController
{
    public function actionIndex()
    {
        $this->layout = 'empty';
        return $this->render('index');
    }

    public function actionKakStatEkspertom()
    {
        return $this->render('kak-stat-ekspertom');
    }

    public function actionAnketa()
    {
        $model = new AnketaForm();
        $model->load(\Yii::$app->request->post());
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            // all inputs are valid
            \Yii::$app->mailer->compose('anketa-html', ['model' => $model])
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo(\Yii::$app->params['supportEmail'])
                ->setSubject('Анкета эксперта')
                ->send();

            \Yii::$app->mailer->compose('anketarecive-html', ['model' => $this])
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo($model->email)
                ->setBcc(['victori.fashion@gmail.com', 'michkire@gmail.com'])
                ->setSubject('Мы получили твою анкету в Академии экспертов')
                ->send();

            $this->redirect('/akademiya-ekspertov/anketa/done');
        } else {
            // validation failed: $errors is an array containing error messages
            $errors = $model->errors;
        }
        return $this->render('anketa', ['model' => $model]);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionFinishSubscribe()
    {
        return $this->render('finish-subscribe');
    }
}