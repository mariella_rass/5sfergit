<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.05.15
 * Time: 15:06
 */

namespace frontend\controllers;


use common\models\Category;
use common\models\Tag;
use common\models\ViewLog;
use frontend\components\FrontendController;
use frontend\models\Post;
use frontend\models\PostSearch;

class PostController extends FrontendController
{
    public function actionIndex($filter = null, $term = null, $sort = null)
    {
        $searchModel = new PostSearch();
        $searchModel->query = $searchModel->query
            ->with('categories', 'user.profile', 'user.achives')
            ->status(Post::PUBLISH);

        if ($filter == 'tag') {
            $term = Tag::find()->where(['tagSlug' => $term])->one();
            if ($term == false) {
                throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
            }
            $searchModel->query = $searchModel->query->tagFilter($term);
        }

        if ($filter == 'category') {
            $term = Category::find()->where(['categorySlug' => $term])->one();
            if ($term == false) {
                throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
            }
            $searchModel->query = $searchModel->query->categoryFilter($term);
        }

        // на главной в новых статьях показываем только набравшие более 500 просмотров
        if (!$filter && !$sort){
            $searchModel->query = $searchModel->query->minViews(500);
        }

        $dataProvider = $searchModel->search(
            array_merge(
                \Yii::$app->request->queryParams,
                [
                    'filter' => $filter,
                    'term' => $term,
                    'sort' => $sort
                ]
            )
        );

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'filter' => $filter,
                'term' => $term,
                'sort' => $sort
            ]
        );
    }

    public function actionView($id, $slug = null)
    {
        $post = Post::findOne($id);
        if ($post != false) {
            if ($post->postName != $slug || !empty($post->postCanonical)) {
                return $this->redirect($post->getUrl(), 301);
            }
            return $this->render(
                'view',
                [
                    'post' => $post
                ]
            );
        } else {
            throw new \yii\web\HttpException(404, 'Ой! Нет такой страницы!');
        }
    }

    public function actionCategory($term = '', $order = null)
    {
        //$posts = Post::last();
        return $this->render(
            'index',
            [
                //'posts' => $posts
            ]
        );
    }

    public function actionStat()
    {
        $headers = \Yii::$app->response->headers;
        $headers->set('X-Accel-Expires', 0);

        $t = \Yii::$app->request->post('t');

        $post = Post::findOne($postId = \Yii::$app->request->get('slug'));

        if (!$post) {
            \Yii::$app->end(200);
        }

        switch (\Yii::$app->request->post('t')) {
            case 'v':
                ViewLog::log($post);
                break;
            case 's':
                if ($count = \Yii::$app->request->post('count', null)) {
                    $post->postShare = $count;
                    $post->save(true, ['postShare']);
                }
                break;
        }

        echo json_encode(
            [
                'v' => $post->postRawViews,
                's' => $post->postShare
            ]
        );

        \Yii::$app->end(200);
    }

    // old point
    public function actionUpdate()
    {
        $postId = \Yii::$app->request->get('slug');

        if ($postId) {
            \Yii::$app->response->redirect(
                \yii\helpers\Url::to(['post/stat', 'slug' => $postId]),
                301
            );
        }
    }
}