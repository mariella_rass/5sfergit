<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.08.15
 * Time: 19:17
 */

namespace frontend\controllers;


use common\models\EventSearch;
use frontend\components\FrontendController;

class EventsController extends FrontendController
{
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $searchModel->query = $searchModel->query
            ->with('user.profile')
            ->andWhere(['eventStatus' => 'on'])
            ->andWhere(['>', 'eventDate', time()])
            ->orderBy(['eventDate' => SORT_ASC]);

        $dataProvider = $searchModel->search(
            [
                ['eventStatus' => 'on'],
            ]
        );

        return $this->render(
            'index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]
        );
    }

    public function actionFinishSubscribe()
    {
        return $this->render('finish-subscribe');
    }
}