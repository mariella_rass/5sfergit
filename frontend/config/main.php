<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'cookieValidationKey' => 'Cap3kxZgtV3MftzTnKZtUjtj3UzVyNfR',
        ],
        'urlManager' => [
            'hostInfo' => 'http://5sfer.bigdig.com.ua',
            'rules' => [
                [
                    'class' => \common\components\PostListUrlRule::className()
                ],
                [
                    'pattern' => '<view:1step|life-calendar|bonus|bonus-alt|live-subscribed|about|politika-konfidencialnosti|kak-izvlech-maksimum-polzy-iz-prochitannyx-materialov>',
                    'route' => 'site/view',
                ],
                [
                    'pattern' => '<id:\d+>-<slug:.*>',
                    'route' => 'post/view',
                    'suffix' => '.html',
                ],
                [
                    'pattern' => '<id:\d+>',
                    'route' => 'post/view',
                ],
                [
                    'class' => \common\components\UniversalUrlRule::className()
                ],
                ['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml']
            ],

        ],
        'opengraph' => [
            'class' => 'dragonjet\opengraph\OpenGraph',
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            // following line will restrict access to admin page
            'as frontend' => 'dektrium\user\filters\FrontendFilter',
            'modelMap' => [
                'User' => 'common\models\User',
            ],
        ],
        'sitemap-category' => [
            'class' => \himiklab\sitemap\Sitemap::className(),
            'models' => [
                // your models
                //\common\models\Post::className(),
                //\common\models\Profile::className(),
                \common\models\Category::className(),
            ],
            //'enableGzip' => true, // default is false
            'cacheExpire' => 24 * 60 * 60, // 1 second. Default is 24 hours
        ],
        'sitemap-post' => [
            'class' => \himiklab\sitemap\Sitemap::className(),
            'models' => [
                // your models
                \common\models\Post::className(),
                //\common\models\Profile::className(),
                //\common\models\Category::className(),
            ],
            //'enableGzip' => true, // default is false
            'cacheExpire' => 24 * 60 * 60, // 1 second. Default is 24 hours
        ]
    ],
    'defaultRoute' => 'post/index',
    'params' => $params,
    'language' => 'ru_RU'
];
