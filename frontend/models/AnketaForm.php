<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.06.15
 * Time: 17:18
 */

namespace frontend\models;

use yii\base\Model;

class AnketaForm extends Model
{
    // Контактные данные
    public $name;
    public $middlename;
    public $surname;
    public $email;
    public $phone;
    public $facebook;
    public $vk;
    public $youtube;
    // Оценка экспертности
    public $education;              // Опиши свое образование (высшее, смежное или прямое):
    public $methodology;            // Если есть инновационная авторская методика, то опиши ее суть. :
    public $experience;             // Опыт приобретения практических знаний в течение 3-7 лет и больше по своей теме (портфолио, наличие квалификации):
    public $additionalEducation;    // Дополнительное образование (тренинги, семинары):
    public $awards;                 // Награды, ученые степени
    public $knowledgeOfCompetitors; // Знание конкурентов и лучших в своей категории
    // Оценка ментальности
    public $history;                // Напиши свою историю успеха в своей нише
    public $result;                 // Следование собственной системе. Какие изменение у тебя произошли после применения
    public $hobby;                  // Хобби и увлечения
    // Оценка контактности
    public $site;                   // Сайт или блог по твоей теме
    public $subscribers100;         // Рассылка. Количество подписчиков в твоей базе (наличие базы клиентов более 100 человек)
    public $profile;                // Наличие профилей-страниц эксперта в социальных сетях
    public $famoust_recomendation;  // Наличие отзывов известных личностей
    public $video_recomendation;    // Наличие видео-отзывов клиентов:
    public $book;                   // Книга
    public $video_content;          // Видео контент (личные материалы по теме)

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'middlename',
                    'surname',
                    'email',
                    'phone',
                    'subscribers100',
                    'famoust_recomendation',
                    'video_recomendation',
                    'book',
                    'video_content'
                ],
                'required'
            ],
            [
                [
                    'facebook',
                    'vk',
                    'youtube',
                    'education',
                    'education',
                    'methodology',
                    'experience',
                    'additionalEducation',
                    'awards',
                    'knowledgeOfCompetitors',
                    'history',
                    'result',
                    'hobby',
                    'site',
                    'profile'
                ],
                'safe'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            // 1
            'name' => 'Имя',
            'middlename' => 'Отчество',
            'surname' => 'Фамилия',
            'email' => 'Е-mail',
            'phone' => 'Телефон',
            'facebook' => 'Facebook',
            'vk' => 'Вконтакте',
            'youtube' => 'Youtube',
            // 2
            'education' => 'Образование',
            'methodology' => 'Авторская методика',
            'experience' => 'Опыт',
            'additionalEducation' => 'Дополнительное образование',
            'awards' => 'Награды, ученые степени',
            'knowledgeOfCompetitors' => 'Знание конкурентов',
            // 3
            'history' => 'Твоя история успеха',
            'result' => 'Собственные результаты',
            'hobby' => 'Хобби и увлечения',
            // 4
            'site' => 'Сайт',
            'subscribers100' => 'Твоя рассылка',
            'profile' => 'Профили эксперта',
            'famoust_recomendation' => 'Отзывы известных личностей',
            'video_recomendation' => 'Видео-отзывы',
            'book' => 'Книга',
            'video_content' => 'Свой видео контент',
        ];
    }
}