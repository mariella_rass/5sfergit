<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.06.15
 * Time: 17:18
 */

namespace frontend\models;

use yii\base\Model;

class SubscribeForm extends Model
{
    // Контактные данные
    public $name;
    public $email;

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'email',
                ],
                'required'
            ],
            [
                ['email'],
                'email'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Е-mail',
        ];
    }
}