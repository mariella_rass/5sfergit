<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.05.15
 * Time: 19:12
 */

namespace frontend\models;

use common\models\base\ViewLog;
use common\models\View;
use yii\helpers\Url;

class Post extends \common\models\Post
{
    public function registerView()
    {
        $this->updateCounters(['postViews' => 1]);
    }

    public function registerRawView()
    {
        $this->updateCounters(['postRawViews' => 1]);
    }

    public function getFormatedViews()
    {
        return $this->postRawViews;
        //return number_format($this->postViews, 0, ',', '.');
    }

    public function getFormatedShares()
    {
        return $this->postShare;
        //return number_format($this->postShare, 0, ',', '.');
    }

    public function getMedia()
    {
        if (!empty($this->postVideo)) {
            return \yii\helpers\Html::tag(
                'div',
                $this->postVideo,
                [
                    'class' => 'videoWrapper'
                ]
            );
        }
        return $this->getImage();
    }

    public function getImage()
    {
       $path = str_replace( "postImage/", "postImage/thumb-" , $this->getImgSrc('postImage')) ;
       if(!(@getimagesize($path))){
            $path = $model->getImgSrc('postImage');
        }
        return \yii\helpers\Html::img(
            $path,
            // $this->getImgSrc('postImage'),
            [
                'class' => 'img-responsive',
                'alt' => $this->postTitle
            ]
        );
    }

    public function getUrl($enableAchor = false)
    {
        $data = empty($this->postCanonical) ? [
            'post/view',
            'id' => $this->primaryKey,
            'slug' => $this->postName
        ] : $this->postCanonical;
        return
            Url::to($data, true);
    }

    public function getPre2Raw()
    {
        return preg_replace_callback(
            '#<pre(?:.*?)>(.*?)</pre>#si',
            function ($matches) {
                return html_entity_decode($matches[1]);
            },
            $this->postText
        );
    }

    public function containCodeBlock()
    {
        return preg_match('#<pre(?:.*?)>(.*?)</pre>#si', $this->postText);
    }
}