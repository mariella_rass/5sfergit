<?php

namespace frontend\models;

use Yii;

/**
 * PostSearch represents the model behind the search form about `backend\models\Post`.
 */
class PostSearch extends \common\models\PostSearch
{
    public function init()
    {
        parent::init();
        $this->sorter = $this->getSorter();
    }

    private function getSorter()
    {
        $sorter = new \yii\data\Sort(
            [
                'defaultOrder' => ['date' => SORT_DESC],
                'attributes' => [
                    'date' => [
                        'asc' => ['postPublishTime' => SORT_DESC],
                        'desc' => ['postPublishTime' => SORT_DESC],
                        'default' => SORT_ASC,
                        'label' => 'Самое новое',
                    ],
                    'popular' => [
                        'asc' => ['postPopularity' => SORT_DESC],
                        'desc' => ['postPopularity' => SORT_DESC],
                        'default' => SORT_ASC,
                        'label' => 'Самое популярное',
                    ],
                ],
            ]
        );
        return $sorter;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}
