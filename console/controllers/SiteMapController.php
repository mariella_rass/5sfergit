<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.06.15
 * Time: 11:22
 */

namespace console\controllers;

use common\models\sitemaps\AuthorSitemapEntity;
use common\models\sitemaps\CategorySitemapEntity;
use common\models\sitemaps\PostSitemapEntity;
use Yii;
use yii\console\Controller;

class SiteMapController extends Controller
{
    public function actionCreateSitemap()
    {
        date_default_timezone_set('Europe/Kiev');
        \Yii::$app->sitemap
            ->addModel(CategorySitemapEntity::className(), 'category')
            ->addModel(PostSitemapEntity::className(), 'post')
            ->addModel(AuthorSitemapEntity::className(), 'author')
            ->create();
    }
}