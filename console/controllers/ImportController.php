<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.05.15
 * Time: 13:07
 */

namespace console\controllers;


use common\models\Author;
use common\models\base\Position;
use common\models\base\View;
use common\models\Category;
use common\models\Tag;
use common\models\ViewLog;
use dektrium\user\models\Profile;
use frontend\models\Post;
use common\models\Term;
use dektrium\user\models\User;
use Yii;


/**
 * Class ImportController
 * @package console\controllers
 *
 * @property \yii\db\Connection importdb
 */
class ImportController extends \yii\console\controller
{
    const VIEW_MULTIPLIER = 10;
    const INFLATION_COEFFICIENT = 0.9;
    protected $importdb;

    public function init()
    {
        $this->initialize();
        return parent::init();
    }

    private function initialize()
    {
        $this->importdb = new \yii\db\Connection(
            [
                'dsn' => 'mysql:host=77.87.199.155;dbname=5sferC',
                'username' => 'u_5sferC0',
                'password' => '3sLSb5yg',
                'charset' => 'utf8',
            ]
        );
    }

    public function actionStart()
    {

        $this->clear();
        $this->importUser();
        $this->importPosts();
//        $this->importTerms();
//        $this->importPostImage();
//        $this->importUserImage();
//        $this->importViews();
//        $this->updatePostView();
//        $this->popularityUpdate();
//        $this->userPositionUpdate();
//        $this->run('cron/daily');
//        $this->updateShare();
    }

    private function updateShare()
    {
        echo 'Start update share... ';
        foreach (Post::find()->andWhere(['postShare' => 0])->each() as $post) {
            $share = 0;
            $url = $post->getUrl();
            $data = json_decode(@file_get_contents('https://graph.facebook.com/?id=' . $url));
            if (isset($data->shares)) {
                $share += $data->shares;
            }
            $data = json_decode(@file_get_contents('https://cdn.api.twitter.com/1/urls/count.json?url=' . $url));
            if (isset($data->count)) {
                $share += $data->count;
            }
            $data = @file_get_contents('http://vk.com/share.php?act=count&index=1&url=' . $url);
            if (preg_match(
                '%,\s(?P<count>\d+)%',
                $data,
                $matches
            )) {
                $share += $matches['count'];
            }
            $post->postShare = $share;
            if (!$post->save(false)) {
                print_r($post->errors);
                die();
            };
            $post = Post::findOne($post->primaryKey);
            echo 'PostId ' . $post->primaryKey . ' share ' . $post->postShare . PHP_EOL;
        }
        echo 'finish' . PHP_EOL;
    }

    private function clear()
    {
//        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        Position::deleteAll();
        ViewLog::deleteAll();
        View::deleteAll();
        Post::deleteAll();
        Profile::deleteAll();
        User::deleteAll();
        Tag::deleteAll();
        Category::deleteAll();
//        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    private function importTerms()
    {
        echo 'Start import terms... ';
        $oldTerms = $this->importdb->createCommand(
            "
                SELECT
                    taxonomy,
                    olb1qs4xdr_terms.term_id,
                    olb1qs4xdr_terms.name,
                    olb1qs4xdr_terms.slug,
                    olb1qs4xdr_term_taxonomy.description
                FROM
                    olb1qs4xdr_term_taxonomy
                        LEFT JOIN
                    olb1qs4xdr_terms ON olb1qs4xdr_terms.term_id = olb1qs4xdr_term_taxonomy.term_id
                WHERE
                    taxonomy IN ('category' , 'post_tag')
                        AND olb1qs4xdr_terms.term_id != 1;
            "
        )->queryAll();
        $colors = [
            'Живи!' => 16159001,
            'Действуй!' => 33464,
            'Богатей!' => 9054863,
            'Влияй!' => 12462376,
            'Люби!' => 3979329
        ];
        foreach ($oldTerms as $oldTerm) {
            $termsType = $oldTerm['taxonomy'] == 'category' ? 'category' : 'tag';
            if ($termsType == 'category') {
                $term = new Category();
                $term->categoryName = $oldTerm['name'];
                if (isset($colors[$term->categoryName])) {
                    $term->categoryColor = $colors[$term->categoryName];
                    $term->categoryEditable = 'false';
                }
                $term->categoryId = $oldTerm['term_id'];
                $term->categorySlug = $oldTerm['slug'];
                $term->categoryTitle = $oldTerm['name'];
                $term->categoryDescription = $oldTerm['description'] ? $oldTerm['description'] : 'Статьи в категории ' . $oldTerm['name'];

            } else {
                $term = new Tag();
                $term->tagId = $oldTerm['term_id'];
                $term->tagName = $oldTerm['name'];
                $term->tagSlug = $oldTerm['slug'];
                $term->tagTitle = $oldTerm['name'];
                $term->tagDescription = $oldTerm['description'] ? $oldTerm['description'] : 'Статьи по тэгу ' . $oldTerm['name'];

            }

            if (!$term->save()) {
                print_r($term->errors);
            }

            $relIds = $this->importdb->createCommand(
                "
                    SELECT
                        tr.object_id
                    FROM
                        olb1qs4xdr_term_relationships AS tr
                            LEFT JOIN
                        olb1qs4xdr_term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
                    WHERE
                        tt.term_id = " . $oldTerm['term_id']
            )->queryColumn();

            $relPosts = \common\models\Post::findAll($relIds);
            foreach ($relPosts as $relPost) {
                $relPost->link($termsType == 'category' ? 'categories' : 'tags', $term);
            }

        }
        echo 'finish' . PHP_EOL;
    }

    private function importPosts()
    {
        echo 'Start import posts... ';
        $oldPosts = $this->importdb->createCommand(
            "
                select
                    ID, post_author, post_date,post_content,post_title,post_name,post_date,post_modified,post_status
                from
                    olb1qs4xdr_posts as op
                where
                    post_type = 'post'
                        and post_status in ('publish','future')
        "
        )->queryAll();
        foreach ($oldPosts as $oldPost) {
            $post = new Post();
            $post->postId = $oldPost['ID'];
            $post->postTitle = $oldPost['post_title'];
            $post->postName = $oldPost['post_name'];
            $post->postStatus = ['publish' => Post::PUBLISH, 'future' => Post::FUTURE][$oldPost['post_status']];
            echo strtotime($oldPost['post_date']) . ' ' . $oldPost['post_date'] . PHP_EOL;
            $post->postPublishTime = strtotime($oldPost['post_date']);
            if ($post->postPublishTime > time() && $post->postStatus == Post::PUBLISH) {
                $post->postStatus = Post::FUTURE;
            }
            $post->postUpdateTime = strtotime($oldPost['post_modified']);
            $text = preg_replace_callback('%<iframe[^>]+>.*<\s?\/iframe\s?>%mi', function($m){
                return htmlentities('<div class="videoWrapper">'.$m[0] .'</div>');
            }, $oldPost['post_content']);
            $text = \yii\helpers\HtmlPurifier::process(
                $text,
                [
                    'AutoFormat.AutoParagraph' => true,
                    'HTML.Allowed' => 'p,b,a[href|target],i,em,strong,li,ul,img[src],h1,h2,h3,h4,h5',
                ]
            );
            $text = html_entity_decode($text);
            //$text = '<p>' . implode('</p><p>', explode("\n", $oldPost['post_content'])) . '</p>';
            $text = str_replace("\xC2\xA0", " ", $text);
            $text = str_replace('<img ', '<img class="img-responsive" ', $text);
            $text = str_replace(["\n", "\r"], ' ', $text);
            $post->postText = $text;
            preg_match('/([\p{L}\d]+[^\p{L}\d]*){1,30}/mu', strip_tags($text), $lead);
            $post->postLead = trim($lead[0]);
            $user = User::findOne($oldPost['post_author']);
            if ($user) {
                $post->link('user', $user);
            }
            if (!$post->save(false)) {
                print_r($post->errors);
            };
        }
        $oldVideos = $this->importdb->createCommand(
            "select
            post_id, meta_value
        from
            olb1qs4xdr_postmeta
        where
            meta_key = '_bucket_video_embed'"
        )->queryAll();
        foreach ($oldVideos as $oldVideo) {
            $post = Post::findOne($oldVideo['post_id']);
            if ($post) {
                $post->postVideo = html_entity_decode($oldVideo['meta_value']);
                $post->save(false);
            }
        }
        echo 'finish' . PHP_EOL;
    }

    private function userPositionUpdate()
    {
        echo 'Start user position upadate... ';
        foreach (['7', '30'] as $type) {
            for ($day = 1; $day <= 30; $day++) {
                echo 'type ' . $type . ' day ' . $day . PHP_EOL;
                Yii::$app->db->createCommand('set @a = 0')->execute();
                $rows = Yii::$app->db->createCommand(
                    "
                        SELECT
                            DATE_SUB(CURDATE(), INTERVAL $day DAY) AS date,
                            profile.user_id,
                            @a:=@a + 1 AS position,
                            (SELECT
                                    SUM((SELECT
                                                SUM(count)
                                            FROM
                                                view
                                            WHERE
                                                view.postId = post.postId
                                                    AND date < DATE_SUB(CURDATE(), INTERVAL $day DAY)
                                                    AND date >= DATE_SUB(DATE_SUB(CURDATE(), INTERVAL $day DAY),
                                                    INTERVAL $type DAY))) AS views
                                FROM
                                    post
                                WHERE
                                    post.userId = profile.user_id) AS popularity
                        FROM
                            profile
                        WHERE
                            profile.status = 'approved'
                        ORDER BY popularity DESC
                    "
                )->queryAll();
                foreach ($rows as $row) {
                    Yii::$app->db->createCommand(
                        "
                    REPLACE `position`(date,user_id,type,position) VALUE(
                        '{$row['date']}',
                        '{$row['user_id']}',
                        '{$type}',
                        '{$row['position']}'
                    )
                    "
                    )->execute();
                }

            }
        }
        echo 'finish' . PHP_EOL;
    }

    private function importPostImage()
    {
        echo 'Start import post image... ';
        $oldImages = $this->importdb->createCommand(
            "
                select
                    post_parent, guid
                from
                    olb1qs4xdr_posts
                where
                    post_type = 'attachment'
        "
        )->queryAll();
        foreach ($oldImages as $oldImage) {
            $newPath = Yii::getAlias('@media') . str_replace('http://5sfer.com', '', $oldImage['guid']);
            $path = implode(
                DIRECTORY_SEPARATOR,
                array_slice(explode(DIRECTORY_SEPARATOR, $newPath), 0, -1)
            );
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            if (!file_exists($newPath)) {
                file_put_contents($newPath, @file_get_contents($oldImage['guid']));

                if (file_exists($newPath) && filesize($newPath) > 0) {
                    echo shell_exec('convert ' . $newPath . ' -resize \'720>\' ' . $newPath);
                    if (exif_imagetype($newPath) == IMAGETYPE_JPEG) {
                        echo shell_exec('jpegoptim -p -m80 --strip-all ' . $newPath);
                    }
                    if (exif_imagetype($newPath) == IMAGETYPE_PNG) {
                        echo shell_exec('optipng ' . $newPath);
                    }
                } else {
                    unlink($newPath);
                }
            }
        }

        $oldThumbs = $this->importdb->createCommand(
            "
                select
                    post_id, guid
                from
                    olb1qs4xdr_postmeta
                        left join
                    olb1qs4xdr_posts ON olb1qs4xdr_postmeta.meta_value = olb1qs4xdr_posts.ID
                where
                    meta_key = '_thumbnail_id'
        "
        )->queryAll();
        if (!is_dir(Yii::getAlias('@media') . '/post')) {
            mkdir(Yii::getAlias('@media') . '/post', 0755, true);
        }
        foreach ($oldThumbs as $oldThumb) {
            $postId = $oldThumb['post_id'];
            $path = Yii::getAlias('@media') . str_replace('http://5sfer.com', '', $oldThumb['guid']);
            echo shell_exec(
                'cp ' . $path . ' ' . Yii::getAlias('@media') . '/post/' . $postId . '.jpg'
            );
            $post = Post::findOne($postId);
            if ($post) {
                $post->postImage = $postId . '.jpg';
                $post->save(false);
            }
        }
        echo 'finish' . PHP_EOL;
    }

    private function importUser()
    {
        echo 'Start import user... ';

        $this->run('rbac/init');

        $auth = Yii::$app->authManager;

        $oldUsers = $this->importdb->createCommand(
            "
                      select * from olb1qs4xdr_users
                    "
        )->queryAll();
        foreach ($oldUsers as $oldUser) {
            $user = new User();
            $user->id = $oldUser['ID'];
            $user->username = $oldUser['user_login'];
            $user->password_hash = $oldUser['user_pass'];
            $user->email = $oldUser['user_email'];
            $user->created_at = strtotime($oldUser['user_registered']);
            $user->updated_at = time();
            $user->confirmed_at = time();
            if ($user->id == 1) {
                $user->username = 'admin';
                $user->email = 'd.m@bigmir.net';
                $user->password_hash = '$2y$10$Ep6yiAg4FIhY3LKNvEJ3eulthHOVyccBRg0hcPeW2y3GN9GcsiOVy';
            }
            $user->save(false);
            if ($user->id == 1) {
                $auth->assign($auth->getRole('admin'), $user->primaryKey);
            } else {
                $auth->assign($auth->getRole('author'), $user->primaryKey);
            }

            $profile = $user->profile;

            $profile->name = $oldUser['display_name'];
            $profile->slug = $oldUser['user_nicename'];
            $profile->status = !in_array($oldUser['ID'], [1, 77]) ? Author::APPROVED : Author::STAFF;

            $profile->bio = $this->importdb->createCommand(
                "select meta_value from olb1qs4xdr_usermeta where user_id = {$user->id} and meta_key = 'description'"
            )->queryScalar();

            $facebook = $this->importdb->createCommand(
                "select IFNULL(meta_value,'') from olb1qs4xdr_usermeta where user_id = {$user->id} and meta_key = 'facebook'"
            )->queryScalar();

            $profile->facebook = $facebook ? $facebook : $profile->facebook;

            $twitter = $this->importdb->createCommand(
                "select IFNULL(meta_value,'') from olb1qs4xdr_usermeta where user_id = {$user->id} and meta_key = 'twitter'"
            )->queryScalar();

            $profile->twitter = $twitter ? $twitter : $profile->twitter;

            $youtube = $this->importdb->createCommand(
                "select IFNULL(meta_value,'') from olb1qs4xdr_usermeta where user_id = {$user->id} and meta_key = 'youtube'"
            )->queryScalar();

            $profile->youtube = $youtube ? $youtube : $profile->youtube;

            $vk = $this->importdb->createCommand(
                "select IFNULL(meta_value,'') from olb1qs4xdr_usermeta where user_id = {$user->id} and meta_key = 'vk'"
            )->queryScalar();

            $profile->vk = $vk ? $vk : $profile->vk;

            echo $profile->name . PHP_EOL;
            //$user->link('profile', $profile);
            if (!$profile->save()) {
                print_r($profile->errors);
                die();
            };
        }
        echo 'finish' . PHP_EOL;
    }

    private function importUserImage()
    {
        //olb1qs4xdr_user_avatar
        $oldImages = $this->importdb->createCommand(
            "
                select
                    user_id,
                    (select
                            guid
                        from
                            olb1qs4xdr_posts
                        where
                            olb1qs4xdr_posts.id = meta_value) guid
                from
                    olb1qs4xdr_usermeta
                where
                    meta_key = 'olb1qs4xdr_user_avatar'
                HAVING guid is not null
        "
        )->queryAll();
        if (!is_dir(Yii::getAlias('@media') . '/user/avatar')) {
            mkdir(Yii::getAlias('@media') . '/user/avatar', 0755, true);
        }
        foreach ($oldImages as $oldImage) {
            $newPath = Yii::getAlias('@media') . str_replace('http://5sfer.com', '', $oldImage['guid']);
            $path = implode(
                DIRECTORY_SEPARATOR,
                array_slice(explode(DIRECTORY_SEPARATOR, $newPath), 0, -1)
            );
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            if (!file_exists($newPath)) {
                file_put_contents($newPath, @file_get_contents($oldImage['guid']));
            }
            if (file_exists($newPath) && filesize($newPath) > 0) {
                echo shell_exec('convert ' . $newPath . ' -resize \'720>\' ' . $newPath);
                if (exif_imagetype($newPath) == IMAGETYPE_JPEG) {
                    echo shell_exec('jpegoptim -p -m80 --strip-all ' . $newPath);
                }
                if (exif_imagetype($newPath) == IMAGETYPE_PNG) {
                    echo shell_exec('optipng ' . $newPath);
                }
                $avatarPath = Yii::getAlias('@media') . '/user/avatar/' . $oldImage['user_id'] . '.jpg';
                echo shell_exec(
                    'convert ' . $newPath . ' -resize \'100x100^\' -gravity center -extent \'100x100\' ' . $avatarPath
                );
                echo shell_exec('jpegoptim -p -m80 --strip-all ' . $avatarPath);
                $user = User::findOne($oldImage['user_id']);
                $user->profile->avatar = $oldImage['user_id'] . '.jpg';
                $user->profile->save(false);
            } else {
                unlink($newPath);
            }
        }
    }

    private function importViews()
    {
        echo 'Start import views... ';
        $rows = (new \yii\db\Query())
            ->select(['date', 'post_id', 'count'])
            ->from('olb1qs4xdr_activity')
            ->where(['type' => 'view']);
        //->andWhere('date > DATE_SUB(NOW(), INTERVAL 90 DAY)');

        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();

        foreach ($rows->batch(1000, $this->importdb) as $data) {
            Yii::$app->db->createCommand()->batchInsert('view', ['date', 'postId', 'count'], $data)->execute();
        }

        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();

        echo 'finish' . PHP_EOL;
    }

    private function updatePostView()
    {
        echo 'Start update views... ';
        \Yii::$app->db->createCommand(
            '
            update post
            set
                postViews = (select
                        sum(count)
                    from
                        view
                    where
                        view.postId = post.postId)
            '
        )->execute();
        echo 'finish' . PHP_EOL;
    }

    private function popularityUpdate()
    {
        echo 'Start update popularity... ';
        $datetime1 = time();
        foreach (Post::find()->each() as $post) {
            /**
             * @var $post \frontend\models\Post
             */
            $views = \Yii::$app->db->createCommand(
                "
                    select
                        *
                    from
                        view
                    WHERE
                      postId = :postId
                        "
            )->bindValue(':postId', $post->postId)->queryAll();
            $popularity = 0;
            foreach ($views as $view) {
                $datetime2 = strtotime($view['date']);
                $dif = floor(($datetime1 - $datetime2) / 86400);
                $popularity = $popularity + ($view['count'] * self::VIEW_MULTIPLIER * (pow(
                            self::INFLATION_COEFFICIENT,
                            $dif
                        )));
            }
            $post->postPopularity = round($popularity);
            if (!$post->save(false)) {
                print_r($post->errors);
            }
        }
        echo 'finish' . PHP_EOL;
    }
}