<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.05.15
 * Time: 10:34
 */

namespace console\controllers;


use frontend\models\Post;
use yii\console\Controller;
use yii\helpers\Url;

class DailyController extends Controller
{
    const INFLATION_COEFFICIENT = 0.9;
    const VIEW_MULTIPLIER = 10;

    public function actionUpdatePostView()
    {
        \Yii::$app->db->createCommand(
            '
        update post
        set
            postViews = (select
                    sum(count)
                from
                    view
                where
                    view.postId = post.postId)
        '
        )->execute();
    }

    public function actionUpdatePostPopularity($clear = false)
    {
        \Yii::$app->db->createCommand(
            '
        update post
        set
            postPopularity = postPopularity * ' . self::INFLATION_COEFFICIENT . ' + (select
                    count * ' . self::VIEW_MULTIPLIER . '
                from
                    view
                where
                    view.postId = post.postId
                        and date = DATE_SUB(CURDATE(), INTERVAL 1 DAY));
                        '
        )->execute();
    }

}