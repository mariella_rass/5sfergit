<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.05.15
 * Time: 10:34
 */

namespace console\controllers;

use common\models\Post;
use common\models\View;
use common\models\ViewLog;
use yii\console\Controller;

class CronController extends Controller
{

    public function actionMinute()
    {

        ob_start();

        $this->publishFuture();

        $output = ob_get_clean();

        if ($output) {
            \Yii::$app->mailer->compose()
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo('michkire@gmail.com')
                ->setSubject('future publish ')
                ->setTextBody($output)
                ->send();
        }

        echo $output;
    }

    public function actionDaily()
    {
        echo "daily cron start...";
        $this->processViewLog();
        $this->clearOldView();
        $this->updatePostPopularity();
        $this->updateUserPopularity();
        $this->updateUserPosition();
        echo "daily cron complete" . PHP_EOL;
    }

    private function publishFuture()
    {
        $future = Post::find()->status(Post::FUTURE)->andWhere('postPublishTime < UNIX_TIMESTAMP()')->all();
        foreach ($future as $post) {
            $post->postStatus = Post::PUBLISH;
            if ($post->save()) {
                echo 'Publish ' . $post->primaryKey . PHP_EOL;
            } else {
                echo 'NOT Publish ' . $post->primaryKey . PHP_EOL;
                foreach ($post->errors as $key => $error) {
                    echo "\t" . $key . " => " . $error[0] . PHP_EOL;
                };
                echo PHP_EOL;
            }
        }
    }

    const INFLATION_COEFFICIENT = '0.9';
    const VIEW_MULTIPLIER = 10;

    private function updatePostPopularity()
    {
        \Yii::$app->db->createCommand(
            '
        update post
        set
            postPopularity = postPopularity * ' . self::INFLATION_COEFFICIENT . ' + (select
                    count * ' . self::VIEW_MULTIPLIER . '
                from
                    view
                where
                    view.postId = post.postId
                        and date = DATE_SUB(CURDATE(), INTERVAL 1 DAY));
                        '
        )->execute();
    }

    private function updateUserPopularity()
    {
        foreach ([1, 7, 30] as $interval) {
            \Yii::$app->db->createCommand(
                "
            update profile
            set
                popularity{$interval} = ifnull((select
                        sum((select
                                    sum(count)
                                from
                                    view
                                where
                                    view.postId = post.postId
                                        and date < CURDATE()
                                        and date >= DATE_SUB(CURDATE(), INTERVAL {$interval} DAY))) as views
                    from
                        post
                    where
                        post.userId = profile.user_id), 0);
            "
            )->execute();
        }
    }

    private function updateUserPosition($shift = 30)
    {
        foreach (['7', '30'] as $type) {
            for ($day = 1; $day <= $shift; $day++) {
                \Yii::$app->db->createCommand('set @a = 0')->execute();
                $rows = \Yii::$app->db->createCommand(
                    "
                        SELECT
                            DATE_SUB(CURDATE(), INTERVAL $day DAY) AS date,
                            profile.user_id,
                            @a:=@a + 1 AS position,
                            (SELECT
                                    SUM((SELECT
                                                SUM(count)
                                            FROM
                                                view
                                            WHERE
                                                view.postId = post.postId
                                                    AND date < DATE_SUB(CURDATE(), INTERVAL $day DAY)
                                                    AND date >= DATE_SUB(DATE_SUB(CURDATE(), INTERVAL $day DAY),
                                                    INTERVAL $type DAY))) AS views
                                FROM
                                    post
                                WHERE
                                    post.userId = profile.user_id) AS popularity
                        FROM
                            profile
                        WHERE
                            profile.status = 'approved'
                        ORDER BY popularity DESC
                    "
                )->queryAll();
                foreach ($rows as $row) {
                    \Yii::$app->db->createCommand(
                        "
                    REPLACE `position`(date,user_id,type,position) VALUE(
                        '{$row['date']}',
                        '{$row['user_id']}',
                        '{$type}',
                        '{$row['position']}'
                    )
                    "
                    )->execute();
                }

            }
        }
    }

    private function clearOldView()
    {
        View::deleteAll('date < DATE_SUB(CURDATE(), INTERVAL 90 DAY)');
    }

    private function processViewLog()
    {
        \Yii::$app->db->createCommand(
            "
            replace view select date, postId, count(*) as count from view_log where date < CURDATE() group by date, postId
            "
        )->execute();
        ViewLog::deleteAll('date < CURDATE()');
    }


}