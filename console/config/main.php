<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl' => 'http://5sfer.com',
            'rules' => [
                [
                    'class' => \common\components\PostListUrlRule::className()
                ],
                [
                    'pattern' => '<id:\d+>-<slug:.*>',
                    'route' => 'post/view',
                    'suffix' => '.html',
                ],
                [
                    'pattern' => '<id:\d+><any:.*>',
                    'route' => 'post/view',
                ],
                [
                    'pattern' => '<view:about|politika-konfidencialnosti|kak-izvlech-maksimum-polzy-iz-prochitannyx-materialov>',
                    'route' => 'site/view',
                ],
                [
                    'class' => \common\components\UniversalUrlRule::className()
                ]
            ],

        ],
        'sitemap' =>
            [
                'class' => \console\components\sitemap\Sitemap::className(),
                'maxUrlsCountInFile' => 10000,
                'sitemapDirectory' => 'frontend/web/sitemaps',
                'sitemapUrlPath' => '/sitemaps',
            ],
        // for yii cache/flush-all
        'backendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@backend/runtime/cache'
        ],
        'frontendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache'
        ],
    ],
    'params' => $params,
];
