<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log', 'raven'],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                "login"=>"user/security/login",
//                "logout"=>"user/security/logout",
//                "registration"=>"/user/registration/register",
//                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
//                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>'
            ],
        ],
        'cache' => [
            'class' => \yii\caching\DbCache::className(),
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        ],
        'authManager' => [
            'class' => \yii\rbac\DbManager::className(),
            'defaultRoles' => ['guest']
        ],
        'i18n' => [
            'translations' => [
                'kvdatetime' => [
                    'class' => \yii\i18n\PhpMessageSource::className(),
                    'basePath' => '@common/messages',
                ],
                'user' => [
                    'class' => \yii\i18n\PhpMessageSource::className(),
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'raven' => [
            'class' => 'sheershoff\sentry\ErrorHandler',
            'dsn' => 'http://8e8bd37743d4411f869845797602f68c:1ae76312db1c4f5b961d411af919860f@sentry.peresmishnyk.com/2',
            // Sentry DSN
        ],
        'log' => [
            'targets' => [
//                'file' => [
//                    'class' => 'yii\log\FileTarget',
//                    'categories' => ['yii\web\HttpException:404'],
//                    'levels' => ['error', 'warning'],
//                ],
//                'email' => [
//                    'class' => 'yii\log\EmailTarget',
//                    'except' => ['yii\web\HttpException:404'],
//                    'levels' => ['error', 'warning'],
//                    'message' => [
//                        'from' => ['info@5sfer.com'],
//                        'to' => ['michkire@gmail.com'],
//                        'subject' => 'errors at 5sfer.com',
//                    ],
//                ],
                'sentry' => [
                    'class' => 'sheershoff\sentry\Target',
                    'except' => ['yii\web\HttpException:404'],
                    'levels' => ['error', 'warning'],
                    'dsn' => 'http://8e8bd37743d4411f869845797602f68c:1ae76312db1c4f5b961d411af919860f@sentry.peresmishnyk.com/2',
                    // Sentry DSN
                ]
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'rememberFor' => 1209600,
            'enableRegistration' => false,
            'enablePasswordRecovery' => true,
            'enableConfirmation' => false,
            'modelMap' => [
                'Profile' => 'common\models\Profile',
            ],
            'mailer' => [
                'sender' => 'info@5sfer.com', // or ['no-reply@myhost.com' => 'Sender name']
            ],
        ]
    ],
    'language' => 'ru-RU',
    'name' => '5 сфер'
];
