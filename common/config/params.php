<?php
return [
    'adminEmail' => 'mika.smith+5sfer@gmail.com',
    'supportEmail' => 'info@5sfer.com',
    'user.passwordResetTokenExpire' => 3600,
    'imageHost' => 'i.5sfer.com'
];
