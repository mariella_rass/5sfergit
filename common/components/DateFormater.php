<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.07.15
 * Time: 18:17
 */

namespace common\components;


class DateFormater
{
    public static function format($ts)
    {
        $_m = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        ];
        $d = date('j', $ts);
        $m = date('n', $ts) - 1;
        return $d . ' ' . $_m[$m];
    }
}