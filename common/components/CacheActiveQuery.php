<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.05.15
 * Time: 14:39
 */

namespace common\components;


use yii\db\ActiveQuery;

class CacheActiveQuery extends ActiveQuery
{
    protected $cacheDependecy;
    protected $cacheTime = 3600;

    public function all($db = null)
    {
        $db = $db ? $db : \Yii::$app->db;
        $result = $db->cache(
            function ($db) {
                return parent::all($db);
            },
            $this->cacheTime,
            $this->cacheDependecy
        );
        return $result;
    }

    public function one($db = null)
    {
        $db = $db ? $db : \Yii::$app->db;
        $result = $db->cache(
            function ($db) {
                return parent::one($db);
            },
            $this->cacheTime,
            $this->cacheDependecy
        );
        return $result;
    }
}