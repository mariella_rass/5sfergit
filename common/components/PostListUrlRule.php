<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.05.15
 * Time: 18:24
 */

namespace common\components;

use yii\base\Object;
use yii\web\UrlRuleInterface;

class PostListUrlRule extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'post/index') {
            $urlPart = [];
            if (isset($params['page']) && !empty($params['page']) && $params['page']>1) {
                $urlPart[] = $params['page'];
                $urlPart[] = 'page';
            }
            if (isset($params['sort']) && !empty($params['sort'])) {
                $urlPart[] = $params['sort'];
            }
            if (isset($params['filter']) && isset($params['term']) && !empty($params['filter']) && !empty($params['term'])) {
                $urlPart[] = $params['term'];
                $urlPart[] = $params['filter'];
            }
            return implode('/', array_reverse($urlPart));
        }
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match(
            '%(?:(?P<filter>category|tag)\/(?P<term>[\w-]+))?(?:\/?(?P<sort>popular))?(?:\/?page\/(?P<page>\d+))?%',
            $pathInfo,
            $matches
        )) {
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
            $param = [];
            foreach (['filter', 'term', 'page', 'sort'] as $key) {
                if (isset($matches[$key])) {
                    $param[$key] = $matches[$key];
                }
            }
            return count($param)||$pathInfo == '' ? ['post/index', $param] : false;
        }
        return false;  // this rule does not apply
    }
}