<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.05.15
 * Time: 18:24
 */

namespace common\components;

use yii\base\Object;
use yii\web\UrlRuleInterface;

class UniversalUrlRule extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if (!count(array_diff_key($params, ['page'=>'','slug'=>'']))) {
            list($controller, $action) = explode('/', $route.'/');
            $urlPart = [];
            if (isset($params['page']) && !empty($params['page']) && $params['page'] > 1) {
                $urlPart[] = $params['page'];
                $urlPart[] = 'page';
            }
            if (isset($action) && !empty($action) && $action != 'index'){
                $urlPart[] = $action;
            }
            if (isset($params['slug']) && !empty($params['slug'])) {
                $urlPart[] = $params['slug'];
            }
            $urlPart[] = $controller;
            $urlPart = array_reverse($urlPart);
//            \Yii::trace([
//                'dif' => array_diff_key($params, ['page'=>'','slug'=>'']),
//                'route' => $route,
//                'params' => $params,
//                implode('/', $urlPart)
//            ]);
            return implode('/', $urlPart);
        }
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match(
            '%(?:\/?(?P<controller>[\w-]+))?(?:\/?(?!page)(?P<slug>[\w-]+))?(?:\/?(?P<action>(?!page)[\w-]+))?(?:\/?page\/(?P<page>\d+))?%',
            $pathInfo,
            $matches
        )) {
            \Yii::trace($matches);
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
            $controller = isset($matches['controller']) ? $matches['controller'] : '';
            $action = isset($matches['action']) ? $matches['action'] : '';

            $testController = \Yii::$app->createControllerByID($controller);
            if ($testController !== null && !$action && isset($matches['slug']) && $testController->createAction($matches['slug']) !== null) {
                $action = $matches['slug'];
                unset($matches['slug']);
            }

            if ($controller) {
                $param = [];
                foreach (['page', 'slug'] as $key) {
                    if (isset($matches[$key])) {
                        $param[$key] = $matches[$key];
                    }
                }
                $route = $controller . '/' . $action;
//                \Yii::trace([$route, $param]);
                return [$route, $param];
            } else {
                return false;
            }
        }
        return false;  // this rule does not apply
    }
}