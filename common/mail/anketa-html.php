<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.06.15
 * Time: 21:18
 *
 * @var $model frontend\models\AnketaForm
 *
 */

$template = [
    'Контактные данные' => [
        'name',
        'middlename',
        'surname',
        'email',
        'phone',
        'facebook',
        'vk',
        'youtube'
    ],
    'Оценка экспертности' => [
        'education',
        'methodology',
        'experience',
        'additionalEducation',
        'awards',
        'knowledgeOfCompetitors'
    ],
    'Оценка ментальности' => [
        'История успеха' => 'history',
        'result',
        'hobby'
    ],
    'Оценка контактности' => [
        'site',
        'Рассылка' => 'subscribers100',
        'profile',
        'famoust_recomendation',
        'video_recomendation',
        'book',
        'video_content'
    ]

]
?>
<?php foreach ($template as $sectionName => $sectionData): ?>
    <h1><?= $sectionName ?></h1>

    <?php foreach ($sectionData as $label => $key): ?>
        <h3><?= is_string($label) ? $label : $model->attributeLabels()[$key] ?></h3>
        <p><?= $model->{$key} ?></p>
    <?php endforeach; ?>
<?php endforeach; ?>

