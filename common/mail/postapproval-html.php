<p>Автор <?= \yii\helpers\Html::a(
        $model->user->profile->name,
        \yii\helpers\Url::to(['author/update', 'id' => $model->user->primaryKey], true)
    ) ?> добавил новую статью <?= \yii\helpers\Html::a(
        $model->postTitle,
        \yii\helpers\Url::to(['post/update', 'id' => $model->primaryKey], true)
    ) ?></p>
<p><?= date("d.m.y") ?></p>