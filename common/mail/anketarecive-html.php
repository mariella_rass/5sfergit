<p>Дорогой друг,<br>

    поздравляем, ты сделал еще один важный шаг на пути своего развития. Мы получили твою анкету!<br>
    Обычно, ее рассмотрение не занимает больше одного дня и очень скоро мы свяжемся с тобой.</p>

<p>А пока мы составляем план твоего обучения, ознакомься со <a
        href="http://pintosevich.com/treningi/raspisanie/?utm_source=5sfer&utm_medium=affiliate&utm_content=ips_trainings&utm_campaign=anketa_letter">списком
        развивающих тренингов и вебинаров</a> от лидера в сфере развития — компании «Isaac Pintosevich Systems».
    Также, рекомендуем тебе пройти автотренинг по написанию и продвижению книги с нуля от Ицхака Пинтосевича «<a
        href="http://pintosevich.com/napishi/?utm_source=5sfer&utm_medium=affiliate&utm_content=napishi&utm_campaign=anketa_letter">Напиши!
        Книга за 60 дней</a>».</p>

<p>Если у тебя есть вопросы, пожалуйста, пиши нам на info@5sfer.com. До скорой связи!</p>

<p>Действуй! Живи! Влияй! Богатей! Люби!</p>