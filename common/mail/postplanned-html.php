<p>Дорогой друг,</p>
<p>твоя статья «<?= \yii\helpers\Html::a(
        $model->postTitle,
        'http://5sfer.com/' . $model->postId
    ) ?>» запланирована к публикации на <?= strftime(
        '%d-%m-%Y %H:%M',
        $model->postPublishTime
    ) ?>.<br></p>
<p>Пожалуйста, прими во внимание, что дата и время публикации могут измениться. В любом случае, мы пришлем тебе письмо в
    момент появления статьи на портале <a href="http://5sfer.com">5 сфер</a></p>
<p>Любые вопросы ты можешь задать нам на info@5sfer.com</p>
<br>
<p>Действуй! Живи! Влияй! Богатей! Люби!</p>
<br>
<p>P.S. Ознакомься со <a href="http://pintosevich.com/treningi/raspisanie/?utm_source=5sfer&utm_medium=affiliate&utm_campaign=author">списком ближайших тренингов IPS</a></p>