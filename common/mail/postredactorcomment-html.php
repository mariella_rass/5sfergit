<p>Дорогой автор,</p>
<p>редактор портала 5 сфер вернул твою статью «<?= \yii\helpers\Html::a(
        $model->postTitle,
        \yii\helpers\Url::to(['author-post/update', 'id' => $model->primaryKey], true)
    ) ?>» на доработку с комментрием:<br></p>
<p>----------------------------------------</p>
<div>
    <?= $model->postRedactorComment ?>
</div>
<p>----------------------------------------</p>
<p>Если у тебя есть вопросы относительно этой статьи, напиши нам на info@5sfer.com</p>
<p>Команда 5 сфер</p>