<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 14:15
 */

namespace common\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class AuthorSearch extends Author
{
    public $query;
    public $sorter;
    public $pagination = [
        'defaultPageSize' => 10,
    ];

    public $profile_name;

    public function init()
    {
        parent::init();
        $this->query = self::find()->with('profile');
        $this->sorter = $this->getSorter();
    }

    private function getSorter()
    {
        $sorter = new \yii\data\Sort(
            [
                'defaultOrder' => ['popularity30' => SORT_DESC],
                'attributes' => [
                    'id' => [],
                    'popularity30' => [
                        'desc' => ['profile.popularity30' => SORT_DESC, 'id' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'за 30 дней',
                    ],
                    'popularity7' => [
                        'desc' => ['profile.popularity7' => SORT_DESC, 'id' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'за 7 дней',
                    ],
                    'popularity1' => [
                        'desc' => ['profile.popularity1' => SORT_DESC, 'id' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'за вчера',
                    ],
                ],
            ]
        );
        return $sorter;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'profile_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $query = $this->query;
        $query->joinWith('profile', true);

        $sorter = isset($this->sorter) ? $this->sorter : false;

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => $this->pagination,
                'sort' => $sorter
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            //return $dataProvider;
        }

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'profile.name', $this->profile_name]);
//            ->andFilterWhere(['like', 'postText', $this->postText])
//            ->andFilterWhere(['like', 'postName', $this->postName])
//            ->andFilterWhere(['like', 'postVideo', $this->postVideo])
//            ->andFilterWhere(['postStatus' => $this->postStatus]);

        return $dataProvider;
    }
}