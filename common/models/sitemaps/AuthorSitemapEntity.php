<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.16
 * Time: 16:56
 */

namespace common\models\sitemaps;


use common\models\Category;
use common\models\Post;
use common\models\Profile;
use console\components\sitemap\SitemapEntityInterface;

class AuthorSitemapEntity extends Profile implements SitemapEntityInterface
{
    /**
     * Get lastmod value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLastmod()
    {
        return time();
    }

    /**
     * Get changefreq value for sitemap file.
     *
     * @return string
     */
    public function getSitemapChangefreq()
    {
        return 'daily';
    }

    /**
     * Get priority value for sitemap file.
     *
     * @return string
     */
    public function getSitemapPriority()
    {
        return 0.8;
    }

    /**
     * Get loc value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLoc()
    {
        return \Yii::$app->urlManager->createAbsoluteUrl(
            ['author/index', 'slug' => $this->slug]
        );
    }

    /**
     * Get data source for sitemap file generation.
     *
     * @return \yii\db\ActiveQuery $dataSource
     */
    public static function getSitemapDataSource()
    {
        return self::find()->from(self::tableName() . ' pr')->where(
            ['exists', Post::find()->from(Post::tableName() . ' po')->where('po.userId = pr.user_id')]
        );
//        echo $query->createCommand()->sql . PHP_EOL;
//        echo $query->createCommand()->getRawSql() . PHP_EOL;
//        die();
//        return self::findBySql('
//        SELECT
//            *
//        FROM
//            profile
//        WHERE
//            EXISTS( SELECT
//                    *
//                FROM
//                    post
//                WHERE
//                    postStatus = 'publish'
//                        AND post.userId = profile.user_id)
//        ');
    }
}