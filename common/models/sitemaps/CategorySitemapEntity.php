<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.16
 * Time: 16:56
 */

namespace common\models\sitemaps;


use common\models\Category;
use console\components\sitemap\SitemapEntityInterface;

class CategorySitemapEntity extends Category implements SitemapEntityInterface
{
    /**
     * Get lastmod value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLastmod()
    {
        return time();
    }

    /**
     * Get changefreq value for sitemap file.
     *
     * @return string
     */
    public function getSitemapChangefreq()
    {
        return 'weekly';
    }

    /**
     * Get priority value for sitemap file.
     *
     * @return string
     */
    public function getSitemapPriority()
    {
        return 0.7;
    }

    /**
     * Get loc value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLoc()
    {
        return \Yii::$app->urlManager->createAbsoluteUrl(
            ['post/index', 'term' => $this->categorySlug, 'filter' => 'category']
        );
    }

    /**
     * Get data source for sitemap file generation.
     *
     * @return \yii\db\ActiveQuery $dataSource
     */
    public static function getSitemapDataSource(){
        return self::find(['categoryStatus' => 'on']);
    }
}