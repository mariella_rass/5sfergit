<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.16
 * Time: 16:56
 */

namespace common\models\sitemaps;


use common\models\Category;
use common\models\Post;
use console\components\sitemap\SitemapEntityInterface;

class PostSitemapEntity extends Post implements SitemapEntityInterface
{
    /**
     * Get lastmod value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLastmod()
    {
        return $this->postPublishTime;
    }

    /**
     * Get changefreq value for sitemap file.
     *
     * @return string
     */
    public function getSitemapChangefreq()
    {
        return 'weekly';
    }

    /**
     * Get priority value for sitemap file.
     *
     * @return string
     */
    public function getSitemapPriority()
    {
        return 0.9;
    }

    /**
     * Get loc value for sitemap file.
     *
     * @return string
     */
    public function getSitemapLoc()
    {
        return \Yii::$app->urlManager->createAbsoluteUrl(
            [
                'post/view',
                'id' => $this->primaryKey,
                'slug' => $this->postName
            ]
    );
    }

    /**
     * Get data source for sitemap file generation.
     *
     * @return \yii\db\ActiveQuery $dataSource
     */
    public static function getSitemapDataSource()
    {
        return self::find(['postStatus' => Post::PUBLISH])->orderBy('postPublishTime DESC');
    }
}