<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.15
 * Time: 13:17
 */

namespace common\models;

class Term extends \common\models\base\Term
{
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'termSlug',
                'attribute' => 'termName',
                // optional params
                'ensureUnique' => true,
                //'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ]
        ];
    }
}