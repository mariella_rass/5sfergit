<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.06.15
 * Time: 12:09
 */

namespace common\models;


class ViewLog extends \common\models\base\ViewLog
{
    public static function log($post)
    {
        $date = strftime("%G-%m-%d", time());
        $ip = inet_pton(\Yii::$app->request->userIP);
        $postId = $post->primaryKey;

        $command = \Yii::$app->db->createCommand(
            "
            INSERT IGNORE INTO view_log(date,ip,postId) VALUES (:date,:ip,:postId)
        "
        );

        $command->bindValue(':date', $date);
        $command->bindValue(':ip', $ip);
        $command->bindValue(':postId', $postId);

        $affectedRows = $command->execute();

        if ($affectedRows == 1) {
            $post->registerView();
        }

        $post->registerRawView();
    }
}