<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 17:47
 */

namespace common\models;


use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
    public function init()
    {
        $modelClass = $this->modelClass;
        $tableName = $modelClass::tableName();

//        $subq = new Query();
//        $subq->
//        from(PostHasTerm::tableName() . ' p2')->
//        where("{$tableName}.termId = p2.termId")->
//        andWhere(['type' => 'category']);
//
//        $this->andWhere(['exists', $subq]);
        parent::init();
    }

    public function special()
    {
        return $this->andWhere(
            [
                'categoryEditable' => 'true',
                'categoryStatus' => 'on'
            ]
        );
    }
}