<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.06.15
 * Time: 16:39
 */

namespace common\models;


use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

class Event extends \common\models\base\Event
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eventId' => 'Event ID',
            'eventName' => 'Название',
            'eventUrl' => 'Ссылка',
            'eventDate' => 'Показывать до',
            'eventStatus' => 'Активно',
            'userId' => 'Пользователь',
            'eventPaid' => 'Платное/бесплатное',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'eventDate',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'eventDate',
                ],
                'value' => function ($event) {
                    return strtotime($this->eventDate);
                },
            ],
        ];
    }
}