<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 14:31
 */

namespace common\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class PostSearch extends Post
{
    public $query;
    public $sorter;
    public $pagination = [
        'pageSize' => 20,
    ];
    public $categoryId;

    public function init()
    {
        parent::init();
        $this->query = self::find();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['categoryId','postId', 'userId', 'postViews', 'postShare', 'postUpdateTime', 'postPublishTime', 'postPopularity'],
                'integer'
            ],
            [['postTitle', 'postLead', 'postText', 'postName', 'postVideo', 'postStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['author-search'] = [
            'postTitle',
            'postLead',
        ];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->query;
        $sorter = isset($this->sorter) ? $this->sorter : false;

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => $this->pagination,
                'sort' => $sorter
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            //return $dataProvider;
        }

        if (in_array($this->scenario, ['author-search','author-update','author-create'])) {
            $query->andFilterWhere(['userId' => \Yii::$app->user->id])
            ->andFilterWhere(['postStatus' => [Post::DRAFT, Post::APPROVAL]])
            ->andFilterWhere(['like', 'postTitle', $this->postTitle]);

        } else {
            $category = Category::findOne($this->categoryId);
            if ($category){
                $query = $query->categoryFilter($category);
            }
            $query->andFilterWhere(['like', 'postTitle', $this->postTitle])
                ->andFilterWhere(['postId' => $this->postId])
                ->andFilterWhere(['userId' => $this->userId])
                ->andFilterWhere(['postStatus' => $this->postStatus]);
        }
        return $dataProvider;
    }
}