<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.15
 * Time: 13:19
 */

namespace common\models;


use himiklab\sitemap\behaviors\SitemapBehavior;
use yii\helpers\Url;
use zhelyabuzhsky\sitemap\models\SitemapEntityInterface;

class Category extends \common\models\base\Category
{
    const DEFAULT_COLOR = 10066329;

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public static function getByName($name)
    {
        return Category::findOne(['categoryName' => $name]);
    }

    public function getUrl()
    {
        return \yii\helpers\Url::to(
            ['post/index', 'term' => $this->categorySlug, 'filter' => 'category']
        );
    }

    public function getName()
    {
        return $this->categoryName;
    }

    public function getTitle()
    {
        return $this->categoryTitle;
    }

    public function getSlug()
    {
        return $this->categorySlug;
    }

    public function getDescription()
    {
        return $this->categoryDescription;
    }

    public function getHexColor()
    {
        $color = isset($this->categoryColor) ? $this->categoryColor : self::DEFAULT_COLOR;
        return sprintf("%'.06x", $color);
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'categorySlug',
                'attribute' => 'categoryName',
                // optional params
                'ensureUnique' => true,
                //'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['categoryName', 'categorySlug']);
                    $model->andWhere(['categoryStatus' => 'on']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => \Yii::$app->urlManager->createAbsoluteUrl(
                            ['post/index', 'term' => $model->categorySlug, 'filter' => 'category']
                        ),
                        'lastmod' => time(),
                        //'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }
}