<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.05.15
 * Time: 20:10
 */

namespace common\models;

use common\models\base\PostHasCategory;
use common\models\base\PostHasTag;
use yii\db\ActiveQuery;
use yii\db\Query;

class PostQuery extends ActiveQuery
{
//    public function init()
//    {
//        $this->cacheDependecy = new ChainedDependency();
//
//        $postUpdateTimeDependecy = new DbDependency();
//        $postUpdateTimeDependecy->sql = 'select max(postUpdateTime) from post';
//        $postUpdateTimeDependecy->reusable = true;
//        $this->cacheDependecy->dependencies[] = $postUpdateTimeDependecy;
//    }

    public function status($status)
    {
        return $this->andWhere(['postStatus' => $status]);
    }

    public function author($id)
    {
        return $this->andWhere(['userId' => $id]);
    }

    public function orderByDate()
    {
        return $this->orderBy(['postId' => SORT_DESC]);
    }

    public function orderByPopularity()
    {
        return $this->orderBy(['postId' => SORT_DESC]);
    }

    public function tagFilter(Tag $term)
    {
//        return $this->
//            joinWith('terms')->
//            andWhere(['term.termId' => $term->primaryKey]);
        $modelClass = $this->modelClass;
        $tableName = $modelClass::tableName();

        $subq = new Query();
        $subq->
        from(PostHasTag::tableName() . ' p2')->
        where("{$tableName}.postId = p2.postId")->
        andWhere(['tagId' => $term->primaryKey]);
        return $this->andWhere(['exists', $subq]);
    }

    public function categoryFilter(Category $term)
    {
//        return $this->
//            joinWith('terms')->
//            andWhere(['term.termId' => $term->primaryKey]);
        $modelClass = $this->modelClass;
        $tableName = $modelClass::tableName();

        $subq = new Query();
        $subq->
        from(PostHasCategory::tableName() . ' p2')->
        where("{$tableName}.postId = p2.postId")->
        andWhere(['categoryId' => $term->primaryKey]);
        return $this->andWhere(['exists', $subq]);
    }

    public function minViews($views)
    {
        return $this->andWhere(['>','postViews',$views]);
    }
}