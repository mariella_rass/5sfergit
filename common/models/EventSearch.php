<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;

/**
 * SearchEvent represents the model behind the search form about `common\models\Event`.
 */
class EventSearch extends Event
{
    public $query;
    public $pagination = [
        'pageSize' => 20,
    ];

    public function init()
    {
        parent::init();
        $this->query = self::find();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eventId', 'eventDate', 'userId'], 'integer'],
            [['eventName', 'eventUrl', 'eventStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $this->query,
                'pagination' => $this->pagination,
                'sort' => new \yii\data\Sort(
                    [
                        'defaultOrder' => ['eventDate' => SORT_ASC],
                        'attributes' => [
                            'eventDate' => [],
                        ]
                    ]
                )
            ]
        );

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'eventId' => $this->eventId,
                'eventDate' => $this->eventDate,
                'userId' => $this->userId,
            ]
        );

        $query->andFilterWhere(['like', 'eventName', $this->eventName])
            ->andFilterWhere(['like', 'eventUrl', $this->eventUrl])
            ->andFilterWhere(['like', 'eventStatus', $this->eventStatus]);

        return $dataProvider;
    }
}
