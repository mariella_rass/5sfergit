<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\base\Achive;

/**
 * AchiveSearch represents the model behind the search form about `common\models\base\Achive`.
 */
class AchiveSearch extends Achive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['achiveId'], 'integer'],
            [['achiveTitle', 'achiveDescription', 'achiveUrl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Achive::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'achiveId' => $this->achiveId,
        ]);

        $query->andFilterWhere(['like', 'achiveTitle', $this->achiveTitle])
            ->andFilterWhere(['like', 'achiveDescription', $this->achiveDescription])
            ->andFilterWhere(['like', 'achiveUrl', $this->achiveUrl]);

        return $dataProvider;
    }
}
