<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 13:42
 */

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;

class Author extends User
{
    const WAIT = 'wait';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';
    const STAFF = 'staff';

    public static function find()
    {
        return new AuthorQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition($dayshift)
    {
        return $this->hasOne(Position::className(), ['user_id' => 'id'])
            ->onCondition('date = DATE_SUB(CURDATE(), INTERVAL :dayshift DAY) AND type = :type')->addParams(
                [':dayshift' => $dayshift]
            );
    }

    public function getUrl()
    {
        return \yii\helpers\Url::to(['author/index', 'slug' => $this->profile->slug]);
    }

    public function getUrlComment()
    {
        return \yii\helpers\Url::to(['author/comment', 'slug' => $this->profile->slug]);
    }

    public function getUrlAchives()
        {
            return \yii\helpers\Url::to(['author/achives', 'slug' => $this->profile->slug]);
        }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition1()
    {
        return $this->getPosition(1);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition7()
    {
        return $this->getPosition(7);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition30()
    {
        return $this->getPosition(30);
    }

    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['userId' => 'id']);
    }

    public function getActiveEvents()
    {
        return $this->hasMany(Event::className(), ['userId' => 'id'])->andWhere(['eventStatus' => 'on'])->andWhere(
            ['>', 'eventDate', time()]
        )->orderBy(['eventDate' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAchives()
    {
        return $this->hasMany(Achive::className(), ['achiveId' => 'achiveId'])->viaTable(
            'user_has_achive',
            ['userId' => 'id']
        );
    }

    public function linkAchives($ids)
    {
        $this->unlinkAll('achives', true);
        if ($ids) {
            foreach (Achive::findAll($ids) as $item) {
                $this->link('achives', $item);
            }
        }
    }

}