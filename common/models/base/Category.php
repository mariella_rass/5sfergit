<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $categoryId
 * @property string $categoryName
 * @property string $categorySlug
 * @property string $categoryEditable
 * @property integer $categoryColor
 * @property string $categoryStatus
 * @property string $categoryTitle
 * @property string $categoryDescription
 *
 * @property PostHasCategory[] $postHasCategories
 * @property Post[] $posts
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryName', 'categorySlug', 'categoryTitle', 'categoryDescription'], 'required'],
            [['categoryEditable', 'categoryStatus', 'categoryDescription', 'categoryRel', 'categoryAppendHtml'], 'string'],
            [['categoryColor'], 'integer'],
            [['categoryName', 'categorySlug'], 'string', 'max' => 200],
            [['categoryTitle'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Category ID',
            'categoryName' => 'Название',
            'categorySlug' => 'Category Slug',
            'categoryEditable' => 'Category Editable',
            'categoryColor' => 'Category Color',
            'categoryStatus' => 'Category Status',
            'categoryTitle' => 'SEO Title',
            'categoryDescription' => 'SEO Description',
            'categoryRel' => 'follow / no-follow',
            'categoryAppendHtml' => 'Дополнительный HTML для статей категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostHasCategories()
    {
        return $this->hasMany(PostHasCategory::className(), ['categoryId' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['postId' => 'postId'])->viaTable('post_has_category', ['categoryId' => 'categoryId']);
    }
}
