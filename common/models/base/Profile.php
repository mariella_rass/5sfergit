<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $bio
 * @property integer $popularity1
 * @property integer $popularity7
 * @property integer $popularity30
 * @property string $status
 * @property string $avatar
 * @property string $facebook
 * @property string $vk
 * @property string $youtube
 * @property string $twitter
 * @property string $website
 * @property string $gravatar_email
 * @property string $slug
 * @property string $triggerComment
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','name', 'nameGenitivus'], 'required'],
            [['user_id', 'popularity1', 'popularity7', 'popularity30', 'rate'], 'integer'],
            [['bio', 'status','slug', 'postForm', 'subscribeDoneText', 'triggerComment'], 'string'],
            [['name', 'nameGenitivus', 'avatar', 'facebook', 'vk', 'gplus', 'youtube', 'twitter', 'website','gravatar_email'], 'string', 'max' => 255],
            [['facebook', 'vk', 'gplus', 'youtube', 'twitter', 'website'], 'default'],
            ['website', 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Имя',
            'nameGenitivus' => 'Имя в родительном падеже (Кого? Ицхака Пинтосевича)',
            'bio' => 'Об авторе',
            'popularity1' => 'Popularity1',
            'popularity7' => 'Popularity7',
            'popularity30' => 'Popularity30',
            'status' => 'Status',
            'avatar' => 'Avatar',
            'facebook' => 'Facebook',
            'gplus' => 'Google Plus',
            'vk' => 'Vk',
            'youtube' => 'Youtube',
            'twitter' => 'Twitter',
            'website' => 'Вебсайт',
            'gravatar_email' => 'gravatar_email',
            'slug' => 'slug',
            'rate' => 'Рейтинг',
            'triggerComment' => 'Комментарии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
