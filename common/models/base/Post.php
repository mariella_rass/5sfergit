<?php

namespace common\models\base;

use common\models\Author;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $postId
 * @property integer $userId
 * @property string $postTitle
 * @property string $postLead
 * @property string $postText
 * @property string $postName
 * @property string $postVideo
 * @property integer $postViews
 * @property integer $postShare
 * @property integer $postUpdateTime
 * @property integer $postPublishTime
 * @property integer $postPopularity
 * @property string $postStatus
 * @property string $postImage
 *
 * @property User $user
 * @property PostHasTerm[] $postHasTerms
 * @property Term[] $terms
 * @property View[] $views
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'postTitle', 'postName', 'postViews', 'postShare', 'postUpdateTime', 'postPopularity'], 'required'],
            [['userId', 'postViews', 'postShare', 'postUpdateTime', 'postPopularity'], 'integer'],
            [['postLead', 'postText', 'postVideo', 'postStatus', 'postPublishTime', 'postRedactorComment', 'postRel'], 'string'],
            [['postTitle', 'postName'], 'string', 'max' => 200],
            [['postImage'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postId' => 'Post ID',
            'userId' => 'Автор',
            'postTitle' => 'Заголовок',
            'postLead' => 'Подзаголовок',
            'postText' => 'Тело статьи',
            'postName' => 'Post Name',
            'postVideo' => 'Код видео',
            'postViews' => 'Post Views',
            'postShare' => 'Post Share',
            'postUpdateTime' => 'Post Update Time',
            'postPublishTime' => 'Время публикации',
            'postPopularity' => 'Post Popularity',
            'postStatus' => 'Статус публикации',
            'postRel' => 'follow / no-follow',
            'file' => 'Изображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Author::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditor()
    {
        return $this->hasOne(Author::className(), ['id' => 'editorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViews()
    {
        return $this->hasMany(View::className(), ['postId' => 'postId']);
    }

}
