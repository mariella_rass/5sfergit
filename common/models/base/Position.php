<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property string $type
 * @property string $date
 * @property integer $user_id
 * @property integer $position
 *
 * @property User $user
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'date', 'user_id', 'position'], 'required'],
            [['type'], 'string'],
            [['date'], 'safe'],
            [['user_id', 'position'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Type',
            'date' => 'Date',
            'user_id' => 'User ID',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
