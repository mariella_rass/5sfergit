<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "post_has_category".
 *
 * @property integer $postId
 * @property integer $categoryId
 *
 * @property Post $post
 * @property Category $category
 */
class PostHasCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_has_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postId', 'categoryId'], 'required'],
            [['postId', 'categoryId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postId' => 'Post ID',
            'categoryId' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['postId' => 'postId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['categoryId' => 'categoryId']);
    }
}
