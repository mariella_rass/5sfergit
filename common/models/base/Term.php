<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "term".
 *
 * @property integer $termId
 * @property string $termName
 * @property string $termSlug
 * @property string $termsType
 * @property string $termEditable
 * @property integer $termColor
 * @property string $termStatus
 * @property string $termTitle
 * @property string $termDescription
 *
 * @property PostHasTerm[] $postHasTerms
 * @property Post[] $posts
 */
class Term extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'term';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['termName', 'termSlug', 'termTitle', 'termDescription'], 'required'],
            [['termEditable', 'termStatus', 'termDescription'], 'string'],
            [['termColor'], 'integer'],
            [['termName', 'termSlug'], 'string', 'max' => 200],
            [['termTitle'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'termId' => 'Term ID',
            'termName' => 'Term Name',
            'termSlug' => 'Term Slug',
            'termEditable' => 'Term Editable',
            'termColor' => 'Term Color',
            'termStatus' => 'Term Status',
            'termTitle' => 'Term Title',
            'termDescription' => 'Term Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostHasTerms()
    {
        return $this->hasMany(PostHasTerm::className(), ['termId' => 'termId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['postId' => 'postId'])->viaTable(
            'post_has_term',
            ['termId' => 'termId']
        );
    }
}
