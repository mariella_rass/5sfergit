<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $tagId
 * @property string $tagName
 * @property string $tagSlug
 * @property string $tagTitle
 * @property string $tagDescription
 *
 * @property PostHasTag[] $postHasTags
 * @property Post[] $posts
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tagName', 'tagSlug', 'tagTitle', 'tagDescription'], 'required'],
            [['tagDescription'], 'string'],
            [['tagName', 'tagSlug'], 'string', 'max' => 200],
            [['tagTitle'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tagId' => 'Tag ID',
            'tagName' => 'Название',
            'tagSlug' => 'Tag Slug',
            'tagTitle' => 'SEO Title',
            'tagDescription' => 'SEO Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostHasTags()
    {
        return $this->hasMany(PostHasTag::className(), ['tagId' => 'tagId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['postId' => 'postId'])->viaTable('post_has_tag', ['tagId' => 'tagId']);
    }
}
