<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "view_log".
 *
 * @property string $date
 * @property integer $postId
 * @property string $ip
 */
class ViewLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'postId', 'ip'], 'required'],
            [['date'], 'safe'],
            [['postId'], 'integer'],
            [['ip'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Date',
            'postId' => 'Post ID',
            'ip' => 'Ip',
        ];
    }
}
