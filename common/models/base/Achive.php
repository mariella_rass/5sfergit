<?php

namespace common\models\base;

use common\models\Author;
use Yii;

/**
 * This is the model class for table "achive".
 *
 * @property integer $achiveId
 * @property string $achiveTitle
 * @property string $achiveDescription
 * @property string $achiveUrl
 * @property string $achiveImage
 *
 * @property UserHasAchive[] $userHasAchives
 * @property User[] $users
 */
class Achive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'achive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['achiveTitle', 'achiveDescription', 'achiveUrl'], 'required'],
            [['achiveDescription'], 'string'],
            [['achiveUrl'], 'url', 'defaultScheme' => 'http'],
            [['achiveTitle', 'achiveUrl'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'achiveId' => 'Achive ID',
            'achiveTitle' => 'Название',
            'achiveDescription' => 'Описание',
            'achiveUrl' => 'Ссылка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Author::className(), ['id' => 'userId'])->viaTable(
            'user_has_achive',
            ['achiveId' => 'achiveId']
        );
    }
}
