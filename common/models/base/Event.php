<?php

namespace common\models\base;

use common\models\Author;
use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $eventId
 * @property string $eventName
 * @property string $eventUrl
 * @property integer $eventDate
 * @property string $eventStatus
 * @property integer $userId
 *
 * @property User $user
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eventName', 'eventUrl', 'eventDate', 'eventStatus', 'userId'], 'required'],
            [['userId'], 'integer'],
            [['eventStatus'], 'string'],
            [['eventDate'], 'safe'],
            [['eventPaid'], 'string'],
            [['eventName', 'eventUrl'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eventId' => 'Event ID',
            'eventName' => 'Event Name',
            'eventUrl' => 'Event Url',
            'eventDate' => 'Event Date',
            'eventStatus' => 'Event Status',
            'eventPrice' => 'Event Price',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Author::className(), ['id' => 'userId']);
    }
}
