<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "post_has_term".
 *
 * @property integer $postId
 * @property string $termId
 *
 * @property Post $post
 * @property Term $term
 */
class PostHasTerm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_has_term';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postId', 'termId'], 'required'],
            [['postId', 'termId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postId' => 'Post ID',
            'termId' => 'Term ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['postId' => 'postId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(Term::className(), ['termId' => 'termId']);
    }
}
