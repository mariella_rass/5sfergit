<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "view".
 *
 * @property string $date
 * @property integer $postId
 * @property integer $count
 *
 * @property Post $post
 */
class View extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'postId', 'count'], 'required'],
            [['date'], 'safe'],
            [['postId', 'count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Date',
            'postId' => 'Post ID',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['postId' => 'postId']);
    }
}
