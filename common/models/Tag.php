<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.15
 * Time: 13:19
 */

namespace common\models;


class Tag extends \common\models\base\Tag
{
    const DEFAULT_COLOR = 10066329;

    public static function find()
        {
            return new TagQuery(get_called_class());
        }
    
        public static function getByName($name)
        {
            return Tag::findOne(['tagName' => $name]);
        }
    
        public function getUrl()
        {
            return \yii\helpers\Url::to(
                ['post/index', 'term' => $this->tagSlug, 'filter' => 'tag']
            );
        }
    
        public function getName()
        {
            return $this->tagName;
        }
    
        public function getTitle()
        {
            return $this->tagTitle;
        }
    
        public function getSlug()
        {
            return $this->tagSlug;
        }
    
        public function getDescription()
        {
            return $this->tagDescription;
        }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'tagSlug',
                'attribute' => 'tagName',
                // optional params
                'ensureUnique' => true,
                //'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ]
        ];
    }

}