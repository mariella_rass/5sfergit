<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.06.15
 * Time: 13:43
 */

namespace common\models;


use common\models\base\Position;
use yii\db\ActiveQuery;

class AuthorQuery extends ActiveQuery{
    public function init()
     {
//         $modelClass = $this->modelClass;
//         $tableName = $modelClass::tableName();
         $this->with('profile');
         parent::init();
     }

    public function status($status){
        return $this->andWhere(['status' => $status]);
    }

    public function withPosition($dayshift, $interval){
        return $this->with(Position::find()->andWhere(['type' => $interval]));
    }
}