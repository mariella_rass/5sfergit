<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 16.06.15
 * Time: 14:21
 */

namespace common\models;


class Position extends \common\models\base\Position
{
    public static function delta($cur, $prev)
    {
        if (isset($prev) && $prev !== false) {
            return $prev->position - $cur;
        } else {
            return 0;
        }
    }
}