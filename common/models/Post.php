<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.15
 * Time: 13:18
 */

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use mongosoft\file\UploadImageBehavior;
use sjaakp\illustrated\Illustrated;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class Post extends \common\models\base\Post
{
    // post status
    const DRAFT = 'draft';
    const APPROVAL = 'approval';
    const FUTURE = 'future';
    const PUBLISH = 'publish';
    const ARCHIVE = 'archive';

    // post type
    const TYPE_UNDEF = 'type-undef';
    const TYPE_NEW = 'type-new';
    const TYPE_RERIGHT = 'type-reright';
    const TYPE_TRANSLATE = 'type-translate';
    const TYPE_NEWS = 'type-news';
    const TYPE_TEST = 'type-test';

    public static function getPostTypeLabel($label)
    {
        switch ($label) {
            case self::TYPE_NEW:
                $r = 'Авторская';
                break;
            case self::TYPE_RERIGHT:
                $r = 'Рерайт';
                break;
            case self::TYPE_TRANSLATE:
                $r = 'Перевод';
                break;
            case self::TYPE_NEWS:
                $r = 'Новости';
                break;
            case self::TYPE_TEST:
                $r = 'Тест';
                break;
            case self::TYPE_UNDEF:
            default:
                $r = 'Не определено';
                break;
        }
        return $r;
    }


    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['userId', 'postTitle', 'postName', 'postViews', 'postShare', 'postUpdateTime', 'postPopularity'],
                'required'
            ],
            [
                ['userId', 'postViews', 'postRawViews', 'postShare', 'postUpdateTime', 'postPopularity'],
                'integer'
            ],
            [['postLead', 'postText', 'postVideo', 'postStatus', 'postRel', 'postComment'], 'string'],
            [['postTitle', 'postName'], 'string', 'max' => 200],
            [['postImage'], 'string', 'max' => 255],
            [['postType'], 'in', 'range' => ['type-undef', 'type-new', 'type-reright', 'type-translate', 'type-news','type-test']],
            [['postPublishTime', '__postImage_crop__', '__postImage_delete__'], 'safe'],
            [['postSourceUrl'], 'url'],
            [
                '__postImage_file__',
                'required',
                'when' => function ($model) {
                    return (in_array(
                            $model->postStatus,
                            ['publish', 'future']
                        ) && empty($model->postImage));
                },
                'whenClient' => 'function(){return false}'
            ],
            [
                'postText',
                'required',
                'when' => function ($model) {
                    return in_array(
                        $model->postStatus,
                        ['publish', 'future']
                    );
                }
            ],
            [
                'postStatus',
                'in',
                'range' => [Post::DRAFT, Post::APPROVAL],
                'when' => function ($model) {
                    return in_array(
                        $model->scenario,
                        ['author-create', 'author-update']
                    );
                },
                'whenClient' => 'function(){return false}'
            ],
            [
                'postRedactorComment',
                'required',
                'when' => function ($model) {
                    return $model->postStatus == 'draft' && !in_array(
                        $model->scenario,
                        ['author-create', 'author-update']
                    );
                },
                'whenClient' => 'function(){return false}'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postId' => 'Post ID',
            'userId' => 'Автор',
            'postTitle' => 'Заголовок',
            'postLead' => 'Подзаголовок',
            'postText' => 'Тело статьи',
            'postName' => 'Post Name',
            'postVideo' => 'Код видео',
            'postViews' => 'Post Views',
            'postShare' => 'Post Share',
            'postUpdateTime' => 'Post Update Time',
            'postPublishTime' => 'Время публикации',
            'postPopularity' => 'Post Popularity',
            'postStatus' => 'Статус публикации',
            'file' => 'Изображение',
            'categoryId' => 'Категория',
            'postSourceUrl' => 'URL источника',
            'postRedactorComment' => 'Сообщение автору',
            'postRel' => 'follow / no-follow',
            'postComment' => 'Коментарии',
            'postType' => 'Тип статьи',
            'postCreateTime' => 'Время первой публикации',
            'editorId' => 'Редактор',
            'categories' => 'Раздел',
            'tags' => 'Метки'
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = [
            'postTitle',
            'userId',
            'postLead',
            'postText',
            'postVideo',
            'postPublishTime',
            'postStatus',
            'postSourceUrl',
            'postRedactorComment',
            'postRel',
            'postComment',
            'postType',
            '__postImage_crop__',
            '__postImage_delete__'
        ];
        $scenarios['author-create'] = $scenarios['author-update'] = [
            'postTitle',
            'postText',
            'postLead',
            'postStatus',
        ];
        return $scenarios;
    }

    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['categoryId' => 'categoryId'])->viaTable(
            'post_has_category',
            ['postId' => 'postId']
        );
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['tagId' => 'tagId'])->viaTable(
            'post_has_tag',
            ['postId' => 'postId']
        );
    }

    public function getFormatedViews()
    {
        return $this->postViews;
        //return number_format($this->postViews, 0, ',', '.');
    }

    public function getFormatedShares()
    {
        return $this->postShare;
        //return number_format($this->postShare, 0, ',', '.');
    }

    public function getMedia()
    {
        if (!empty($this->postVideo)) {
            return \yii\helpers\Html::tag(
                'div',
                $this->postVideo,
                [
                    'class' => 'videoWrapper'
                ]
            );
        }
        return $this->getImage();
    }

    public function getImage()
    {
        return \yii\helpers\Html::img(
            $this->getImgSrc('postImage'),
            [
                'class' => 'img-responsive rounded-4x'
            ]
        );
    }

    public function getUrl($enableAchor = false)
    {
        $data = [
            'post/view',
            'id' => $this->primaryKey,
            'slug' => $this->postName
        ];
        return
            Url::to($data);
    }

    public function linkCategories($ids)
    {
        if ($ids) {
            $this->unlinkAll('categories', true);
            foreach (Category::findAll($ids) as $item) {
                $this->link('categories', $item);
            }
        }
    }

    public function linkTags($ids)
    {
        $this->unlinkAll('tags', true);
        if ($ids) {
            foreach ($ids as $id) {
                $tag = Tag::findOne($id);
                if (!$tag) {
                    \Yii::trace($id);
                    $tag = new Tag();
                    $tag->tagName = $id;
                    $tag->tagTitle = $id;
                    $tag->tagDescription = 'Статьи по тэгу ' . $id;
                    if ($tag->save()) {
                        $tags[] = $tag;
                    }
                } else {
                    $tags[] = $tag;
                }
            }
            foreach ($tags as $tag) {
                $this->link('tags', $tag);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Author::className(), ['id' => 'userId']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // editorId as current user id if author 5sfer
            if (isset(\Yii::$app->user) && \Yii::$app->user->identity && !$this->editorId && \Yii::$app->authManager->checkAccess(
                    \Yii::$app->user->identity->getId(),
                    'admin'
                )
            ) {
                $this->editorId = \Yii::$app->user->id;
            };
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                "class" => Illustrated::className(),
                'attributes' => [
                    'postImage' => [
                        'cropSize' => 720,
                        'aspectRatio' => 1.6
                    ],
                ],
                'directory' => '@media/post',
                'baseUrl' => \Yii::$app->params['mediaBaseUrl'] . '/post',
            ],
            [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'postName',
                'attribute' => 'postTitle',
                // optional params
                'ensureUnique' => false,
                //'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => $this->primaryKey < 20556,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['postId', 'postName', 'postUpdateTime']);
                    $model->andWhere(['postStatus' => Post::PUBLISH]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => \Yii::$app->urlManager->createAbsoluteUrl(
                            ['post/view', 'id' => $model->postId, 'slug' => $model->postName]
                        ),
                        'lastmod' => $model->postUpdateTime,
                        //'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'postPublishTime',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'postPublishTime',
                ],
                'value' => function ($event) {
                    if ($this->postStatus == Post::PUBLISH || $this->postStatus == Post::FUTURE) {
                        if (!$this->postPublishTime) {
                            return time();
                        }
                        if (is_int($this->postPublishTime)) {
                            return $this->postPublishTime;
                        }
                        return strtotime($this->postPublishTime);
                    } else {
                        return null;
                    }
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'postLead',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'postLead',
                ],
                'value' => function ($event) {
                    if (empty($this->postLead)) {
                        preg_match('/([\p{L}\d]+[^\p{L}\d]*){1,30}/mu', strip_tags(html_entity_decode($this->postText)), $lead);
                        return trim(isset($lead[0]) ? $lead[0] : '');
                    } else {
                        return strip_tags($this->postLead);
                    }
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'postStatus',
                ],
                'value' => function ($event) {
                    if ($this->oldAttributes['postStatus'] != Post::PUBLISH && $this->postStatus == Post::PUBLISH) {
                        $this->sendPublishMail();
                        if (!$this->postCreateTime) {
                            $this->postCreateTime = time();
                        }
                    }
                    if ($this->oldAttributes['postStatus'] == Post::DRAFT && $this->postStatus == Post::APPROVAL) {
                        $this->sendApprovalMail();
                    }
                    if ($this->postStatus == Post::FUTURE && $this->oldAttributes['postPublishTime'] != $this->postPublishTime) {
                        $this->sendPlannedMail();
                    }
                    if ($this->oldAttributes['postStatus'] != Post::DRAFT && $this->postStatus == Post::DRAFT) {
                        $this->sendRedactorCommentMail();
                    } else {
                        if ($this->oldAttributes['postStatus'] != $this->postStatus) {
                            $this->postRedactorComment = '';
                        }
                    }
                    return $this->postStatus;
                }
            ],
        ];
    }

    public function sendRedactorCommentMail()
    {
        if ($this->user->profile->status == Author::APPROVED) {
            \Yii::$app->mailer->compose('postredactorcomment-html', ['model' => $this])
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo($this->user->email)
                ->setBcc(['victori.fashion@gmail.com', 'michkire@gmail.com'])
                ->setSubject('Твоя статья вернулась на доработку')
                ->send();
        }
    }

    public function sendPublishMail()
    {
        if ($this->user->profile->status == Author::APPROVED) {
            \Yii::$app->mailer->compose('postpublish-html', ['model' => $this])
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo($this->user->email)
                ->setBcc(['victori.fashion@gmail.com', 'michkire@gmail.com'])
                ->setSubject('Твоя статья опубликована на «5 Сфер»')
                ->send();
        }
    }

    public function sendPlannedMail()
    {
        if ($this->user->profile->status == Author::APPROVED) {
            \Yii::$app->mailer->compose('postplanned-html', ['model' => $this])
                ->setFrom(\Yii::$app->params['supportEmail'])
                ->setTo($this->user->email)
                ->setBcc(['victori.fashion@gmail.com', 'michkire@gmail.com'])
                ->setSubject('Твоя статья запланирована к публикации на «5 Сфер»')
                ->send();
        }
    }

    public function sendApprovalMail()
    {
        \Yii::$app->mailer->compose('postapproval-html', ['model' => $this])
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setTo(\Yii::$app->params['supportEmail'])
            ->setBcc(['victori.fashion@gmail.com', 'michkire@gmail.com'])
            ->setSubject($this->user->profile->name . ' добавил новую статью')
            ->send();
    }
}