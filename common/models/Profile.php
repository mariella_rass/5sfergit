<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.06.15
 * Time: 10:38
 */

namespace common\models;


use himiklab\sitemap\behaviors\SitemapBehavior;
use sjaakp\illustrated\Illustrated;

class Profile extends \common\models\base\Profile
{
    public function behaviors()
    {
        return [
            [
                "class" => Illustrated::className(),
                'attributes' => [
                    'avatar' => [
                        'cropSize' => 100,
                        'aspectRatio' => 1,
                    ],
                ],
                'directory' => '@media/user',
                'baseUrl' => \Yii::$app->params['mediaBaseUrl'] . '/user',
                'noImage' =>
                    \Yii::$app->id == 'app-frontend'
                        ? '<img class="media-object img-responsive rounded-x"" src="/img/ava.jpg">'
                        : '<img class="media-object" style="width: 64px; height: 64px;border-radius: 50% !important;" src="/img/ava.jpg">'

            ],
            [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                //'translit' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => \Yii::$app->urlManager->createAbsoluteUrl(
                            ['author/index', 'slug' => $model->slug]
                        ),
                        'lastmod' => time(),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Имя',
            'nameGenitivus' => 'Имя в родительном падеже (Кого? Ицхака Пинтосевича)',
            'bio' => 'Об авторе',
            'popularity1' => 'Popularity1',
            'popularity7' => 'Popularity7',
            'popularity30' => 'Popularity30',
            'status' => 'Статус',
            'avatar' => 'Avatar',
            'facebook' => 'Facebook',
            'vk' => 'Vk',
            'youtube' => 'Youtube',
            'twitter' => 'Twitter',
            'website' => 'Вебсайт',
            'gravatar_email' => 'gravatar_email',
            'file' => 'Аватарка',
            'slug' => 'slug',
            'postForm' => 'HTML и JS формы подписки под постами автора',
            'subscribeDoneText' => 'Текст на странице подтверждения подписки автора',
            'triggerComment' => 'Комментарии',
        ];
    }
}